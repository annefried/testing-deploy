## Compass Hospitality - Headless CMS

- Headless Website
- Supports Typscript and GraphQL.
- Backend in WordPress.

## Editor with plugins

- [VSCode](https://code.visualstudio.com/)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Editorconfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)

### Installation

In the project directory, Run:

```sh
$ yarn install
```

# Development

```shell script
yarn run dev
yarn run type-check
yarn run format
yarn run lint
```

# Vercel
