export const LanguageChanger = (obj: any, field: string, lang: any) => {
  const paramLang = lang == "th" ? "TH" : "";
  let data = obj[field + paramLang];
  let isObject = typeof data === "object" && data !== null;
  while (isObject) {
    data = data[field + paramLang];
    isObject = typeof data === "object";
  }
  return data;
};
