import type { ParentItem } from "@/interfaces/global_interfaces";

export const searchMenuItems = (
  items: ParentItem[],
  searchKey: string
): ParentItem[] => {
  return items?.map((o: ParentItem) => {
    const matchingChildren = o.children
      ?.map((u: any) => {
        if (u.label?.toLowerCase().includes(searchKey.toLowerCase())) {
          return {
            ...u,
            cities: u.cities
              ? u.cities.map((city: any) => ({
                  ...city,
                  hotels: city.hotels || [],
                }))
              : [],
          };
        }

        const matchingCities = (u.cities || [])
          .map((city: any) => {
            const cityMatches = city.label
              ?.toLowerCase()
              .includes(searchKey.toLowerCase());
            const matchingHotels = (city.hotels || []).filter((hotel: any) =>
              hotel.label?.toLowerCase().includes(searchKey.toLowerCase())
            );
            return cityMatches
              ? { ...city }
              : matchingHotels.length > 0
                ? { ...city, hotels: matchingHotels }
                : null;
          })
          .filter((city: any) => city !== null);

        return matchingCities.length > 0
          ? { ...u, cities: matchingCities }
          : null;
      })
      .filter((u) => u !== null);

    return {
      ...o,
      children:
        matchingChildren && matchingChildren.length > 0 ? matchingChildren : [],
    };
  });
};
