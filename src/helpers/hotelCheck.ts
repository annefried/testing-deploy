import type { ParentItem } from "@/interfaces/global_interfaces";

const hotelCheck = (
  filteredResultsWithHotels: ParentItem[] | undefined,
  item: ParentItem
) => {
  return (
    filteredResultsWithHotels?.length !== 0 &&
    filteredResultsWithHotels?.some((k) => k?.label === item.label)
  );
};

export default hotelCheck;
