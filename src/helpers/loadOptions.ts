import type {
  FetchOptionsFunction,
  OptionsType,
} from "@/interfaces/global_interfaces";

const LoadOptions = (
  options: OptionsType | any,
  setOptions: React.Dispatch<React.SetStateAction<OptionsType>> | any,
  fetchFunction: FetchOptionsFunction,
  type: string
): void => {
  if (!options.optionsLoaded) {
    setOptions((prevOptions: any) => ({ ...prevOptions, isLoading: true }));
    fetchFunction(setOptions, type);
  }
};

export default LoadOptions;
