import axios from "axios";

export const getData = async (query: string) => {
  const endpoint = process.env.NEXT_PUBLIC_END_POINT_URL;
  const graphqlQuery = {
    query,
  };

  const headers = {
    "content-type": "application/json",
  };

  try {
    const response = axios({
      url: endpoint,
      method: "post",
      headers: headers,
      data: graphqlQuery,
    });
    return response;
  } catch (err) {
    return err;
  }
};
