export const searchMenuItems = (items: any[], searchKey: string): any[] => {
  const searchLower = searchKey.toLowerCase();
  const visitedLabels = new Set<string>();

  const searchInItems = (items: any[]): any[] => {
    const matchedItems: any[] = [];

    items.forEach((item: any) => {
      const labelLower = item.label.toLowerCase();

      if (
        (labelLower === searchLower || labelLower.includes(searchLower)) &&
        item.options &&
        item.options.length > 0
      ) {
        if (!visitedLabels.has(labelLower)) {
          matchedItems.push(item);
          visitedLabels.add(labelLower);
        }
      }

      if (item.options && item.options.length > 0) {
        const matchedOptions = searchInOptions(item.options);
        if (matchedOptions.length > 0) {
          if (!visitedLabels.has(labelLower)) {
            matchedItems.push({
              ...item,
              options: matchedOptions,
            });
            visitedLabels.add(labelLower);
          }
        }
      }
    });

    return matchedItems;
  };

  const searchInOptions = (options: any[]): any[] => {
    const matchedOptions: any[] = [];

    options.forEach((option: any) => {
      const labelLower = option.label.toLowerCase();

      if (labelLower.includes(searchLower) && !visitedLabels.has(labelLower)) {
        matchedOptions.push(option);
        visitedLabels.add(labelLower);
      }

      if (option.options && option.options.length > 0) {
        const nestedMatchedOptions = searchInOptions(option.options);
        if (nestedMatchedOptions.length > 0) {
          matchedOptions.push({
            ...option,
            options: nestedMatchedOptions,
          });
        }
      }
    });

    return matchedOptions;
  };
  return searchInItems(items);
};
