import { useEffect, useState } from "react";

export default function useHydration() {
  const [isClient, setIsClient] = useState<boolean>(false);

  useEffect(() => {
    setIsClient(true);
  }, []);

  return {
    isClient,
  };
}
