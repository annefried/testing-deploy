"use client";
import Link from "next/link";
import Button from "@/components/Button/button";
import { usePathname } from "next/navigation";
import { GET_MENUS } from "@/lib/queries/menu";
import { GET_FOOTER_MENUS } from "@/lib/queries/footer";
import Wrapper from "@/components/Layout/wrapper";
import { useEffect, useState } from "react";
import client from "@/lib/apollo/client";
import Loader from "@/components/Loader/loader";

export default function NotFound() {
  const t = {
    th: {
      errorPages: {
        client: {
          notFound: "ไม่พบหน้านี้.",
          description:
            "ขออภัย เราไม่พบหน้าที่คุณกำลังมองหา อาจถูกย้ายหรือไม่สามารถใช้งานได้ชั่วคราว",
          button: "กลับสู่หน้าแรก",
        },
      },
    },
    en: {
      errorPages: {
        client: {
          notFound: "Page Not Found.",
          description:
            "Sorry, we couldn’t find the page you’re looking for. It might has been moved or temporarily unavailable.",
          button: "Back to homepage",
        },
      },
    },
  };
  const pathName = usePathname();
  const [menus, setMenus] = useState(null);
  const [footerMenus, setFooterMenus] = useState(null);
  const locales = ["en", "th"];
  const lang = pathName
    ? locales.filter((locale) => pathName.includes(`/${locale}/`))[0]
    : "en";
  const translation = (t as any)[lang];
  const text = translation?.errorPages?.client;

  const getNavbarData = async () => {
    try {
      const data = await client.query({
        query: GET_MENUS,
      });
      setMenus(data?.data?.menu);
    } catch (err) {
      if (err) {
        return setMenus(null);
      }
    }
  };

  const getFooterData = async () => {
    try {
      const data = await client.query({
        query: GET_FOOTER_MENUS,
      });
      setFooterMenus(data?.data?.menu);
    } catch (err) {
      if (err) {
        return setFooterMenus(null);
      }
    }
  };

  useEffect(() => {
    getNavbarData();
  }, []);

  useEffect(() => {
    getFooterData();
  }, []);

  return (
    <>
      {" "}
      {menus && footerMenus ? (
        <Wrapper menus={menus} footerMenus={footerMenus} lang={lang}>
          <div className="break-word flex h-[70vh] flex-col items-center justify-center bg-secondary-400 bg-white text-center text-text-main">
            {translation && (
              <div className="px-5 text-left xl:px-0">
                <h1 className="font-display text-t-h5 lg:text-t-h1">404</h1>{" "}
                <br />
                <h2 className="font-display text-t-h6 lg:text-t-h2">
                  {text?.notFound}
                </h2>{" "}
                <br />
                <p className="font-display text-t-subtitle-2">
                  {text?.description}
                </p>{" "}
                <br />
                <Link href="/">
                  <Button className="hover-effect custom-background">
                    {" "}
                    {text?.button}
                  </Button>
                </Link>
              </div>
            )}
          </div>
        </Wrapper>
      ) : (
        <div className="flex h-dvh w-full items-center justify-center">
          <Loader />
        </div>
      )}
    </>
  );
}
