import BrandLogos from "@/components/BrandLogosSection/brandLogos";
import Banner from "@/components/Carousels/Banner/banner";
import { getClient } from "@/lib/apollo/server";
import { GET_HOME_PAGE } from "@/lib/queries/pages";

export const revalidate = 0;

const getPageData = async () => {
  const { data, error } = await getClient().query({
    query: GET_HOME_PAGE,
    variables: {
      id: "home",
    },
  });

  if (error) {
    throw new Error("Error fetching footer countries");
  }
  return data?.page?.homeFields;
};

export default async function Home() {
  const homeContent = await getPageData();
  const sectionKeys = Object.keys(homeContent);

  return (
    <main className="min-h-screen">
      {sectionKeys?.map((key) => {
        const subObjectPayload = homeContent[key];
        switch (key) {
          case "sectionHomeBanner":
            return (
              <Banner key={key} payload={subObjectPayload?.homeBannerData} />
            );
          default:
            return null;
        }
      })}
      <BrandLogos />
    </main>
  );
}
