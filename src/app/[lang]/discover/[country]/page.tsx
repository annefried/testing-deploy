import BrandLogos from "@/components/BrandLogosSection/brandLogos";
import Discover from "@/components/HotelsPage/Discover/discover";
import { getClient } from "@/lib/apollo/server";
import { GET_HOTELS_BY_CITY } from "@/lib/queries/pages";
import Page500 from "@/pages/500";
import { notFound } from "next/navigation";
export const revalidate = 60;
const getPageData = async (country: string) => {
  const { data, error } = await getClient().query({
    query: GET_HOTELS_BY_CITY,
    variables: {
      name: country?.toLowerCase()?.replace(/-/g, " "),
    },
  });
  if (error) {
    return <Page500 />;
  }
  return data?.allDestinationPages?.nodes;
};
const Page = async ({
  params,
}: {
  params: { country: string; lang: string };
}) => {
  const data = await getPageData(params?.country);
  if (data?.length === 0) {
    return notFound();
  }
  return (
    <div>
      <Discover
        {...data[0]?.destinationPage}
        lang={params?.lang}
        country={params?.country}
      />
      <BrandLogos />
    </div>
  );
};

export default Page;
