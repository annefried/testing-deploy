import Wrapper from "@/components/Layout/wrapper";
import { ApolloWrapper } from "@/lib/apollo/apollo-provider";
import { getClient } from "@/lib/apollo/server";
import { GET_MENUS } from "@/lib/queries/menu";
import { GET_FOOTER_MENUS } from "@/lib/queries/footer";
import CookieConsentBanner from "@/components/Cookies/cookies";

export async function generateStaticParams() {
  return [{ lang: "en" }, { lang: "th" }];
}

const getMenu = async () => {
  const { data, error } = await getClient().query({
    query: GET_MENUS,
  });

  if (error) {
    return null;
  }

  return {
    props: {
      menu: data?.menu,
    },
  };
};

const GetFooterMenus = async () => {
  const { data, error } = await getClient().query({
    query: GET_FOOTER_MENUS,
  });
  if (error) {
    return null;
  }
  return {
    props: {
      menu: data?.menu,
    },
  };
};

export default async function Layout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const menus = await getMenu();
  const footerMenus = await GetFooterMenus();

  return (
    <ApolloWrapper>
      {menus !== null && footerMenus !== null && (
        <Wrapper
          menus={menus?.props?.menu}
          footerMenus={footerMenus?.props?.menu}
        >
          {children}
        </Wrapper>
      )}

      <CookieConsentBanner />
    </ApolloWrapper>
  );
}
