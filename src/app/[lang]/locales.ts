import type { LocaleType } from "@/interfaces/global_interfaces";
import "server-only";

const locales = {
  en: () =>
    import("@/components/Locales/en.json").then((module) => module.default),
  th: () =>
    import("@/components/Locales/th.json").then((module) => module.default),
};

export const getLocaleData = (lang: keyof LocaleType | string | string[]) => {
  return locales[lang as keyof LocaleType]();
};
