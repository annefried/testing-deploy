import BrandLogos from "@/components/BrandLogosSection/brandLogos";
import Hotels from "@/components/HotelsPage/hotels";
import { getClient } from "@/lib/apollo/server";
import { GET_HOTELS_BY_CITY } from "@/lib/queries/pages";
import Page500 from "@/pages/500";
import { notFound } from "next/navigation";
export const revalidate = 60;
const getPageData = async (city: string) => {
  const { data, error } = await getClient().query({
    query: GET_HOTELS_BY_CITY,
    variables: {
      name: city?.replace(/-/g, " "),
    },
  });
  if (error) {
    return <Page500 />;
  }
  return data?.allDestinationPages?.nodes;
};
const Page = async ({ params }: { params: { city: string; lang: string } }) => {
  const data = await getPageData(params?.city);
  if (data?.length === 0) {
    return notFound();
  }
  return (
    <div>
      <Hotels
        {...data[0]?.destinationPage}
        lang={params?.lang}
        city={params?.city}
      />
      <BrandLogos />
    </div>
  );
};

export default Page;
