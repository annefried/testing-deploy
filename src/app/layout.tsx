import { Libre_Baskerville, Karla } from "next/font/google";
import "react-toastify/dist/ReactToastify.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./globals.scss";

const libre_baskerville = Libre_Baskerville({
  weight: ["400"],
  subsets: ["latin"],
  variable: "--libre_baskerville-font",
});
const karla = Karla({
  weight: ["400", "700"],
  subsets: ["latin"],
  variable: "--karla-font",
});
export const metadata: any = {
  title: "Compass Hospitality",
  description: "Compass Hospitalityp",
};

export default function RootLayout({
  children,
  params,
}: Readonly<{
  children: React.ReactNode;
  params: { lang: string };
}>) {
  return (
    <html lang={params.lang ?? "en"}>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <body className={`${libre_baskerville.variable} ${karla.variable}`}>
        {children}
      </body>
    </html>
  );
}
