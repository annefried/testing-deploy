export interface ButtonOptions {
  /**
   * Button display variants
   * @default "solid"
   * @type ButtonVariant
   */
  variant?: ButtonVariant;
  shadow?: string;
}

export type ButtonProps = React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement> | any,
  HTMLButtonElement
> &
  ButtonOptions;

export type ButtonVariant =
  | "outline"
  | "outline-disable"
  | "solid"
  | "ghost"
  | "white-background";
