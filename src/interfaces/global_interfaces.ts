import type { DocumentNode } from "graphql";
import type { Params } from "next/dist/shared/lib/router/utils/route-matcher";
import type React from "react";

type locale = { [key: string]: string } | any;

type localeLoader = () => Promise<locale>;

export type LocaleType = {
  en: localeLoader;
  th: localeLoader;
};

export interface MenuProps {
  item: any;
  isDropdownOpen: number | null;
  handleMouseEnter: (id: number) => void;
  handleMouseLeave: () => void;
  setSubChildMenu: (id: number | null) => void;
  subChildMenu: number | null;
  filteredResultsWithHotels: any;
  filteredResultsWithChildrenButNoCities: any;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  searchKey?: string | null;
}
export interface HotelItem {
  label: string;
  parentDatabaseId?: number;
  uri: string;
}

export interface CityItem {
  uri: string;
  label: string;
  parentId: number;
  id: number;
  hotels?: HotelItem[];
  parentDatabaseId?: number;
  databaseId?: number;
}

export interface ChildItem {
  hotels: any;
  uri: string;
  lang?: boolean;
  heading?: {
    languageHeading: string;
  };
  id: number;
  parentId: number;
  databaseId: number;
  children?: CityItem[];
  label: string | Params;
  cities?: CityItem[];
}

export interface ParentItem {
  children: ChildItem[];
  lang?: boolean;
  id: number;
  heading?: string;
  parentId: number;
  uri: string | null;
  parentDatabaseId?: number;
  databaseId?: number;
  label: string;
  childItems?: ChildItem[];
}

export interface FooterMenuItems {
  count: number;
  databaseId?: number;
  id: number;
  menuItems: MenuItems;
  name: string;
  slug: string;
}

export interface MenuItems {
  nodes: NodeItems[];
  pageInfo: PageInfo;
  name: string;
  slug: string;
}
export interface NodeItems {
  databaseId: number;
  id: string;
  label: string;
  locations: string[];
  menuItemId: number;
  parentDatabaseId: number;
  parentId: string;
  path: string;
  target?: string;
  uri: string;
}

export interface PageInfo {
  endCursor: string;
  hasNextPage: boolean;
}

export interface AppContextType {
  state: {
    discoverRef: any;
    showBookNow: boolean;
    localeData: LocaleType | any;
    hotelPayload: any;
  };
  dispatch: {
    setHotelPayload: any;
    setShowBookNow(arg0: boolean): unknown;
    handleBookNow: () => void;
    handleCloseBookNow: (value: boolean) => void;
    scrollToSection: any;
  };
}

export interface DynamicPageProps {
  params: {
    dynamic: string;
    lang?: string;
    slug?: string;
  };
  searchParams: any;
}

export interface Options {
  value: string;
  label: string;
}
export interface FilterProps {
  borderLeft?: boolean;
  hideBorder?: boolean;
  countriesIsLoading?: boolean;
  destinationsIsLoading?: boolean;
  handleCountrySelect?: (e: Options) => void;
  handleDestinationSelect?: (e: Options) => void;
  countrySelectedOption?: Options[] | any;
  destinationSelectedOption?: Options[] | any;
  countryOptions?: Options[];
  destinationOptions?: Options[];
  showCountry?: boolean;
  showDestination?: boolean;
  countryLabel?: string;
  destinationLabel?: string;
  loadCountriesOptions?: () => void;
  loadDestinationsOptions?: () => void;
  loadCategoryOptions?: () => void;
  filterPacks?: FilterPropItem[];
}

export interface FilterPropItem {
  isLoading: boolean;
  handleSelect?: (e: Options) => void;
  selectedOption: Options[] | any;
  label: string;
  loadOptions?: () => void;
  optionsLoaded?: boolean;
  options: Options[];
  query?: DocumentNode;
  setState?: (e: any) => void;
  responseMapping?: (e: any) => Array<{ label: string; value: string }>;
  onChange?: any;
  isChange?: boolean;
}

export interface PayloadItem {
  slug?: string;
  url?: string;
  location?: string;
  title: string;
  src: {
    src: string;
  };
}

export interface QuincunxGridProps {
  payload: PayloadItem[] | any;
  urlText?: string;
  fetchMore?: (showMore?: boolean) => void;
  hideArrow?: boolean;
}

export interface OptionsType {
  optionsLoaded: boolean;
  options: any[];
  isLoading: boolean;
}

export interface FetchOptionsFunction {
  (
    setOptions: React.Dispatch<React.SetStateAction<OptionsType>>,
    type: string
  ): Promise<void>;
}

export interface DropdownApiResponseProps {
  data: {
    data: {
      menu: {
        bookNowMenu: { hotel_dropdown: { value: string; label: string }[] };
      };
    };
  };
}

export interface States {
  optionsLoaded: boolean;
  options: Array<{ value: string; label: string }>;
  isLoading: boolean;
  selectedOption: Array<{ value: string; label: string }> | any;
}

export interface imageProps {
  node: { mediaItemUrl: string; altText?: string };
}
