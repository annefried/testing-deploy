import type { DateValueType } from "react-tailwindcss-datepicker";

export interface DatePickerProps {
  placeholder?: string;
  lang: string | any;
  value: DateValueType;
  setValue: (newValue: DateValueType) => void;
}
