export interface BannerItem {
  bannerImage?:
    | {
        node: {
          mediaItemUrl: string;
        };
      }
    | any;
  description?: string;
  subTitle?: string;
  title: string;
  buttonText?: string;
  link?: string;
  saleType?: string;
  showMainButton?: boolean;
  alt?: string;
}
