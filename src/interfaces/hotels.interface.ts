import type { imageProps } from "./global_interfaces";

export interface HotelProps {
  taglineForOffers: any;
  relatedOffers?: any;
  lang: string;
  country?: string;
  city?: string;
  sectionDestinationBanner: {
    bannerTitle: string;
    subText: string;
    backgroundImage: {
      node: {
        altText: string | undefined;
        mediaItemUrl: string;
      };
    };
  };
  taglineUnderHotelHeading: {
    subText: string;
  };
}

export interface LocationContact {
  address: string;
  phone: string;
  email: string;
  longitude: number;
  latitude: number;
}

export interface HotelDetailProps {
  offers: [];
  tabsData: [];
  dining: any;
  lang: string;
  slug: string;
  title: string;
  hotelFacilities: {
    nodes: {}[];
  };
  hotelDetails: {
    discoverHotelSection: {
      room: null;
      dining:
        | {
            type: [];
          }[]
        | any;
      locationcontact: LocationContact | any;
    }[];
    hotelSection: {
      bookUrl: {
        url: string;
      };
      hotelName: string;
      hotelShortDescription: string;
      hotelDescription: string;
      bannerImage: imageProps;
      selectBrand: {
        edges: {
          name: string;
        }[];
      };
    };
  };
}

export interface HotelRoomsProps {
  lang: string;
  roomsData?: [] | any;
  hotelSlug: string;
  roomFeatures: {
    nodes: {};
  };
  node: {
    title: string;
    roomsFields: {
      roomsDetails: {
        roomGallery: any;
        subText: string;
        occupancy: string;
        bedding: [];
        bannerImage: imageProps;
      };
    };
  };
  hotelDetails:
    | {
        bookUrl: {
          url: string;
        };
        featuresAndFacilities: any;
        selectPriceRange: {
          edges: {
            node: {
              name: string;
            };
          }[];
        };
      }
    | any;
}
