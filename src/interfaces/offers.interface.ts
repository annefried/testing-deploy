import type { RefObject } from "react";
import type { BannerItem } from "./carousel.interface";
import type { imageProps } from "./global_interfaces";

export interface BrowseOfferProps {
  title: string;
  payload: BannerItem[];
  ref: RefObject<HTMLDivElement>;
  lang: any;
}

export interface DetailProps {
  title: string;
  paragraph?: string;
  limitedTimeDate?: string;
  stayPeriodDate?: string;
  termsAndConditions: {
    heading: string;
    items: {
      text: string;
    }[];
  };
  ref: RefObject<HTMLDivElement>;
}

export interface ParticipatingHotelsProps {
  title: string;
  slug: string;
  lang: any;
}

export interface OfferProps {
  lang?: string;
  title: string;
  slug: string;
  offerMain: { offerIntroduction: string };
  featuredImage: imageProps;
  offerSection: {
    bannerSubheading: string | undefined;
    heading: string;
    sectionSubText: string;
  };
  browseOffersHeading: string;
}

interface ParentOfferProps {
  node: {
    title: string;
    offerMain: {
      offerIntroduction: string;
    };
    featuredImage: {
      node: {
        mediaItemUrl: string;
        altText: string;
      };
    };
  };
}

export interface ChildOfferProps {
  node: {
    offersChild: {
      parentOffer:
        | {
            nodes: ParentOfferProps;
          }
        | any;
      offersDetail: {
        periodEndDate: string;
        stayPeriodStart: string;
        termsConditions: string;
        inclusions: string;
        reservationUrl: {
          url: string;
        };
      };
    };
  };
}
