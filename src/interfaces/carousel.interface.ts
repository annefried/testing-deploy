export interface BannerItem {
  altText?: string;
  bannerTitleTH: string;
  subTextTH: string;
  mediaItemUrl: string;
  url?: string;
  code?: string | null;
  link?: string;
  slug?: string;
  location?: string;
  bannerTitle?: string | any;
  subText?: string;
  eventPage?: boolean;
  node?: any;
  imageFieldForHomebanner?:
    | {
        node: {
          mediaItemUrl: string;
        };
      }
    | any;
  deal?: boolean;
  date?: string;
  description?: string;
  title: string;
  showButton: boolean;
  buttonText: string;
  src: {
    src: string;
  };
}
