import type { BannerItem } from "./carousel.interface";

interface PayloadSignature {
  destinationName: string;
  destinationNameTH: string;
  destinationImage: {
    node: {
      altText: string;
      mediaItemUrl: string;
    };
  };
  discoverLink: {
    url: string;
    edges: {
      node: {
        link: string;
      };
    }[];
  };
}

export interface SignatureProps {
  title: string | any;
  payload: PayloadSignature[];
  lang: string;
}

export interface OfferProps {
  title: string | any;
  description?: string;
}

interface ExtraOrdinaryExperiences {
  experienceName: string;
  experienceNameTH: string;
  imageFieldForExperiences: {
    node: {
      altText: string;
      mediaItemUrl: string;
    };
  };
  exploreLink: {
    nodes: {
      link: string;
      uri?: string;
    }[];
  };
}

export interface FeaturedPropertiesProps {
  title: string | any;
  payload: BannerItem[];
  lang: string;
}

export interface TravelPayloadProps {
  node: {
    slug: string;
    title: string;
    featuredImage: {
      node: {
        mediaItemUrl: string;
      };
    };
    travelInspiration: {
      selectDestination: {
        edges: {
          node: {
            name: string;
          };
        }[];
      };
    };
  };
}
[];

export interface TravelProps {
  title: string;
  showExploreButton?: boolean;
  numberOfItems?: number;
  featuredItems?: boolean;
  infiniteScroll?: boolean;
  serverSideData?: any;
}

export interface ContentSectionProps {
  title: string;
  link: string;
  text: string;
  position?: boolean;
  property?: boolean;
  price?: string;
}

export interface ExtraOrdinaryExperiencesContentSection {
  items: ExtraOrdinaryExperiences;
}

export interface ExtraOrdinaryExperiencesProps {
  title: string | any;
  description: string;
  payload: ExtraOrdinaryExperiences[];
  lang?: string;
}

export interface GridItem {
  destination: string;
  src: {
    src: string;
  };
  title: string;
}
