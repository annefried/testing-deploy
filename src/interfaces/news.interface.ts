export interface NewsDataProps {
  newsData:
    | {
        date: string;
        title: string;
        edges: {
          node: {
            newsFields: {
              newsData: {
                content: string;
                image: string;
              }[];
            };
          };
        }[];
      }
    | any;
  pageData:
    | {
        bannerImage: any;
        companyName: string | undefined;
        bannerHeader: string;
        title: string;
        subTitle: string;
      }
    | any;
}
