export interface ContactUs {
  description?: string;
  subTitle?: string;
  title: string;
  buttonText?: string;
  link?: string;
  saleType?: string;
}
