import type { StaticImport } from "next/dist/shared/lib/get-img-props";
import type React from "react";

export interface InputProps {
  label?: string;
  value: any;
  placeholder?: string;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type?: string;
  inputName: string;
  className?: string;
  showLeftIcon?: boolean;
  showRightIcon?: boolean;
  icon?: string | StaticImport;
  required?: boolean;
  error?: boolean;
}
