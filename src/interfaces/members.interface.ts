export interface BenefitsProps {
  title: string;
  content: {
    heading: string;
    content: string;
  }[];
  featureImage: string;
}

export interface MembersProps {
  bannerSection: {
    subText: string;
    sectionHeading: string;
    backgroundImage: {
      node: {
        mediaItemUrl: string;
      };
    };
  };
  sectionForm: {
    formHeading: string;
  };
  compassBenefitsSection: {}[];
}

export interface BannerSectionProps {
  subText: string;
  sectionHeading: string;
  backgroundImage: {
    node: {
      mediaItemUrl: string;
    };
  };
}

export interface SectionFormProps {
  formHeading: string;
}

export interface CompassBenefitsSectionProps {
  sectionHeading: string;
  content: {
    heading: string;
    content: string;
  }[];
  sectionImage: {
    node: {
      mediaItemUrl: string;
    };
  };
}
