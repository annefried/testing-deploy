"use client";

import { ApolloLink, HttpLink } from "@apollo/client";
import {
  NextSSRInMemoryCache,
  NextSSRApolloClient,
  SSRMultipartLink,
  ApolloNextAppProvider,
} from "@apollo/experimental-nextjs-app-support/ssr";

function makeClient() {
  const httpLink = new HttpLink({
    uri: process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL,
    useGETForQueries: true,
  });

  const multipartLink: any = new SSRMultipartLink({
    stripDefer: true,
  });

  return new NextSSRApolloClient({
    cache: new NextSSRInMemoryCache(),
    defaultOptions: {
      query: {
        fetchPolicy: "network-only",
      },
    },
    link:
      typeof window === "undefined"
        ? ApolloLink.from([multipartLink, httpLink])
        : httpLink,
  });
}

export function ApolloWrapper({
  children,
}: Readonly<{ children: React.ReactNode }>) {
  return (
    <ApolloNextAppProvider makeClient={makeClient}>
      {children}
    </ApolloNextAppProvider>
  );
}
