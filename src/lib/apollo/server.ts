import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloLink,
} from "@apollo/client";
import { registerApolloClient } from "@apollo/experimental-nextjs-app-support/rsc";
import apolloOnError from "./error";

const GRAPHQL_ENDPOINT = process.env.NEXT_PUBLIC_END_POINT_URL ?? "";

export const { getClient } = registerApolloClient(
  () =>
    new ApolloClient({
      cache: new InMemoryCache({
        typePolicies: {
          Query: {
            fields: {
              "*": {
                read() {
                  return null;
                },
              },
            },
          },
        },
      }),
      defaultOptions: {
        query: {
          fetchPolicy: "network-only",
        },
      },
      link: ApolloLink.from([
        apolloOnError,
        new HttpLink({
          uri: GRAPHQL_ENDPOINT,
          useGETForQueries: true,
        }),
      ]),
    })
);
