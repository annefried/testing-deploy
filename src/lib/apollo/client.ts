import {
  ApolloClient,
  InMemoryCache,
  ApolloLink,
  HttpLink,
} from "@apollo/client";
import apolloOnError from "./error";

const uri = process.env.NEXT_PUBLIC_END_POINT_URL ?? "";

const cache = new InMemoryCache({});

const client = new ApolloClient({
  cache,
  link: ApolloLink.from([
    apolloOnError,
    new HttpLink({
      uri,
    }),
  ]),
  defaultOptions: {
    query: {
      fetchPolicy: "no-cache",
    },
  },
});

export default client;
