import { onError } from "apollo-link-error";

const apolloOnError: any = onError(({ graphQLErrors, networkError }: any) => {
  // eslint-disable-next-line no-console
  console.log(graphQLErrors, networkError);
  if (networkError) {
    const customError = new Error(`Network error: ${networkError.message}`);
    throw customError;
  }

  if (graphQLErrors) {
    graphQLErrors.forEach(
      ({
        message,
        locations,
        path,
      }: {
        message: string;
        locations: string;
        path: string;
      }) => {
        // eslint-disable-next-line no-console
        console.log(
          `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
            locations
          )}, Path: ${path}`
        );
        const customError: Error & {
          graphQLErrors?: any[];
          networkError?: any;
        } = new Error(`GraphQL error: ${message}`);
        customError.graphQLErrors = graphQLErrors;
        customError.networkError = networkError;
        throw customError;
      }
    );
  }
});
export default apolloOnError;
