import { gql } from "@apollo/client";

export const GET_MENUS = gql`
  query GET_MENUS {
    menu(id: "Header Menu", idType: NAME) {
      name
      menuItems(first: 2000) {
        nodes {
          id
          databaseId
          label
          labelTH {
            labelTH
          }
          parentId
          menuItemId
          parentDatabaseId
          uri
          uriTH {
            uriTH
          }
        }
      }
    }
  }
`;
