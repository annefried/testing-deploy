import { gql } from "@apollo/client";

export const GET_FOOTER_MENUS = gql`
  query GET_FOOTER_MENUS {
    menu(id: "footer Menu", idType: NAME) {
      count
      id
      databaseId
      name
      slug
      menuItems(first: 200) {
        nodes {
          id
          databaseId
          label
          labelTH {
            labelTH
          }
          parentId
          locations
          menuItemId
          parentDatabaseId
          uri
          uriTH {
            uriTH
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;
