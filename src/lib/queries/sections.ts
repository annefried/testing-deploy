import { gql } from "@apollo/client";

export const GET_BRAND_LOGOS = gql`
  query BrandComponent($title: String!) {
    brandsComponent(where: { title: $title }, first: 100) {
      edges {
        node {
          brandsComponentFields {
            brandLogo {
              brandText
              brandLogo {
                image {
                  node {
                    altText
                    mediaItemUrl
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
