import { gql } from "@apollo/client";

export const GET_FEATURED_OFFERS = gql`
  query MainOffer($first: Int!) {
    promotionsAndOffers(
      where: {
        taxQuery: {
          taxArray: {
            taxonomy: OFFERTYPE
            operator: IN
            field: NAME
            terms: "Featured"
          }
        }
      }
      first: $first
    ) {
      edges {
        node {
          title
          slug
          offerMain {
            selectOfferTag {
              nodes {
                name
              }
            }
            offerIntroduction
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_BANNER_DATA = gql`
  query MainOffer {
    promotionsAndOffers(
      where: {
        taxQuery: {
          taxArray: {
            taxonomy: OFFERTYPE
            operator: IN
            field: NAME
            terms: "Display as Banner"
          }
        }
      }
    ) {
      edges {
        node {
          title
          slug
          offerMain {
            offerIntroduction
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_OFFER_PAGE = gql`
  query OfferPage {
    page(id: "offers", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      offersFields {
        offerSection {
          heading
          sectionSubText
          bannerSubheading
        }
        browseOffersHeading
      }
    }
  }
`;

export const GET_OFFERS = (filterValue?: string) => gql`
  query MainOffer {
    promotionsAndOffers(
      ${
        filterValue &&
        `where: {
        taxQuery: {
        taxArray:
            [{taxonomy:OFFERDESTINATION, operator: AND, terms: ["${filterValue}"], field: NAME}], relation:AND
        }
        }`
      },
      first: 100000,
    ) {
      edges {
        node {
          title
          slug
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          offerMain {
            selectDestination {
              nodes {
                name
                description
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_OFFER_WIHOUT_FILTER = gql`
  query MainOffer {
    promotionsAndOffers(first: 100000) {
      edges {
        node {
          title
          slug
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          offerMain {
            selectDestination {
              nodes {
                name
                description
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_PARENT_OFFER = gql`
  query MainOffer($slug: String!) {
    promotionsAndOffers(where: { slug: $slug }, first: 100000) {
      edges {
        node {
          title
          offerMain {
            discountPercentage
            offerIntroduction
            offerDate {
              periodStart
              periodEnd
            }
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_OFFERS_LIST = gql`
  query MainOffer {
    promotionsAndOffers(first: 10) {
      edges {
        node {
          slug
        }
      }
    }
  }
`;

export const GET_FEATURED_OFFERS_BY_COUNTRY = gql`
  query MainOffer($filterValue: [String]) {
    promotionsAndOffers(
      where: {
        taxQuery: {
          taxArray: {
            taxonomy: OFFERDESTINATION
            operator: AND
            field: NAME
            terms: $filterValue
          }
        }
      }
      first: 100000
    ) {
      edges {
        node {
          title
          slug
          offerMain {
            selectOfferTag {
              nodes {
                name
              }
            }
            offerIntroduction
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_OFFER_DETAILS_BY_HOTEL = gql`
  query OfferDetail($hotelSlug: String!, $mainOfferSlug: String!) {
    childOffers(
      where: { hotelSlug: $hotelSlug, mainOfferSlug: $mainOfferSlug }
    ) {
      edges {
        node {
          offersChild {
            parentOffer {
              nodes {
                ... on PromotionAndOffer {
                  title
                  featuredImage {
                    node {
                      mediaItemUrl
                      altText
                    }
                  }
                  offerMain {
                    offerIntroduction
                  }
                }
              }
            }
            offersDetail {
              inclusions
              periodEndDate
              stayPeriodStart
              termsConditions
              reservationUrl {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_MAIN_OFFERS_BY_HOTEL = gql`
  query GetHotelWithOffers($slug: String!) {
    hotels(where: { slug: $slug }, first: 100000) {
      edges {
        node {
          hotelDetails {
            mainOfferHotel {
              nodes {
                slug
                ... on PromotionAndOffer {
                  title
                  slug
                  featuredImage {
                    node {
                      mediaItemUrl
                      altText
                    }
                  }
                  offersType {
                    nodes {
                      name
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_PARTICIPATING_HOTELS = (
  offerSlug: string | null,
  filterValue?: string
) => gql`
query GetHotelWithOffers {
  hotels(
    where: {offerSlug: "${offerSlug}"
    ${filterValue ? `, taxQuery: {taxArray: {operator: IN, taxonomy: DESTINATION, field: NAME, terms: "${filterValue}"}}` : ""}
  },
  first: 10000
  ) {
    edges {
      node {
        title
        slug
        hotelDetails {
          hotelSection {
            bannerImage {
              node {
                mediaItemUrl
                altText
              }
            }
            bookUrl {
            url
            }
            selectDestination {
              nodes {
                name
                description
              }
            }
          }
          mainOfferHotel {
            nodes {
              slug
            }
          }
        }
      }
    }
  }
}
`;
