import { gql } from "@apollo/client";

export const GET_EVENT_PAGE = gql`
  query EventPage {
    page(id: "event", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      eventFields {
        eventBanner {
          bannerSection {
            eventName
            eventNameTH
            title
            titleTH
            image {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
        contentSection {
          theDifferenceSection {
            heading
            headingTH
            content
            contentTH
            image {
              node {
                mediaItemUrl
                altText
              }
            }
            eventsDescription {
              heading
              headingTH
              subText
              subTextTH
              backgroundImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
            }
            whatWeCanDoSectionContent {
              heading
              headingTH
              services {
                serviceName
                serviceNameTH
                image {
                  node {
                    mediaItemUrl
                    altText
                  }
                }
              }
            }
          }
        }
        perfectEventVenueSection {
          heading
          headingTH
        }
      }
    }
  }
`;

export const GET_EVENT_FILTER = (obj: {
  category?: string;
  destination?: string;
}) => gql`
  query MeetingEvents {
    allMeetingevents(
      where: {
        taxQuery: {
          taxArray: [
          ${
            obj.category
              ? `  {
              taxonomy: EVENTCATEGORY
              field: NAME
              operator: IN
              terms: "${obj.category}"
            }`
              : ""
          }
            ${
              obj.destination
                ? `{
              taxonomy: EVENTDESTINATION
              field: NAME
              operator: IN
              terms: "${obj.destination}"
            }`
                : ""
            }
          ]
        }
      }
    ) {
      nodes {
        title
        featuredImage {
          node {
            mediaItemUrl
            altText
          }
        }
        meetingAndEvents {
          category {
            nodes {
              name
            }
          }
          selectHotel {
            nodes {
              slug
            }
          }
          meetingevents {
            venue {
              heading
              headingTH
              image {
                node {
                  mediaItemUrl
                  altText
                }
              }
              description
              descriptionTH
              roomSize {
                tableStyle
                sizesquareMeter
                numberOfPeople
                capacity
                pdfLink {
                  url
                }
              }
            }
          }
          moreInformation {
            url
          }
          eventDestination {
            nodes {
              name
              description
            }
          }
        }
      }
    }
  }
`;
