import { gql } from "@apollo/client";

export const GET_HOTELS_BY_COUNTRY = (
  country?: string | null | any,
  first?: number,
  city?: string | null | any
) => gql`
query hotelsByCountry {
    hotels(
        first: ${first ?? null}
        after: null,
        where: {
            taxQuery: {
            taxArray: ${
              !city
                ? `{
                taxonomy: DESTINATION, 
                operator: AND, 
                terms: "${country}", 
                field: SLUG
              }`
                : `[{taxonomy: DESTINATION, operator: AND, terms: ["${country}"], field: SLUG}, {taxonomy: DESTINATION, operator: AND, terms: ["${city}"], field: SLUG}], relation: AND`
            }
            }
            }
    ) {
      edges {
        node {
          slug
          title
          hotelDetails {
            hotelSection {
              bannerImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
              selectDestination {
                nodes {
                  name
                  description
                }
              }
              selectPriceRange {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_HOTELS_BY_CITIES = (city: string | null | any) => gql`
query GetHotelsByCity {
  hotels(
    first: 10000,
    after: null,
    where: {taxQuery: {taxArray: {taxonomy: DESTINATION, operator: AND, terms: ["${city}"], field: SLUG}}}
  ) {
    edges {
      node {
        slug
        title
        hotelDetails {
          hotelSection {
            bannerImage {
              node {
                mediaItemUrl
                altText
              }
            }
            selectDestination {
              nodes {
                name
              }
            }
            selectPriceRange {
              edges {
                node {
                  name
                }
              }
            }
            }
          }
        }
      }
  }
}
`;

export const GET_HOTEL_DETAIL_BY_SLUG = gql`
  query GetHotelDetail($slug: String!) {
    hotelBy(slug: $slug) {
      title
      titleTH {
        titleTH
      }
      hotelDetails {
        hotelSection {
          bannerImage {
            node {
              mediaItemUrl
              altText
            }
          }
          hotelDescription
          hotelDescriptionTH
          hotelShortDescription
          hotelShortDescriptionTH
          bookUrl {
            url
          }
          selectBrand {
            edges {
              node {
                name
              }
            }
          }
        }
        discoverHotelSection {
          locationcontact {
            address
            email
            phone
            type
            longitude
            latitude
          }
        }
      }
      hotelFacilities {
        nodes {
          name
          icon {
            icon {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_HOTEL_FOR_META = gql`
  query GetHotelDetail($slug: String!) {
    hotelBy(slug: $slug) {
      title
      titleTH {
        titleTH
      }
      hotelDetails {
        hotelSection {
          hotelDescription
          hotelDescriptionTH
          hotelShortDescription
          hotelShortDescriptionTH
        }
      }
      hotelFacilities {
        nodes {
          name
          icon {
            icon {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_HOTEL_GALLERY = gql`
  query GetHotelDetail($slug: String!) {
    hotelBy(slug: $slug) {
      hotelDetails {
        hotelSection {
          hotelGallery {
            nodes {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_ROOMS_BY_HOTEL = gql`
  query GetRoomsByHotel($slug: String!) {
    rooms(where: { hotelSlug: $slug }) {
      edges {
        node {
          roomsFields {
            roomsDetails {
              bannerImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
              subText
              bedding
              occupancy
            }
          }
          title
          slug
        }
      }
    }
  }
`;

export const GET_DINING_BY_SLUG = gql`
  query GetHotelDining($hotelSlug: String!) {
    resturants(where: { hotelSlug: $hotelSlug }) {
      edges {
        node {
          title
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          resturantDetailsFields {
            resturantDetails {
              resturantDetails
              contactWebsite {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_ROOM_BY_TITLE = gql`
  query GetRoomsByHotel($title: String!) {
    rooms(where: { title: $title }) {
      edges {
        node {
          roomsFields {
            roomsDetails {
              bannerImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
              subText
              bedding
              occupancy
              roomGallery {
                nodes {
                  mediaItemUrl
                  altText
                }
              }
            }
          }
          title
        }
      }
    }
  }
`;

export const GET_HOTEL_INFO = gql`
  query GetHotelDetail($slug: String!) {
    hotelBy(slug: $slug) {
      title
      titleTH {
        titleTH
      }
      slug
      hotelDetails {
        hotelSection {
          selectPriceRange {
            edges {
              node {
                name
              }
            }
          }
          bookUrl {
            url
          }
        }
      }
      roomFeatures {
        nodes {
          name
          icon {
            icon {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_EVENTS_MEETINGS_BY_HOTEL = gql`
  query GetMeetingEvents($hotelSlug: String!) {
    allMeetingevents(where: { hotelSlug: $hotelSlug }) {
      nodes {
        meetingAndEvents {
          meetingevents {
            venue {
              heading
              image {
                node {
                  mediaItemUrl
                  altText
                }
              }
              description
              roomSize {
                tableStyle
                sizesquareMeter
                numberOfPeople
                capacity
                pdfLink {
                  url
                }
              }
            }
          }
          moreInformation {
            url
          }
        }
      }
    }
  }
`;

export const GET_HOTEL_TABS = gql`
  query HotelTabs {
    menu(id: "Hotel Label Menu", idType: NAME) {
      hotelDetailPageLabels {
        labels_data {
          label
          type
        }
        labels_dataTH {
          label
          type
        }
      }
    }
  }
`;

export const GET_FEATURED_HOTELS = gql`
  query GetFeaturedHotels {
    hotels(
      first: 5
      where: {
        taxQuery: {
          taxArray: {
            taxonomy: DESTINATION
            operator: EXISTS
            field: NAME
            terms: "Featured Post"
          }
        }
      }
    ) {
      edges {
        node {
          slug
          title
          hotelDetails {
            hotelSection {
              bannerImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
              selectPriceRange {
                edges {
                  node {
                    name
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_HOTEL_LIST = () => gql`
  query GetHotelsList {
    hotels(first: 1000000, after: null) {
      edges {
        node {
          slug
          title
          hotelDetails {
            hotelSection {
              bannerImage {
                node {
                  mediaItemUrl
                  altText
                }
              }
            }
            discoverHotelSection {
              locationcontact {
                longitude
                latitude
              }
            }
          }
        }
      }
    }
  }
`;
