import { gql } from "@apollo/client";

export const GET_EAT_DRINK_PAGE = gql`
  query EatDrinkPage {
    page(id: "eat-and-drink", idType: URI) {
      slug
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      eatDrinksPage {
        banner {
          subHeading
          subHeadingTH
          heading
          headingTH
          backgroundImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        worldOfTastes {
          mainHeading
          mainHeadingTH
          content {
            heading
            headingTH
            text
            textTH
            images {
              nodes {
                mediaItemUrl
                altText
              }
            }
          }
        }
        sectionExploreExclusiveDining {
          heading
          headingTH
          subText
          subTextTH
        }
        sectionRestaurantsYouMayLike {
          heading
          headingTH
          subText
          subTextTH
        }
      }
    }
  }
`;

export const GET_YOU_MAY_LIKE_HEADER = gql`
  query EatDrinkPage {
    page(id: "eat-and-drink", idType: URI) {
      eatDrinksPage {
        sectionRestaurantsYouMayLike {
          heading
          headingTH
          subText
          subTextTH
        }
      }
    }
  }
`;

export const GET_EAT_AND_DRINK_FILTER = (obj: {
  category?: string | null;
  destination?: string | null;
  name?: string;
  hotelSlug?: string;
}) => gql`
  query GET_Resturants {
    resturants (
      where: {
        ${obj.name ? `name:"${obj.name}"` : ""}
       ${
         obj.category || obj.destination
           ? ` taxQuery: {
          taxArray: [
            ${obj.category ? ` { taxonomy: CUISINE, terms: "${obj.category}", field: NAME, operator: IN }` : ""}
            ${
              obj.destination
                ? `{
              taxonomy: DININGDESTINATION
              terms: "${obj.destination}"
              field: NAME
              operator: IN
            }`
                : ""
            }
          ]
        }`
           : ""
       }
      },
      first: 10000
    ) {
      edges {
        node {
          title
          slug
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          resturantDetailsFields {
            resturantDetails {
              resturantPrice {
                nodes {
                  name
                }
              }
              selectCusines {
                nodes {
                  name
                }
              }
              destination {
                nodes {
                  name
                  description
                }
              }
              resturantPhotos {
                nodes {
                  mediaItemUrl
                  altText
                }
              }
              resturantDetails
              resturantLocation
              contactWebsite {
                url
              }
              restaurantTime {
                timeDetails {
                  time
                  title
                }
                day
              }
              dressCode
            }
            selectHotel {
              nodes {
                slug
              }
            }
          }
        }
      }
    }
  }
`;
