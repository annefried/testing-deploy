import { gql } from "@apollo/client";

export const GET_OUR_BRAND_DATA = gql`
  query OurBrand {
    page(id: "our-brand", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      ourBrand {
        bannerSection {
          heading
          headingTH
          subText
          subTextTH
          backgroundImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        brandsSection {
          brandName
          brandNameTH
          brandText
          brandTextTH
          brandImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;
