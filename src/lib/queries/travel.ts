import { gql } from "@apollo/client";

export const GET_TRAVEL_DETAIL = gql`
  query TravelDetails($slug: String) {
    posts(
      where: {
        categoryName: "Travel Blog"
        slug: $slug
        taxQuery: {
          taxArray: {
            taxonomy: TAG
            operator: IN
            field: NAME
            terms: "Featured Post"
          }
        }
      }
    ) {
      edges {
        node {
          slug
          title
          titleTH {
            titleTH
          }
          date
          featuredImage {
            node {
              mediaItemUrl
              altText
              caption(format: RAW)
            }
          }
          travelInspiration {
            contentType {
              imageContent
              normalContent
            }
            introduction
            introductionTH
            travelDetails {
              fieldGroupName
              textTravel
              textTravelTH
              image {
                node {
                  mediaItemUrl
                  altText
                  caption
                }
              }
            }
            selectDestination {
              edges {
                node {
                  name
                }
              }
            }
            isItAFeaturedPost {
              edges {
                node {
                  name
                }
              }
            }
            text
            textTH
          }
        }
      }
    }
  }
`;

export const GET_TRAVEL_DATA = (
  terms?: string | null,
  first?: number,
  after?: string | null
) => gql`
query TravelDetails {
  posts(
    where: {
      categoryName: "Travel Blog",
      ${terms ? `taxQuery: {taxArray: {taxonomy: TAG, operator: IN, field: NAME, terms: "${terms}"}}` : ""}
    }
    first: ${first ?? null}
    after: ${after ? `"${after}"` : null}
  ) {
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        slug
        title
        titleTH {
          titleTH
        }
        featuredImage {
          node {
            mediaItemUrl
            altText
          }
        }
        travelInspiration {
          selectDestination {
            edges {
              node {
                name
              }
            }
          }
          isItAFeaturedPost {
              edges {
                node {
                  name
                }
              }
            }
        }
      }
    }
  }
}
`;
