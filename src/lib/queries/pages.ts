import { gql } from "@apollo/client";

export const GET_HOME_PAGE = gql`
  query PageBySlugHome($id: ID!) {
    page(id: $id, idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      homeFields {
        sectionHomeBanner {
          homeBannerData {
            bannerTitle
            bannerTitleTH
            imageFieldForHomebanner {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
        sectionSignature {
          signatureText
          signatureTextTH
          destinationCards {
            destinationImage {
              node {
                mediaItemUrl
                altText
              }
            }
            destinationName
            destinationNameTH
            discoverLink {
              url
            }
          }
        }
        sectionPromotionAndOffers {
          promotionAndOffers
          promotionAndOffersTH
          promotionAndOffersSubText
          promotionAndOffersSubTextTH
        }
        featuredProperties
        featuredPropertiesTH
        sectionExtraordinaryExperiences {
          extraordinaryExperiencesText
          extraordinaryExperiencesTextTH
          extraordinaryExperiencesSubText
          extraordinaryExperiencesSubTextTH
          experiencesCards {
            experienceName
            experienceNameTH
            imageFieldForExperiences {
              node {
                mediaItemUrl
                altText
              }
            }
            exploreLink {
              nodes {
                link
                uri
              }
            }
          }
        }
        travelInspiration
        travelInspirationTH
        sectionInstagram {
          sectionHeading
          sectionHeadingTH
        }
      }
    }
  }
`;

export const GET_HOME_PAGE_META = gql`
  query PageBySlug($id: ID!) {
    page(id: $id, idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      homeFields {
        sectionHomeBanner {
          homeBannerData {
            imageFieldForHomebanner {
              node {
                mediaItemUrl
                altText
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_HOTELS_BY_CITY = gql`
  query PageByCity($name: String!) {
    allDestinationPages(where: { name: $name }) {
      nodes {
        destinationPage {
          sectionDestinationBanner {
            bannerTitle
            bannerTitleTH
            subText
            subTextTH
            backgroundImage {
              node {
                mediaItemUrl
                altText
              }
            }
          }
          taglineForOffers {
            heading
            subText
          }
          taglineUnderHotelHeading {
            subText
            subTextTH
          }
        }
      }
    }
  }
`;

export const GET_MEMBERS_PAGE = gql`
  query MemberBenefit {
    page(id: "member", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      memberBenefit {
        bannerSection {
          sectionHeading
          subText
          backgroundImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        sectionForm {
          formHeading
        }
        compassBenefitsSection {
          content {
            heading
            content
          }
          sectionImage {
            node {
              mediaItemUrl
              altText
            }
          }
          sectionHeading
        }
      }
    }
  }
`;

export const GET_CONTACT_US_DATA = gql`
  query ContactUs {
    page(id: "contact-us", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      contactUsFields {
        bannerSection {
          bannerImage {
            node {
              mediaItemUrl
              altText
            }
          }
          heading
          subText
        }
        sectionInquires {
          heading
        }
        locations {
          heading
          subText
          image {
            node {
              mediaItemUrl
              altText
            }
          }
          information {
            content
            contentLink {
              url
            }
            type
          }
        }
      }
    }
  }
`;
