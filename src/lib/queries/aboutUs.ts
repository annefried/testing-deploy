import { gql } from "@apollo/client";

export const GET_ABOUT_US_PAGE = gql`
  query AboutUs {
    page(id: "about-us", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      aboutUsFields {
        bannerSection {
          subHeading
          subHeadingTH
          mainHeading
          mainHeadingTH
          bannerImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        companyBackgroundSection {
          mainHeading
          mainHeadingTH
          companyText
          companyTextTH
        }
        managementServices {
          mainHeading
          mainHeadingTH
          contentmanagementservices {
            managementServicesImage {
              node {
                mediaItemUrl
                altText
              }
            }
            managementServicesText
            managementServicesTextTH
          }
        }
        advisoryAndConsultancySection {
          mainHeading
          mainHeadingTH
          afterHeadingText
          afterHeadingTextTH
          imageText
          imageTextTH
          image {
            node {
              mediaItemUrl
              altText
            }
          }
          ourServiceInclude {
            icon {
              node {
                mediaItemUrl
                altText
              }
            }
            heading
            headingTH
            serviceContent
            serviceContentTH
          }
        }
        managementBrandServices {
          mainHeading
          imageSection {
            heading
            content
            image {
              node {
                mediaItemUrl
                altText
              }
            }
            contentTH
            headingTH
          }
          mainHeadingTH
        }
        hospitalityServices {
          mainHeading
          imageSection {
            content
            heading
            image {
              node {
                mediaItemUrl
                altText
              }
            }
            contentTH
            headingTH
          }
          mainHeadingTH
        }
        ourLocation {
          mainHeading
          mainHeadingTH
        }
      }
    }
  }
`;
