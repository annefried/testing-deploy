import { gql } from "@apollo/client";

export const GET_NEWS_DETAILS = gql`
  query NewsData($slug: String!) {
    allNews(where: { slug: $slug }) {
      edges {
        node {
          slug
          date
          title
          titleTH {
            titleTH
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          newsFields {
            newsData {
              imagesNews {
                node {
                  mediaItemUrl
                  altText
                }
              }
              textNews
              textNewsTH
            }
            pressDownloadLink {
              url
            }
          }
        }
      }
    }
  }
`;

export const GET_NEWS_WITH_PAGINATION = gql`
  query NewsData($first: Int, $last: Int, $after: String, $before: String) {
    allNews(first: $first, last: $last, after: $after, before: $before) {
      edges {
        cursor
        node {
          date
          slug
          excerpt
          excerptTH {
            excerptTH
          }
          title
          titleTH {
            titleTH
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          newsFields {
            newsData {
              imagesNews {
                node {
                  mediaItemUrl
                  altText
                }
              }
              textNews
              textNewsTH
            }
          }
        }
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

export const GET_NEWS_PAGE_HEADER = gql`
  query NewsPage {
    page(id: "news", idType: URI) {
      newsPage {
        bannerSection {
          companyText
          companyTextTH
          bannerHeading
          bannerHeadingTH
          backgroundImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        latestNewsSection {
          newsHeading
          newsHeadingTH
          subText
          subTextTH
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
      }
    }
  }
`;

export const GET_NEWS_PAGE_META = gql`
  query GetPageMeta($id: ID!) {
    page(id: $id, idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      newsPage {
        bannerSection {
          companyText
        }
      }
    }
  }
`;
