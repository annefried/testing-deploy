import { gql } from "@apollo/client";

export const GET_DROPDOWN_ITEMS = gql`
  query GET_BOOK_MENUS {
    menu(id: "Book Now Menu", idType: NAME) {
      bookNowMenu {
        hotel_dropdown {
          country
          countryTH
          countryCode
          details {
            cityName
            cityNameTH
            cityCode
            hotelDetails {
              hotelName
              hotelCode
              hotelLink
            }
          }
        }
      }
    }
  }
`;

export const GET_DESTINATIONS = gql`
  query GET_BOOK_MENUS {
    menu(id: "Book Now Menu", idType: NAME) {
      bookNowMenu {
        hotel_dropdown {
          details {
            cityName
            cityNameTH
          }
        }
      }
    }
  }
`;

export const GET_DESTINATIONS_BY_COUNTRY = gql`
  query GET_Cities_Dropdown($parentLabel: String!) {
    menu(id: "Cities Dropdown", idType: NAME) {
      menuItems(first: 500, where: { parentLabel: $parentLabel }) {
        nodes {
          label
          parentDatabaseId
        }
      }
    }
  }
`;

export const GET_COUNTRIES = gql`
  query GET_Cities_Dropdown {
    menu(id: "Cities Dropdown", idType: NAME) {
      menuItems(first: 500, where: { parentDatabaseId: 0 }) {
        nodes {
          label
        }
      }
    }
  }
`;

export const GET_HOTELS = gql`
  query GetHotelsList($first: Int!) {
    hotels(first: $first, after: null) {
      edges {
        node {
          title
          slug
        }
      }
    }
  }
`;

// export const GET_CATEGORY = gql`
//   query CategoryResturant {
//     menu(id: "Category Menu Resturants", idType: NAME) {
//       menuItems {
//         edges {
//           node {
//             label
//           }
//         }
//       }
//     }
//   }
// `;

export const GET_EVENT_CATEGORY_NAME = gql`
  query GetMenuByName {
    menuItems(where: { location: CATEGORY_MENU_EVENTS }) {
      edges {
        node {
          label
        }
      }
    }
  }
`;

export const GET_DESTINATION_DROPDOWN = gql`
  query GET_Cities_Dropdown {
    menu(id: "Cities Dropdown", idType: NAME) {
      databaseId
      name
      menuItems(first: 500, where: {}) {
        nodes {
          databaseId
          label
          parentId
          menuItemId
          parentDatabaseId
        }
      }
    }
  }
`;
