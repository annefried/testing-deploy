import { gql } from "@apollo/client";

export const GET_SPA_ITEM = gql`
  query SpaPage {
    page(id: "spa", idType: URI) {
      seo {
        metaDesc
        focuskw
        title
      }
      seoForTh {
        focuskw
        metadesc
        title
      }
      spaTreatmentPage {
        bannerSection {
          heading
          headingTH
          subHeading
          subHeadingTH
          backgroundImage {
            node {
              mediaItemUrl
              altText
            }
          }
        }
        wellnessSection {
          mainHeading
          mainHeadingTH
          content {
            heading
            headingTH
            text
            textTH
            images {
              nodes {
                mediaItemUrl
                altText
              }
            }
          }
        }
        sectionExploreTreatments {
          heading
          headingTH
          subText
          subTextTH
        }
        sectionServicesYouMayLike {
          heading
          subText
          headingTH
          subTextTH
        }
      }
      slug
    }
  }
`;

export const GET_SPA_SERVICE_HEADER = gql`
  query SpaPage {
    page(id: "spa", idType: URI) {
      spaTreatmentPage {
        sectionServicesYouMayLike {
          heading
          subText
          headingTH
          subTextTH
        }
      }
      slug
    }
  }
`;

export const GET_SPA_DETAIL = (obj: {
  destination?: string | null;
  slug?: string | null;
}) => gql`
  query SpaDetailsPage {
    spaServices (
      where: {
        ${obj.slug ? `slug:"${obj.slug}"` : ""}
        taxQuery: {
          taxArray: [
            ${
              obj.destination
                ? `{
              taxonomy: SERVICEDESTINATIONS
              terms: "${obj.destination}"
              field: SLUG
              operator: AND
            }`
                : ""
            }
          ]
        }
      },
      first: 10000
    ){
      edges {
        node {
          slug
          title
          titleTH {
            titleTH
          }
          featuredImage {
            node {
              mediaItemUrl
              altText
            }
          }
          spaServiceDetailsFields {
            serviceDetails {
              serviceImages {
                edges {
                  node {
                    mediaItemUrl
                    altText
                  }
                }
              }
              introductionText
              introductionTextTH
              highlightSection {
                content
                contentTH
              }
              price {
                edges {
                  node {
                    name
                  }
                }
              }
              location
              locationTH
              selectDestination {
                edges {
                  node {
                    name
                    description
                  }
                }
              }
              selectServiceCategory {
                edges {
                  node {
                    name
                  }
                }
              }
              moreInformationButtonUrl {
                url
              }
            }
          }
        }
      }
    }
  }
`;
