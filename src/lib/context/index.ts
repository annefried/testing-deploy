import type { AppContextType } from "@/interfaces/global_interfaces";
import { createContext } from "react";

const defaultValue: AppContextType = {
  state: {
    showBookNow: false,
    localeData: null,
    discoverRef: null,
    hotelPayload: null,
  },
  dispatch: {
    setShowBookNow: () => {},
    handleBookNow: () => {},
    handleCloseBookNow: () => {},
    scrollToSection: () => {},
    setHotelPayload: () => {},
  },
};

const AppContext = createContext<AppContextType>(defaultValue);

export default AppContext;
