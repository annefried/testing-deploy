import { useState, useContext } from "react";
import { useParams, useSelectedLayoutSegment } from "next/navigation";
import Link from "next/link";
import Picker from "../DatePicker/datePicker";
import type { DateValueType } from "react-tailwindcss-datepicker";
import CustomAsyncSelect from "../Select/asyncSelect";
import type {
  AppContextType,
  DropdownApiResponseProps,
  OptionsType,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import { GET_DROPDOWN_ITEMS } from "@/lib/queries/other";
import moment from "moment";
import type { InputActionMeta } from "react-select";
import { searchMenuItems } from "@/helpers/bookNow";
import client from "@/lib/apollo/client";
import Button from "../Button/button";

const Booking = () => {
  const locale = useParams()?.lang;
  const currentPath = useSelectedLayoutSegment();
  const { state }: AppContextType = useContext(AppContext);
  const [inputText, setInputText] = useState<string>("");
  const states = state?.localeData?.bookNow;
  const [options, setOptions] = useState<any>({
    optionsLoaded: false,
    options: [],
    isLoading: false,
  });
  const [previousOptions, setPreviousOptions] = useState<any>({
    options: [],
  });
  const [destination, setDestination] = useState<string | any>();
  const date = new Date();
  const endDate = new Date(date);
  endDate.setDate(date.getDate() + 1);

  const [value, setValue] = useState<DateValueType | any>({
    startDate: date,
    endDate: endDate,
  });

  let url = `${process.env.NEXT_PUBLIC_BOOKING_URL}onlineId=5&pgroup=FO205HL3&lang=${locale}`;
  let bookNowUrl;
  const formattedEnDate = moment(value.endDate).format("YYYY-MM-DD");
  const formattedStartDate = moment(value.startDate).format("YYYY-MM-DD");
  if (destination?.type === "hotel" && destination?.value) {
    bookNowUrl = `${process.env.NEXT_PUBLIC_HOTEL_URL}propertyId=${destination?.value}&onlineId=8&checkout=${formattedEnDate}&checkin=${formattedStartDate}&lang=${locale}&pgroup=FO205HL3`;
  } else if (destination?.type === "hotel" && !destination?.value) {
    let link = destination?.link === "#" ? url : destination?.link;
    bookNowUrl = !link.startsWith("https://") ? `https://${link}` : link;
  } else if (
    state?.hotelPayload?.url &&
    state?.hotelPayload?.url?.includes("reservation.compasshospitality.com")
  ) {
    bookNowUrl = `${state?.hotelPayload?.url}&checkout=${formattedEnDate}&checkin=${formattedStartDate}`;
  } else {
    bookNowUrl = `${url}&checkout=${formattedEnDate}&checkin=${formattedStartDate}`;
  }

  const fetchOptions = async () => {
    const response: { data: DropdownApiResponseProps } | any =
      await client.query({
        query: GET_DROPDOWN_ITEMS,
      });
    const data: DropdownApiResponseProps | any =
      response &&
      response?.data?.menu?.bookNowMenu?.hotel_dropdown?.filter(
        (k: any) => k.details !== null
      );

    const convertPayload = data?.map(
      (country: { country: string; countryCode: string; details: any[] }) => {
        return {
          label: country.country,
          type: "country",
          options:
            country?.details &&
            country?.details?.length !== 0 &&
            country?.details?.map((city) => {
              return {
                label: city?.cityName,
                type: "city",
                options: city?.hotelDetails
                  ? city?.hotelDetails.map(
                      (hotel: {
                        hotelName: string;
                        hotelCode: string;
                        hotelLink: string;
                      }) => {
                        return {
                          label: hotel?.hotelName,
                          value: hotel?.hotelCode ? hotel?.hotelCode : null,
                          type: "hotel",
                          link: hotel?.hotelLink,
                        };
                      }
                    )
                  : [],
              };
            }),
        };
      }
    );
    setPreviousOptions({
      options: convertPayload ?? [],
    });
    setOptions({
      optionsLoaded: true,
      options: convertPayload ?? [],
      isLoading: false,
    });
  };

  const mayLoadOptions = (
    options: OptionsType | any,
    setOptions: React.Dispatch<React.SetStateAction<OptionsType>> | any,
    fetchFunction: () => void
  ): void => {
    if (!options.optionsLoaded) {
      setOptions((prevOptions: any) => ({ ...prevOptions, isLoading: true }));
      fetchFunction();
    }
  };

  const handleInputChange = (inputText: string, meta: InputActionMeta) => {
    if (meta.action !== "input-blur" && meta.action !== "menu-close") {
      setInputText(inputText);
      if (inputText && inputText.trim() !== "") {
        const modifiedResults =
          searchMenuItems(previousOptions.options, inputText) || [];
        const filteredResults = modifiedResults
          ?.map((k) => ({
            ...k,
            options: k.options?.filter(
              (option: { label: string }) => option.label
            ),
          }))
          ?.map((k) => ({
            ...k,
            options: k.options?.filter(
              (option: { label: string }, index: number, self: any[]) =>
                index === self.findIndex((o) => o.label === option.label)
            ),
          }));
        setOptions({
          optionsLoaded: true,
          options: filteredResults,
          isLoading: false,
        });
      } else {
        setOptions({
          optionsLoaded: true,
          options: previousOptions.options,
          isLoading: false,
        });
      }
    }
  };

  return (
    <section className="absolute w-full rounded bg-white p-4 font-body text-t-subtitle-2 text-text-main shadow-md md:p-6 lg:left-auto lg:right-[23px] lg:mx-0 lg:mx-0 lg:my-4 lg:w-1/2 xl:right-[60px]">
      <div className="flex flex-col lg:flex-row">
        <section className="w-full lg:w-1/2">
          <label
            htmlFor="date"
            className="mb-3 block font-body text-t-subtitle-2 text-text-main"
          >
            {states?.fields?.destination?.label}
          </label>
          {currentPath === "hotels" && state?.hotelPayload ? (
            <div className="rounded-md border border-secondary-300 bg-white p-3 font-body text-t-subtitle-2 text-gray-900">
              <p
                className={`${state?.hotelPayload?.title?.length >= 40 ? "book-now" : ""}`}
              >
                {state?.hotelPayload?.title}
              </p>
            </div>
          ) : (
            <CustomAsyncSelect
              placeholder={states?.fields?.destination?.placeholder}
              options={options.options}
              filterOption={null}
              inputValue={inputText}
              onInputChange={handleInputChange}
              onFocus={() => mayLoadOptions(options, setOptions, fetchOptions)}
              isLoading={options.isLoading}
              value={destination}
              onChange={(e: { value: string; label: string; type: string }) =>
                setDestination(e)
              }
            />
          )}
        </section>
        <section className="ml-0 w-full lg:ml-3 lg:w-1/2">
          <label
            htmlFor="date"
            className="mb-3 mt-3 block font-body text-t-subtitle-2 text-text-main lg:mt-0"
          >
            {states?.fields?.CheckInCheckOut?.label}
          </label>
          <Picker
            value={value}
            setValue={setValue}
            lang={locale}
            placeholder={states?.fields?.CheckInCheckOut?.placeholder}
          />
        </section>
      </div>
      <section className="flex flex-col">
        <section>
          <Link
            href={url}
            target="blank"
            className=" float-right  my-4 border-b-[1px] border-b-secondary-500"
          >
            {states?.flexPeriodText}
          </Link>
        </section>

        <Link className="mt-2 w-full" target="blank" href={bookNowUrl}>
          <Button
            className="hover-effect width-full focus-btn uppercase"
            shadow="shadow-md"
          >
            {state?.localeData?.navbar?.bookNowText}
          </Button>
        </Link>
      </section>
    </section>
  );
};

export default Booking;
