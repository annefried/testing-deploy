const Loader = () => {
  return (
    <div className="flex gap-2">
      <div className="h-4 w-4 animate-pulse rounded-full bg-primary-main"></div>
      <div className="h-4 w-4 animate-pulse rounded-full bg-primary-main"></div>
      <div className="h-4 w-4 animate-pulse rounded-full bg-primary-main"></div>
    </div>
  );
};

export default Loader;
