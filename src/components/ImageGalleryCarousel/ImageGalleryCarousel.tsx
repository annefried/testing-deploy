"use client";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";
import type { BannerItem } from "@/interfaces/carousel.interface";
import Image from "next/image";
export default function ImageGalleryCarousel({ payload }: { payload: any }) {
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map((item: BannerItem, index: number) => (
      <div
        key={index}
        className="relative max-h-[300px] min-h-[300px] w-full flex-shrink-0 overflow-hidden md:max-h-[400px] md:min-h-[400px] lg:max-h-[405px] lg:min-h-[405px] xl:max-h-[380px] xl:min-h-[380px] "
      >
        <Image
          src={item?.node?.src?.src ?? item?.node?.src}
          width={400}
          height={400}
          alt="image"
          className="max-h-[300px] min-h-[300px] w-full object-cover md:max-h-[400px] md:min-h-[400px] lg:max-h-[405px] lg:min-h-[405px] xl:max-h-[380px] xl:min-h-[380px] "
        />
      </div>
    ));
  return (
    <div>
      <ResponsiveCarousel items={items} centerMode={false} payload={payload} />
    </div>
  );
}
