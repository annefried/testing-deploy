"use client";
import type {
  FooterMenuItems,
  LocaleType,
  ParentItem,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import { useParams, usePathname } from "next/navigation";
import type { RefObject } from "react";
import { useEffect, useRef, useState } from "react";
import { ToastContainer } from "react-toastify";
import Footer from "./Footer/footer";
import Navbar from "./Navbar/navbar";

const locales: LocaleType = {
  en: () => import("@/components/Locales/en.json").then((r) => r.default),
  th: () => import("@/components/Locales/th.json").then((r) => r.default),
};

const getLocaleData = (lang: keyof LocaleType | string | string[] | any) => {
  return locales[lang as keyof LocaleType]();
};

interface WrapperProps {
  children: React.ReactNode;
  menus: { menuItems: { nodes: ParentItem[] } };
  footerMenus: FooterMenuItems;
  lang?: string;
}

const Wrapper: React.FC<WrapperProps> = ({
  children,
  menus,
  footerMenus,
  lang,
}) => {
  const discoverRef = useRef();
  const params = useParams();
  const pathName = usePathname();
  const [hotelPayload, setHotelPayload] = useState(null);
  const [currentLocaleData, setCurrentLocaleData] = useState<LocaleType | null>(
    null
  );
  const [showBookNow, setShowBookNow] = useState<boolean>(false);

  useEffect(() => {
    const fetchLocaleData = async () => {
      try {
        const data = await getLocaleData(params?.lang ? params.lang : lang);
        setCurrentLocaleData(data);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error("Error fetching locale data:", error);
      }
    };

    fetchLocaleData();
  }, [params?.lang]);

  const handleBookNow = () => {
    setShowBookNow((prev) => !prev);
  };

  const handleCloseBookNow = (value: boolean) => {
    setShowBookNow(value);
  };

  const scrollToSection = (sectionRef: RefObject<HTMLDivElement>) => {
    if (sectionRef && sectionRef.current) {
      sectionRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };

  return (
    <AppContext.Provider
      value={{
        state: {
          showBookNow,
          localeData: currentLocaleData,
          discoverRef,
          hotelPayload,
        },
        dispatch: {
          handleBookNow,
          handleCloseBookNow,
          setShowBookNow,
          scrollToSection,
          setHotelPayload,
        },
      }}
    >
      <Navbar payload={menus?.menuItems?.nodes} locale={lang} />
      <div>{children}</div>
      {pathName !== `/${params?.lang}`}
      <Footer footerMenus={footerMenus} locale={lang} />
      <ToastContainer
        autoClose={5000}
        closeOnClick={true}
        newestOnTop={true}
        position="top-right"
        theme="light"
      />
    </AppContext.Provider>
  );
};

export default Wrapper;
