"use client";
import { useContext } from "react";
import LightArrowRight from "@/components/icons/LightArrowRight.svg";
import Link from "next/link";
import type { AppContextType, HotelItem } from "@/interfaces/global_interfaces";
import Image from "next/image";
import AppContext from "@/lib/context";

const SwapNavbar = ({
  hotelListMenu,
  setHotelListMenu,
  filteredCities,
  setChangeColor,
}: any) => {
  const { state }: AppContextType = useContext(AppContext);
  return (
    <div className="swap-list mx-12 my-6">
      <section
        className="item-center flex w-[15%] cursor-pointer"
        onClick={() => {
          setChangeColor(null);
          setHotelListMenu(null);
        }}
      >
        {" "}
        <section>
          <Image
            src={LightArrowRight}
            width={6}
            height={9}
            className="h-auto w-full"
            alt="icon"
          />
        </section>
        <section className="ml-3">
          <h4>{state?.localeData?.navbar?.destinationText}</h4>{" "}
        </section>
      </section>{" "}
      {hotelListMenu && filteredCities?.length !== 0 && (
        <div className="ml-8 mt-4 flex">
          <Link
            href={filteredCities[0]?.filter(Boolean)[0]?.uri}
            className="mt-2 font-bold"
          >
            {filteredCities[0]?.filter(Boolean)[0]?.label}
          </Link>
          <section className=" border-left-secondary-300 ml-12 border-l-[1px] pl-4">
            {hotelListMenu &&
              filteredCities?.length !== 0 &&
              filteredCities[0]
                ?.filter(Boolean)[0]
                ?.hotels?.map((item: HotelItem, i: number) => {
                  return (
                    <section className="my-2" key={i}>
                      <Link href={item.uri} className="hover:text-primary-main">
                        {item?.label}
                      </Link>
                    </section>
                  );
                })}
          </section>
        </div>
      )}
    </div>
  );
};

export default SwapNavbar;
