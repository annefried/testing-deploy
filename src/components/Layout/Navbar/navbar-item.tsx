"use client";
import InputField from "@/components/InputField/input";
import { ArrowUp } from "@/components/icons/arrows";
import hotelCheck from "@/helpers/hotelCheck";
import type {
  AppContextType,
  ChildItem,
  CityItem,
  MenuProps,
  ParentItem,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import SwapNavbar from "./swap-navbar-ui";

const NavbarItem = ({
  item,
  isDropdownOpen,
  handleMouseEnter,
  setSubChildMenu,
  subChildMenu,
  filteredResultsWithHotels,
  filteredResultsWithChildrenButNoCities,
  handleChange,
  searchKey,
  handleMouseLeave,
}: MenuProps) => {
  const { state }: AppContextType = useContext(AppContext);
  const [hotelListMenu, setHotelListMenu] = useState<string | null>(null);
  const [changeColor, setChangeColor] = useState<string | null>(null);
  const activePath = usePathname();
  const hotels = hotelCheck(filteredResultsWithHotels, item);
  const noHotels =
    filteredResultsWithChildrenButNoCities?.length !== 0 &&
    filteredResultsWithHotels?.some((k: ParentItem) => k?.label !== item.label);
  const showMenu =
    (isDropdownOpen === item.id && item?.children?.length !== 0) ||
    (isDropdownOpen === item.id && hotels);

  useEffect(() => {
    setHotelListMenu(activePath);
    setChangeColor(null);
  }, []);

  useEffect(() => {
    setHotelListMenu(activePath);
  }, [activePath]);

  const handleMenuState = (id: number) => {
    if (isDropdownOpen === id) {
      handleMouseLeave();
    } else {
      handleMouseEnter(id);
    }
  };

  const filteredCities = hotelListMenu
    ? item?.children
        .map((child: { cities: ChildItem[] }) =>
          child.cities?.filter((city) => city.uri === hotelListMenu)
        )
        .filter((cities: ChildItem[]) => cities && cities.length > 0)
    : [];
  if (hotels || (item.children && item.children.length > 0)) {
    return (
      <div
        className="mx-3 font-body text-t-subtitle-2 text-text-main xl:mx-8"
        key={item.id}
      >
        <div>
          <button
            type="button"
            className="relative flex rounded-full bg-transparent outline-none"
            onClick={() => handleMenuState(item.id)}
          >
            <span className="absolute -inset-1.5"></span>
            <span className="sr-only">Open child menu</span>
            <div className="flex items-center justify-center px-2">
              <Link
                href="#"
                className={`${showMenu ? "border-b border-text-main" : ""} mr-2`}
                aria-current="page"
              >
                {item.label}
              </Link>
              <ArrowUp />
            </div>
          </button>
        </div>
        <div
          className={`rounded-xs shadow-xs menu absolute right-0 z-10 mt-0 h-auto w-full origin-top-right bg-white px-[1rem] pt-4 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none lg:mt-[1.4rem] ${showMenu ? "block" : "hidden"}`}
        >
          {hotelListMenu &&
          filteredCities[0] &&
          filteredCities[0]?.filter(Boolean)[0]?.hotels?.length !== 0 &&
          hotels ? (
            <SwapNavbar
              filteredCities={filteredCities}
              setHotelListMenu={setHotelListMenu}
              hotelListMenu={hotelListMenu}
              noResultsFound={state?.localeData?.noResultsFound}
              setChangeColor={setChangeColor}
            />
          ) : (
            <div className={`${!hotels && "flex justify-end py-3"}`}>
              {" "}
              {hotels && (
                <section className="mx-12 my-6 w-1/3">
                  <InputField
                    value={searchKey}
                    inputName="search"
                    handleChange={handleChange}
                    placeholder={state?.localeData?.navbar?.searchField}
                    className="px-3 py-2"
                  />
                </section>
              )}
              {hotels && item?.children?.length === 0 && (
                <div className="item-center flex justify-center py-6">
                  <h4>{state?.localeData?.noResultsFound}</h4>
                </div>
              )}
              <div
                className={` ${showMenu ? `flex flex-wrap` : "hidden"}`}
                role="menu"
                aria-orientation="vertical"
                aria-labelledby="user-menu-button"
                tabIndex={-1}
                key={`${item.id} ${item.label}`}
              >
                {item.children.map((child: any) =>
                  child.cities && child.cities.length > 0 && noHotels ? (
                    <div
                      onMouseEnter={() => setSubChildMenu(child.id)}
                      key={`${child.id} ${child.label}`}
                      className={`flex-basis-0 flex-grow-1 relative my-2 mr-4 py-2`}
                    >
                      <button
                        type="button"
                        className={`flex rounded-full bg-transparent outline-none  ${subChildMenu === child.id || (activePath === child?.uri && "text-primary-main")}`}
                        id="user-menu-button"
                        aria-expanded="false"
                        aria-haspopup="true"
                      >
                        <div className="flex items-center justify-center px-2">
                          <Link
                            href={child.uri}
                            className={`mr-2 ${activePath === child?.uri && "text-primary-main"}`}
                            aria-current="page"
                          >
                            {child.label}
                          </Link>
                          <ArrowUp />
                        </div>
                      </button>{" "}
                      <div
                        className={`absolute z-10 mt-6 h-auto w-48 origin-top-right rounded-md bg-white shadow-lg shadow-md ring-1 ring-black ring-opacity-5 focus:outline-none ${subChildMenu === child.id ? "block" : "hidden"}`}
                        role="menu"
                        aria-orientation="vertical"
                        aria-labelledby="user-menu-button"
                        tabIndex={-1}
                      >
                        {child?.cities.map((subChild: CityItem, i: number) => (
                          <div
                            key={i}
                            className="py-[1rem] hover:text-primary-main"
                          >
                            <button
                              type="button"
                              className="relative flex rounded-full bg-transparent outline-none"
                              id="user-menu-button"
                              aria-expanded="false"
                              aria-haspopup="true"
                            >
                              <div className="flex items-start justify-start px-2">
                                <Link
                                  href={subChild.uri.replace(
                                    "/destination",
                                    ""
                                  )}
                                  className={`mr-2 text-left hover:text-primary-main ${activePath === subChild?.uri && "text-primary-main"}`}
                                  aria-current="page"
                                >
                                  {subChild.label}
                                </Link>
                              </div>
                            </button>
                          </div>
                        ))}
                      </div>
                    </div>
                  ) : child.cities && child.cities.length > 0 && hotels ? (
                    <div
                      className={`flex-basis-0 flex-grow-1 my-2 w-[25%] px-4 py-2 text-left`}
                      key={`${child.id} ${child.label}-cities`}
                    >
                      {" "}
                      <Link
                        href={child.uri}
                        role="menuitem"
                        className={`mx-8 ${activePath === child?.uri && "text-primary-main"} mb-4 font-[600]`}
                        tabIndex={-1}
                      >
                        {child?.label}
                      </Link>
                      <div className="mx-2 mt-2 grid grid-cols-2">
                        {child?.cities.map((subChild: CityItem, i: number) => (
                          <div
                            onClick={() => setHotelListMenu(subChild?.uri)}
                            key={i}
                            className="mx-6 flex cursor-pointer py-2"
                          >
                            <section
                              className={`flex-1 text-left hover:text-primary-main ${activePath === subChild?.uri && "text-primary-main"}`}
                              aria-current="page"
                            >
                              {subChild.label}
                            </section>
                            {subChild?.hotels?.length !== 0 && (
                              <section
                                onMouseEnter={() =>
                                  setChangeColor(subChild?.uri)
                                }
                                onMouseLeave={() => setChangeColor(null)}
                                className={`${subChild?.uri === changeColor ? "hovered-icon" : "un-hovered-icon"} mt-[7px] px-2`}
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="6"
                                  height="9"
                                  viewBox="0 0 6 9"
                                  fill="none"
                                >
                                  <path d="M3.78133 4.49981L0.481335 1.19981L1.424 0.257812L5.66667 4.49981L1.424 8.74248L0.481335 7.79915L3.78133 4.49981Z" />
                                </svg>
                              </section>
                            )}{" "}
                          </div>
                        ))}
                      </div>
                    </div>
                  ) : (
                    <Link
                      key={`${child.id} ${child.label}-child`}
                      href={child?.uri}
                      className={`flex-basis-0 ${activePath === child?.uri && "text-primary-main"} flex-grow-1 mx-[2.38rem] my-2 py-2 text-left ${showMenu && "hover:text-primary-main"}`}
                      role="menuitem"
                      tabIndex={-1}
                      onMouseEnter={() => setSubChildMenu(null)}
                    >
                      {child?.label}
                    </Link>
                  )
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  } else {
    return (
      <Link
        href={item.uri}
        className={`font-t-subtitle-2 mx-6 xl:mx-8 ${activePath === item.uri && "text-primary-main"}  ${isDropdownOpen === item.id && "hover:text-primary-main"}`}
        aria-current="page"
        onClick={() => handleMenuState(item.id)}
      >
        {item.label}
      </Link>
    );
  }
};

export default NavbarItem;
