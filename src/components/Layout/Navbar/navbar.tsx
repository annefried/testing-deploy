"use client";
import Booking from "@/components/Booking/booking";
import Button from "@/components/Button/button";
import Logo from "@/components/Images/Logo.png";
import InputField from "@/components/InputField/input";
import { ArrowLeft } from "@/components/icons/arrows";
import hotelCheck from "@/helpers/hotelCheck";
import { searchMenuItems } from "@/helpers/search";
import type {
  AppContextType,
  ChildItem,
  CityItem,
  HotelItem,
  ParentItem,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import dynamic from "next/dynamic";
import Image from "next/image";
import Link from "next/link";
import {
  useParams,
  usePathname,
  useRouter,
  useSelectedLayoutSegment,
} from "next/navigation";
import { useContext, useEffect, useRef, useState } from "react";
import { useMediaQuery } from "react-responsive";
import SwapNavbar from "../MobileNavbar/swap-navbar-ui";
import LocaleSwitcher from "../Switcher/locale-switcher";
import NavbarItem from "./navbar-item";

const MobileNavbarItem = dynamic(
  () => import("../MobileNavbar/mobile-navbar-item"),
  {
    ssr: false,
  }
);

const Navbar = ({ payload, locale }: any) => {
  const navbarRef = useRef<HTMLDivElement>(null);
  const mobileScreen = useMediaQuery({ maxWidth: 767 });
  const lang = useParams()?.lang ?? locale;
  const router = useRouter();
  const currentPath = useSelectedLayoutSegment();
  const { dispatch, state }: AppContextType = useContext(AppContext);
  const bookNowBtn = lang === "en" ? "Book Now" : "จองตอนนี้";
  const closeBtn = lang === "en" ? "Close" : "ปิด";
  const languageMenuItem: ChildItem | any = {
    label: lang,
    parentId: 0,
    uri: "#",
    id: 27774,
    children: [
      {
        label: "English",
        uri: `/en`,
        id: 1,
        parentId: 27774,
      },
      {
        label: "Thai",
        uri: `/th`,
        id: 2,
        parentId: 27774,
      },
    ],
    lang: true,
    heading: state?.localeData?.navbar?.languageHeading,
    databaseId: 0,
  };

  const menuWithNestedChildItems: ParentItem[] =
    payload
      ?.filter((item: ParentItem) => item.parentDatabaseId === 0)
      ?.map((parentItem: ParentItem) => {
        const childItems: ChildItem[] =
          payload
            ?.filter(
              (item: ParentItem) =>
                item.parentDatabaseId === parentItem?.databaseId
            )
            ?.map((childItem: ChildItem) => {
              const cities: CityItem[] =
                payload
                  ?.filter(
                    (cityItem: CityItem) =>
                      cityItem.parentDatabaseId === childItem.databaseId
                  )
                  ?.map((city: CityItem) => {
                    const hotels: HotelItem[] =
                      payload
                        ?.filter(
                          (hotelItem: HotelItem) =>
                            hotelItem.parentDatabaseId === city.databaseId
                        )
                        ?.map((hotel: HotelItem) => ({
                          ...hotel,
                          uri: `/${lang}${hotel.uri}`,
                        })) || [];

                    return {
                      label: city.label,
                      id: city.databaseId,
                      uri: `/${lang}${city.uri}`,
                      hotels: hotels.length !== 0 ? hotels : [],
                    };
                  }) || [];

              return {
                label: childItem.label,
                id: childItem.databaseId,
                uri: `/${lang}${childItem.uri}`,
                cities: cities.length !== 0 ? cities : [],
              };
            }) || [];

        return {
          parentId: parentItem?.parentDatabaseId,
          id: parentItem?.databaseId,
          uri: `/${lang}${parentItem.uri}` || null,
          label: parentItem.label,
          children: childItems.length !== 0 ? childItems : [],
        };
      }) || [];

  menuWithNestedChildItems.push(languageMenuItem);
  const pathName = usePathname();
  const [searchKey, setSearchKey] = useState<string | null>("");
  const [isMobileMenuOpen, setMobileMenuOpen] = useState<boolean>(false);
  const [isScrolled, setIsScrolled] = useState(false);
  const [subChildMenu, setSubChildMenu] = useState<number | null>(null);
  const [isDropdownOpen, setDropdownOpen] = useState<number | null>(null);
  const [filteredResults, setFilteredResults] = useState(
    menuWithNestedChildItems
  );

  const handleMouseEnter = (id: number) => {
    setDropdownOpen(id);
    dispatch.setShowBookNow(false);
  };

  const clearSearchState = () => {
    setSearchKey("");
    setFilteredResults(menuWithNestedChildItems);
  };

  const handleMouseLeave = () => {
    clearSearchState();
    if (!isMobileMenuOpen) {
      setDropdownOpen(null);
      setSubChildMenu(null);
    }
  };

  const handleMobileMenuToggle = () => {
    setMobileMenuOpen(!isMobileMenuOpen);
    clearSearchState();
    setDropdownOpen(null);
    dispatch.setShowBookNow(false);
  };

  const handleHideMobileMenu = () => {
    if (window.innerWidth > 1000) {
      setMobileMenuOpen(false);
      clearSearchState();
    }
  };

  const handleBookNow = () => {
    if (
      currentPath === "hotels" &&
      state?.hotelPayload?.url &&
      !state?.hotelPayload?.url?.includes("reservation.compasshospitality.com")
    ) {
      router.push(state?.hotelPayload?.url);
    } else {
      dispatch.handleBookNow();
      setMobileMenuOpen(false);
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      setIsScrolled(window.pageYOffset > 120);
    };

    window.addEventListener("scroll", handleScroll);

    handleScroll();

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    setDropdownOpen(null);
    setMobileMenuOpen(false);
  }, [pathName]);

  useEffect(() => {
    window.addEventListener("resize", handleHideMobileMenu);
    handleHideMobileMenu();
    return () => window.removeEventListener("resize", handleHideMobileMenu);
  }, []);

  const handleClickOutside = (event: MouseEvent) => {
    if (
      navbarRef.current &&
      !navbarRef.current.contains(event.target as Node)
    ) {
      handleMouseLeave();
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const filteredResultsWithHotels = menuWithNestedChildItems
    ? menuWithNestedChildItems.filter((o: ParentItem) =>
        o.children?.some((u) =>
          u.cities?.some((city) => (city.hotels || []).length > 0)
        )
      )
    : [];

  const filteredResultsWithChildrenButNoCities = menuWithNestedChildItems
    ? menuWithNestedChildItems.filter((o: ParentItem) => {
        const hasChildrenButNoCities = (o.children || []).some(
          (u) => !u.cities || u.cities.length === 0
        );

        return hasChildrenButNoCities;
      })
    : [];

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newSearchKey = e.target.value;
    setSearchKey(newSearchKey);

    const filteredResultsWithChildrenZero = menuWithNestedChildItems
      ? menuWithNestedChildItems.filter(
          (o: ParentItem) => (o.children || []).length === 0
        )
      : [];

    const modifiedResults =
      searchMenuItems(filteredResultsWithHotels, newSearchKey) || [];
    const templateObject =
      filteredResultsWithHotels?.map((o: ParentItem) => ({
        ...o,
        children: [],
      })) || [];

    const result =
      modifiedResults.length > 0 ? modifiedResults : templateObject;
    const mergedResults = [
      ...result,
      ...filteredResultsWithChildrenButNoCities,
      ...filteredResultsWithChildrenZero,
    ];
    if (newSearchKey && newSearchKey.trim() !== "") {
      setFilteredResults(mergedResults);
    } else {
      setFilteredResults(menuWithNestedChildItems);
    }
  };

  return (
    <nav
      ref={navbarRef}
      className={`${mobileScreen || isScrolled ? "fixed" : "relative"} z-30 ${isScrolled ? "shadow-md" : ""} w-full cursor-pointer bg-white font-body text-t-subtitle-2 text-text-main`}
    >
      <div className="relative z-30 pt-0 xs:py-[3px]">
        <div className="mx-auto flex h-16 items-center justify-between px-2 sm:px-6 xl:px-[4rem]">
          <div className="flex flex-1 items-center justify-between">
            <div onClick={handleMouseLeave}>
              <Link
                href={`/${lang}`}
                className="flex flex-shrink-0 items-center"
              >
                <Image
                  src={Logo}
                  width={165}
                  height={44}
                  className="w-[100px]  xs:w-[140px]"
                  alt="logo"
                  priority
                />
              </Link>
            </div>
            <div className="flex items-center lg:hidden">
              <div>
                {state?.localeData ? (
                  <Button
                    className="hover-effect custom-background width-sm lg:width-md xl:width-md foucs-btn"
                    shadow="shadow-md"
                    onClick={handleBookNow}
                  >
                    {!state?.showBookNow
                      ? state?.localeData?.navbar?.bookNowText
                      : state?.localeData?.navbar?.closeText}
                  </Button>
                ) : (
                  <Button
                    className="hover-effect width-sm lg:width-md xl:width-md focus-btn"
                    shadow="shadow-md"
                    onClick={handleBookNow}
                  >
                    {!state?.showBookNow ? bookNowBtn : closeBtn}
                  </Button>
                )}
              </div>
              <div className="ml-4 flex">
                <button
                  className={` ${isMobileMenuOpen ? "checked" : ""}`}
                  onClick={handleMobileMenuToggle}
                >
                  <div className="hamburger-lines flex h-[18px] w-[25px] flex-col justify-between xs:h-[26px] xs:w-[32px]">
                    <span className="line line1"></span>
                    <span
                      className={`line line2 ${!isMobileMenuOpen ? "my-[2px]" : "my-0"}`}
                    ></span>
                    <span
                      className={`line line3 ${!isMobileMenuOpen ? "my-[1px]" : "my-0"}`}
                    ></span>
                  </div>
                </button>
              </div>
            </div>
            <div className="hidden lg:ml-6 lg:flex lg:items-center">
              {filteredResults &&
                filteredResults
                  .filter((item: ParentItem) => !item.lang)
                  .map((item: ParentItem, index: number) => (
                    <div key={index}>
                      <NavbarItem
                        item={item}
                        isDropdownOpen={isDropdownOpen}
                        handleMouseEnter={handleMouseEnter}
                        handleMouseLeave={handleMouseLeave}
                        subChildMenu={subChildMenu}
                        setSubChildMenu={setSubChildMenu}
                        filteredResultsWithHotels={filteredResultsWithHotels}
                        filteredResultsWithChildrenButNoCities={
                          filteredResultsWithChildrenButNoCities
                        }
                        handleChange={handleChange}
                        searchKey={searchKey}
                      />
                    </div>
                  ))}
            </div>
          </div>
          <div className="hidden lg:block">
            <LocaleSwitcher
              bookNowBtn={
                state && state?.localeData !== null
                  ? state?.localeData?.navbar?.bookNowText
                  : bookNowBtn
              }
              closeBtn={
                state && state?.localeData !== null
                  ? state?.localeData?.navbar?.closeText
                  : closeBtn
              }
              handleMouseLeave={handleMouseLeave}
            />
          </div>
        </div>
      </div>
      {state?.showBookNow && <Booking />}
      <div
        className={`shadow-xs h-auto border-t bg-white shadow-md lg:hidden ${isMobileMenuOpen ? "block" : "hidden"} transition-all duration-300 ease-in-out`}
        id="mobile-menu"
      >
        <div className="space-y-1 py-3">
          {filteredResults &&
            filteredResults?.map((item: ParentItem, index: number) => (
              <div key={index}>
                {isDropdownOpen ? (
                  <>
                    {isDropdownOpen === item.id && (
                      <>
                        <div
                          onClick={() => {
                            setDropdownOpen(null);
                            clearSearchState();
                          }}
                          className={`item-center mx-2 flex px-2 py-2`}
                        >
                          <button type="button" className="mt-[1px]">
                            <ArrowLeft />
                          </button>

                          <section
                            className={`${item.lang ? "ml-4 mt-[1px]" : "ml-auto mr-auto text-center"}`}
                          >
                            <h3
                              className="font-t-subtitle-2 text-center"
                              aria-current="page"
                            >
                              {item.heading ? item.heading : item.label}
                            </h3>
                          </section>
                        </div>
                        {filteredResultsWithHotels?.length !== 0 &&
                          filteredResultsWithHotels?.some(
                            (k: ParentItem) => k?.label === item.label
                          ) && (
                            <section className="m-4">
                              <InputField
                                value={searchKey}
                                inputName="search"
                                handleChange={handleChange}
                                placeholder={
                                  state?.localeData?.navbar?.searchField
                                }
                                className="px-3 py-2"
                              />
                            </section>
                          )}

                        <SwapNavbar
                          item={item}
                          isDropdownOpen={isDropdownOpen}
                          filteredResults={filteredResults}
                          setMobileMenuOpen={setMobileMenuOpen}
                          handleMobileMenuToggle={handleMobileMenuToggle}
                          hasHotels={hotelCheck(
                            filteredResultsWithHotels,
                            item
                          )}
                        />
                      </>
                    )}
                  </>
                ) : (
                  <MobileNavbarItem
                    key={index}
                    handleMobileMenuToggle={handleMobileMenuToggle}
                    handleMouseEnter={handleMouseEnter}
                    setMobileMenuOpen={setMobileMenuOpen}
                    item={item}
                    filteredResultsWithHotels={filteredResultsWithHotels}
                  />
                )}
              </div>
            ))}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
