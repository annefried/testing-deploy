import Image from "next/image";
import Link from "next/link";

import BrandLogosWithPicture1 from "@/components/icons/Footer-Brand-Logos/BrandPic1.png";
import BrandLogosWithPicture2 from "@/components/icons/Footer-Brand-Logos/BrandPic2.png";
import BrandLogosWithPicture3 from "@/components/icons/Footer-Brand-Logos/BrandPic3.png";
import BrandLogosWithPicture4 from "@/components/icons/Footer-Brand-Logos/BrandPic4.png";

import Button from "@/components/Button/button";
import { useParams } from "next/navigation";

const BrandLogosWithPicture = [
  BrandLogosWithPicture1,
  BrandLogosWithPicture2,
  BrandLogosWithPicture3,
  BrandLogosWithPicture4,
];

const BrandLogosWithPic = ({ title = "Our Brand" }: { title?: string }) => {
  const lang = useParams()?.lang;
  return (
    <div className="bg-white">
      <h1 className="flex justify-center gap-2 pt-i-90 font-display text-t-h5">
        <span className="text-center text-text-main">{title}</span>
      </h1>
      <div>
        <div className="flex flex-col pt-i-80 sm:grid sm:grid-cols-2 sm:px-i-sm xl:grid-cols-4 xl:px-0">
          {BrandLogosWithPicture.map((image, index) => (
            <Image
              key={index}
              alt={`Brand Image ${index + 1}`}
              src={image}
              style={{ width: "100%" }}
            />
          ))}
        </div>
        <div className="flex justify-center py-i-80">
          <Link href={`/${lang}/our-brand`}>
            <Button>View our brand</Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default BrandLogosWithPic;
