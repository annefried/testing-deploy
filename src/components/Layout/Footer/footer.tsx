"use client";

import Image from "next/image";
import Link from "next/link";

import AppContext from "@/lib/context";
import { useParams } from "next/navigation";
import { useContext } from "react";

import Button from "@/components/Button/button";
import ScrollTop from "@/components/ScrollTopButton/ScrollTop";

import FacebookLogo from "@/components/icons/FooterIcon/Facebook.svg";
import InstagramLogo from "@/components/icons/FooterIcon/Instagram.svg";

import MailIcon from "@/components/icons/FooterIcon/MailIcon.svg";
import PhoneIcon from "@/components/icons/FooterIcon/PhoneIcon.svg";
import LocationIcon from "@/components/icons/FooterIcon/VectorLocation.svg";

import AmericanExpressIcon from "@/components/icons/FooterIcon/AmericanExpress.svg";
import MasterCardIcon from "@/components/icons/FooterIcon/MasterCard.svg";
import UnionPayIcon from "@/components/icons/FooterIcon/UnionPay.svg";
import VisaIcon from "@/components/icons/FooterIcon/Visa.svg";

import SiteMapIcon from "@/components/icons/FooterIcon/SiteMap.svg";

import type {
  AppContextType,
  FooterMenuItems,
} from "@/interfaces/global_interfaces";

const Footer = ({
  footerMenus,
  locale,
}: {
  footerMenus: FooterMenuItems;
  locale?: string;
}) => {
  const lang = useParams()?.lang ?? locale;
  const { state }: AppContextType = useContext(AppContext);

  const desktopMenus = footerMenus.menuItems.nodes.filter(
    (menu) =>
      menu.parentId !== "cG9zdDoyODg4Mg==" &&
      menu.parentId !== "cG9zdDoyODY1MA==" &&
      menu.parentId !== null
  );

  const mobileMenus = footerMenus.menuItems.nodes.filter(
    (menu) =>
      menu.id == "cG9zdDoyODg4Mw==" ||
      menu.id == "cG9zdDoyODg4NA==" ||
      menu.id == "cG9zdDoyODg4NQ==" ||
      menu.id == "cG9zdDoyODg4Ng=="
  );

  return (
    <footer className="relative">
      {/* This section is for desktop responsiveness */}
      <div className="flex flex-col bg-black text-white">
        <div className="mb-i-80 grid justify-between px-i-xs pt-i-50 font-body xs:px-i-80 sm:mb-i-40 sm:px-i-70 md:grid-cols-3 md:flex-row lg:gap-i-sm lg:pt-i-20 xl:grid-cols-4 xl:px-i-20 xl:pt-i-50">
          <ul className="mb-i-80 flex flex-col gap-i-xs md:mb-0 ">
            <li className="font-display text-t-subtitle-2 ">
              <Link
                href={`/${lang}/about-us`}
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.aboutUs}
              </Link>
            </li>
            <li>
              <Link
                href="/about-us#background"
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.companyBackground}
              </Link>
            </li>
            <li>
              <Link
                href={`/${lang}/about-us#management`}
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.managementServices}
              </Link>
            </li>
            <li>
              <Link
                href={`/${lang}/about-us#advisory`}
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.advisory}
              </Link>
            </li>
            <li>
              <Link
                href={`/${lang}/about-us#location`}
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.ourLocation}
              </Link>
            </li>
          </ul>
          <ul className="mb-i-80 flex flex-col gap-i-xs md:mb-0">
            <li className="font-display text-t-subtitle-2">
              {state?.localeData?.footer?.menus?.mediaCenter}
            </li>
            <li>
              <Link href={`/${lang}/news`} className="hover:text-primary-main">
                {state?.localeData?.footer?.menus?.news}
              </Link>
            </li>
            <div className="flex gap-i-sm pt-i-xs">
              <Link
                href="https://www.facebook.com/CompassHospitality"
                target="_blank`"
                passHref={true}
              >
                <Image
                  alt="Facebook Icon"
                  src={FacebookLogo}
                  className="cursor-pointer"
                />
              </Link>
              <Link
                href="https://www.instagram.com/compass_hospitality/?hl=en"
                target="_blank"
                passHref={true}
              >
                <Image
                  alt="Instagram Icon"
                  src={InstagramLogo}
                  className="cursor-pointer"
                />
              </Link>
            </div>
          </ul>
          <ul className="mb-i-80 flex flex-col gap-i-xs md:mb-0">
            <li className="font-display text-t-subtitle-2">
              <Link
                href={`/${lang}/contact-us`}
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.contactUs}
              </Link>
            </li>

            <li className="flex flex-row">
              {" "}
              <Image alt="Location Vector" src={LocationIcon} />
              {state?.localeData?.footer?.menus?.location}
            </li>

            {state?.localeData?.footer?.menus?.address && (
              <div
                dangerouslySetInnerHTML={{
                  __html: state?.localeData?.footer?.menus?.address,
                }}
              ></div>
            )}

            <li className="flex flex-row gap-i-xxs">
              <Image alt="Phone Icon" src={PhoneIcon} />
              <a href="tel:+6621687667" className="hover:text-primary-main">
                {state?.localeData?.footer?.menus?.phoneNumber}
              </a>
            </li>

            <li className="flex flex-row gap-i-xxs">
              <Image alt="Mail Icon" src={MailIcon} />
              <a
                href="mailto:sales@compasshospitality.com"
                className="hover:text-primary-main"
              >
                {state?.localeData?.footer?.menus?.email}
              </a>
            </li>
          </ul>
          <div className="block md:hidden xl:block">
            <ul className="flex flex-col gap-i-xs">
              <li className="font-display text-t-subtitle-2">
                {state?.localeData?.footer?.menus?.newsLetter}
              </li>
              <Button
                className="hover-effect focus-btn active-btn w-i-10 !text-t-caption"
                shadow="shadow-md"
              >
                <Link
                  href={`/${lang}/member`}
                  className="hover-effect focus-btn active-btn"
                >
                  {state?.localeData?.footer?.menus?.subscribe}
                </Link>
              </Button>
              <li className="font-display text-t-subtitle-2">
                {state?.localeData?.footer?.menus?.payment}
              </li>
              <div className="flex gap-i-xs">
                <Image alt="Master Card" src={MasterCardIcon} />
                <Image alt="Visa" src={VisaIcon} />
                <Image alt="UnionPay" src={UnionPayIcon} />
                <Image alt="American Express" src={AmericanExpressIcon} />
              </div>
              <li className="my-2 flex items-center font-body text-t-subtitle-2">
                <Image alt="Master Card" src={SiteMapIcon} />
                <Link
                  className="ml-[2px] mt-[2px] hover:text-primary-main"
                  href="/sitemap_index.xml"
                  target="blank"
                >
                  {state?.localeData?.footer?.menus?.sitemap}
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div className="hidden xl:block">
          <div className="mb-i-40  flex flex-col justify-between px-i-20 font-body  md:flex-row">
            {state?.localeData?.footer?.menus?.compassDestination && (
              <h1
                className="w-3/12 font-display text-t-subtitle-2"
                dangerouslySetInnerHTML={{
                  __html: state?.localeData?.footer?.menus?.compassDestination,
                }}
              ></h1>
            )}

            <ul className="grid w-9/12 grid-cols-6 gap-i-xs">
              {desktopMenus.map((menu, index) => {
                const destination = menu.label
                  .replace(/hotels/gi, "")
                  .trim()
                  .toLowerCase()
                  .split(" ")
                  .join("-");

                return (
                  <li key={index}>
                    <Link
                      href={`/${lang}/destination/${destination}`}
                      className="hover:text-primary-main"
                    >
                      {menu.label}
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        {/* This section is for mobile and tablet responsiveness. */}
        <div className="block px-i-xs xs:px-i-80 sm:px-i-70 md:grid md:grid-cols-3 xl:hidden">
          <ul className="col-span-2 mb-i-sm flex flex-col gap-i-sm md:justify-between md:pb-i-90">
            <li className="font-display md:text-t-subtitle-2">
              {state?.localeData?.footer?.menus?.compassDestinationMobile}
            </li>
            {mobileMenus.map((menu, index) => {
              const destination = menu.label.toLowerCase().split(" ").join("-");

              return (
                <li key={index}>
                  <Link
                    href={`/${lang}/discover/${destination}`}
                    className="hover:text-primary-main"
                  >
                    {menu.label}
                  </Link>
                </li>
              );
            })}
          </ul>
          <div className="hidden md:block">
            <ul className="flex flex-col gap-i-xs">
              <li className="font-display text-t-subtitle-2">
                {state?.localeData?.footer?.menus?.newsLetter}
              </li>
              <Button
                className="hover-effect focus-btn w-i-10 !text-t-caption"
                shadow="shadow-md"
              >
                <Link href="/">
                  {state.localeData?.footer?.menus?.subscribe}
                </Link>
              </Button>
              <li className="font-display text-t-subtitle-2">
                {state?.localeData?.footer?.menus?.payment}
              </li>
              <div className="flex gap-i-xs">
                <Image alt="Master Card" src={MasterCardIcon} />
                <Image alt="Visa" src={VisaIcon} />
                <Image alt="UnionPay" src={UnionPayIcon} />
                <Image alt="American Express" src={AmericanExpressIcon} />
              </div>
              <li className="my-2 flex items-center font-body text-t-subtitle-2">
                <Image alt="Master Card" src={SiteMapIcon} />
                <Link
                  className="ml-[2px] mt-[2px] hover:text-primary-main"
                  href="/sitemap_index.xml"
                  target="blank"
                >
                  {state?.localeData?.footer?.menus?.sitemap}
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="bg-black  text-white">
        <div className="grid grid-cols-2 border-t-2 border-t-white px-i-xs py-i-sm text-t-caption xs:px-i-80 xs:text-t-subtitle-2 sm:grid-cols-3 sm:px-i-70  md:mx-i-70 md:px-i-sm lg:mx-i-80 xl:px-[50px]">
          <ul className="flex xl:gap-i-90">
            <li className="cursor-pointer font-body hover:text-primary-main">
              <Link href={`/${lang}/terms-and-conditions`}>
                {state?.localeData?.footer?.menus?.termsAndConditions}
              </Link>
            </li>
            {/* Desktop Responsive */}
            <li className="hidden cursor-pointer hover:text-primary-main xl:block">
              <Link href={`/${lang}/privacy-policy`}>
                {state?.localeData?.footer?.menus?.privacyPolicy}
              </Link>
            </li>
          </ul>
          <ul>
            {/* Tablet and Mobile Responsive */}
            <li className="cursor-pointer text-right font-body hover:text-primary-main md:text-center xl:hidden">
              <Link href={`/${lang}/privacy-policy`}>
                {state?.localeData?.footer?.menus?.privacyPolicy}
              </Link>
            </li>
          </ul>
          <div className="hidden font-body md:block md:text-right">
            {state?.localeData?.footer?.menus?.rightsReserved}
          </div>
        </div>
      </div>
      <ScrollTop />
    </footer>
  );
};

export default Footer;
