"use client";
import Image from "next/image";

import FootBanner from "@/components/Images/Banners/FooterBanner.svg";
import { useParams } from "next/navigation";

const FooterBanner = ({ lang }: { lang?: string }) => {
  const locale = useParams()?.lang;
  const engContent = {
    title: "Become a Compass Hospitality Hotel",
    content: `"Discover exceptional hospitality with Compass Hospitality. Our tailored solutions deliver unforgettable experiences in prime destinations across Southeast Asia, Sri Lanka, and the UK. With a diverse portfolio and commitment to sustainability, we empower you to create personalized memories that exceed expectations."`,
  };
  const thaiContent = {
    title: "Become a Compass Hospitality Hotel",
    content: `"Discover exceptional hospitality with Compass Hospitality. Our tailored solutions deliver unforgettable experiences in prime destinations across Southeast Asia, Sri Lanka, and the UK. With a diverse portfolio and commitment to sustainability, we empower you to create personalized memories that exceed expectations."`,
  };
  const content =
    (locale && locale === "en") || lang ? engContent : thaiContent;
  return (
    <div className="relative">
      <div className="relative">
        <Image
          alt="Footer Banner Image"
          src={FootBanner}
          width={2500}
          height={2500}
          className="absolute h-full w-full object-cover"
        />

        <div className="relative p-i-90 text-white sm:py-i-40 lg:px-i-30 lg:py-i-30 xl:px-i-10 xl:py-i-10">
          <div className="pb-i-xs font-display text-t-subtitle-1  hover:underline sm:text-t-h3 lg:text-t-h5">
            <a href="mailto:sales@compasshospitality.com">{content.title}</a>
          </div>
          <div className="font-body text-t-caption xs:text-t-subtitle-2 xl:text-t-h6">
            {content.content}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterBanner;
