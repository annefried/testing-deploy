"use client";
import NoresultFound from "@/components/HotelsPage/noResultsFound";
import ArrowRight from "@/components/icons/Home/RightArrow.svg";
import Marker from "@/components/icons/Marker.svg";
import type {
  AppContextType,
  QuincunxGridProps,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import Image from "next/image";
import Link from "next/link";
import { useParams } from "next/navigation";
import { useContext, useState } from "react";
import CarouselContainer from "./CarouselContainer/container";
import "./styles.scss";
const QuincunxGrid = (props: QuincunxGridProps) => {
  const lang = useParams()?.lang;
  const { state }: AppContextType = useContext(AppContext);
  const { payload, urlText, hideArrow } = props;
  const [showMore, setShowMore] = useState(false);

  const itemsToShow = showMore ? payload : payload.slice(0, 5);
  const ContentSection = (props: {
    title: string;
    url?: string;
    location?: string;
    price?: string;
  }) => {
    const { title, url, location, price } = props;
    return (
      <section className="item-start absolute inset-0 z-10 my-4 flex transform flex-col justify-end p-4 text-white">
        {price && (
          <h6 className="mb-3 text-left font-body text-t-span xl:text-t-caption">
            {`${state?.localeData?.hotels?.from} ${price}++ / ${state?.localeData?.hotels?.perNight}`}
          </h6>
        )}
        <h2 className="break-word text-left font-display text-t-subtitle-2 sm:text-t-subtitle-1 xl:text-t-h5">
          {title}
        </h2>
        {!hideArrow && url && (
          <div className="item-center mt-3 flex cursor-pointer transition-transform group-hover:translate-x-1">
            <h5 className="mr-3 font-body text-t-caption">{urlText}</h5>
            <Image src={ArrowRight} alt="icon" width={12} height={10} />
          </div>
        )}
        {location && (
          <section className="item-center mt-3 flex transition-transform group-hover:translate-x-1">
            <Image src={Marker} alt="icon" width={12} height={10} />
            <h5 className="lg:t-subtitle-2 ml-2 font-body text-t-span xl:text-t-caption">
              {location}
            </h5>
          </section>
        )}
      </section>
    );
  };

  return (
    <>
      <div className="grid-slider">
        <CarouselContainer
          lang={lang}
          payload={payload}
          urlText={urlText}
          hideArrow={true}
        />
      </div>
      {itemsToShow.length == 0 && <NoresultFound />}
      <div className="block grid-cols-6 gap-3 px-2 sm:hidden lg:px-0 xl:grid">
        {itemsToShow?.length !== 0 &&
          itemsToShow.map((items: any, index: number) => (
            <div
              key={index}
              style={{ gridColumn: index % 5 < 2 ? "span 3" : "span 2" }}
              className="group relative my-4 block overflow-hidden lg:my-0"
            >
              <Link href={`/${lang}/${items?.url ?? items?.slug}`} className="">
                <Image
                  src={items?.src?.src ?? items?.src}
                  alt={items?.title}
                  key={index}
                  width={400}
                  height={300}
                  style={{ height: "auto", width: "100%" }}
                  className={`max-h-[250px] min-h-[250px] object-cover ${index % 5 < 2 ? "xl:max-h-[500px] xl:min-h-[500px]" : "xl:max-h-[400px] xl:min-h-[400px]"} transition-transform duration-500 group-hover:scale-110`}
                />
                <ContentSection
                  title={items?.title}
                  url={`/${lang}/${items?.url ?? items?.slug}`}
                  location={items?.location ?? ""}
                  price={items?.price ?? ""}
                />
                <div className="grid-shadow z-5 pointer-events-none absolute inset-0"></div>
              </Link>
            </div>
          ))}
      </div>
      {payload?.length > 5 && (
        <div className="text-center">
          <button
            className="font-600 lg:t-h6 mt-5 inline-block font-body text-t-subtitle-2 text-text-main sm:hidden xl:inline-block"
            onClick={() => {
              setShowMore(!showMore);
            }}
            type="button"
          >
            {showMore
              ? state?.localeData?.showLess
              : `${state?.localeData?.showMore} (${payload?.length})`}
          </button>
        </div>
      )}
    </>
  );
};

export default QuincunxGrid;
