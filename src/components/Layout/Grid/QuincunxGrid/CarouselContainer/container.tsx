"use client";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";
import ArrowRight from "@/components/icons/Home/RightArrow.svg";
import Marker from "@/components/icons/Marker.svg";
import type { BannerItem } from "@/interfaces/carousel.interface";
import Image from "next/image";
import Link from "next/link";
export default function CarouselContainer({
  payload,
  urlText,
  lang,
  hideArrow,
}: {
  payload: any;
  urlText?: string;
  lang?: string | any;
  hideArrow?: boolean;
}) {
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map((item: BannerItem, index: number) => (
      <div
        key={index}
        className="relative max-h-[300px] min-h-[300px]  w-full flex-shrink-0 lg:max-h-[500px] lg:min-h-[500px]"
      >
        <Link href={`/${lang}/${item?.url ?? item?.slug}`} className="">
          <Image
            src={item.src?.src ?? item?.src}
            alt="image"
            width={400}
            height={400}
            style={{ height: "auto", width: "100%" }}
          />

          <section className="item-start absolute inset-0  z-20  flex transform flex-col justify-end p-4 font-display text-white">
            <h2 className="break-word text-left font-display text-t-subtitle-2 sm:text-t-subtitle-1 xl:text-t-h5">
              {item?.title}
            </h2>

            {item?.url ||
              (!hideArrow && item?.slug && (
                <section className="item-center mt-3 flex transition-transform hover:translate-x-1">
                  <h5 className=" mr-3 font-body text-t-caption text-t-span">
                    {urlText}
                  </h5>
                  <section className="mt-[5px]">
                    <Image src={ArrowRight} alt="icon" width={12} height={10} />
                  </section>
                </section>
              ))}
            {item?.location && (
              <section className="item-center mt-3 flex transition-transform group-hover:translate-x-1">
                <Image
                  src={Marker}
                  alt="icon"
                  width={12}
                  height={10}
                  className="!w-[12px]"
                />
                <h5 className="lg:t-subtitle-2 ml-2 font-body text-t-span xl:text-t-caption">
                  {item?.location}
                </h5>
              </section>
            )}
          </section>

          <div
            className="absolute inset-0 z-10"
            style={{
              background:
                "linear-gradient(180deg, rgba(50, 49, 40, 0.00) 0%, rgba(50, 49, 40, 0.58) 78.55%, #000 100%)",
            }}
          ></div>
        </Link>
      </div>
    ));
  return (
    <div>
      <ResponsiveCarousel items={items} centerMode={false} payload={payload} />
    </div>
  );
}
