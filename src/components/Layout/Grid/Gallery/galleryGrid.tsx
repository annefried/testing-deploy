"use client";
import "./styles.scss";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Lightbox from "yet-another-react-lightbox";
import "yet-another-react-lightbox/styles.css";
import Image from "next/image";
import { useContext, useState } from "react";
import AppContext from "@/lib/context";
import type { AppContextType } from "@/interfaces/global_interfaces";
import MobileCarousel from "./Mobile/carousel";
import DesktopCarousel from "./Desktop/carousel";
import NextJsImage from "./renderImage";
const Gallery = (props: any) => {
  const { state }: AppContextType = useContext(AppContext);
  const { payload, title = "" } = props;
  const [index, setIndex] = useState(0);
  const [open, setOpen] = useState(false);
  const handleClick = (index: number) => {
    setIndex(index);
    setOpen(true);
  };

  const slides = payload?.map(
    (
      item: {
        mediaItemUrl: string;
        altText: string;
      },
      i: number
    ) => ({
      src: item?.mediaItemUrl,
      alt: item?.altText ?? "image",
      width: 1080,
      height: 1620,
      id: i,
    })
  );

  return (
    <div className="px-0 py-4 sm:px-4 lg:px-12 lg:py-12">
      <h3 className="mb-5 text-center font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
        {title}
      </h3>
      <MobileCarousel payload={payload} />
      <DesktopCarousel payload={payload} />
      <div className="hidden grid-cols-6 gap-3 xl:grid">
        <div className="col-span-3 grid grid-cols-1">
          {payload?.length !== 0 &&
            payload.slice(0, 2).map((item: any, index: number) => (
              <div
                onClick={() => handleClick(index)}
                key={index}
                className={`max-h-[244.2px] min-h-[244.2px] cursor-pointer ${index === 1 ? "mt-3" : ""}`}
              >
                <Image
                  src={item?.mediaItemUrl}
                  alt={item?.altText ?? "image"}
                  width={390}
                  height={300}
                  className="h-full w-full object-cover"
                />
              </div>
            ))}
        </div>

        <div className="col-span-3">
          {payload.length > 2 && (
            <Image
              src={payload[2].mediaItemUrl}
              onClick={() => handleClick(2)}
              alt={payload[2].altText ?? "image"}
              width={390}
              height={300}
              priority
              style={{ height: "auto", width: "100%" }}
              className="max-h-[500px] min-h-[500px] cursor-pointer object-cover"
            />
          )}
        </div>

        <div className="col-span-6">
          <div className="grid grid-cols-5 gap-3">
            {payload?.length !== 0 &&
              payload?.length >= 3 &&
              payload?.slice(3, 8)?.map((item: any, i: number) => (
                <div
                  key={i}
                  onClick={() => handleClick(i + 3)}
                  className="relative cursor-pointer"
                >
                  <Image
                    src={item?.mediaItemUrl}
                    alt={item?.altText ?? "image"}
                    width={300}
                    height={300}
                    style={{ height: "auto", width: "100%" }}
                    className="max-h-[220px] min-h-[220px] object-cover"
                  />

                  <div
                    style={{
                      background:
                        "linear-gradient(0deg, rgba(0, 0, 0, 0.30) 0%, rgba(0, 0, 0, 0.30) 100%)",
                    }}
                    className="z-5 absolute inset-0 flex items-center justify-center"
                  >
                    {payload.length > 8 && i === 4 && (
                      <h5 className="font-body text-t-subtitle-2 text-white lg:text-t-h6">
                        {payload.length - 8}+ {state?.localeData?.photos}
                      </h5>
                    )}
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <Lightbox
        slides={slides}
        open={open}
        index={index}
        close={() => setOpen(false)}
        render={{ iconClose: () => null, slide: NextJsImage }}
        controller={{ closeOnBackdropClick: open }}
        plugins={[Zoom]}
      />
    </div>
  );
};

export default Gallery;
