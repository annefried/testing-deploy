"use client";
import Image from "next/image";
import type { BannerItem } from "@/interfaces/carousel.interface";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";

export default function MobileCarousel({
  payload,
}: {
  payload: BannerItem | any;
}) {
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map((item: BannerItem, index: number) => (
      <div key={index} className="relative">
        <Image
          src={item?.mediaItemUrl}
          alt={item?.altText ?? "image"}
          width={300}
          height={300}
          className="max-h-[300px] min-h-[300px] object-cover"
        />
      </div>
    ));
  return (
    <div className="mx-3 block md:hidden">
      <ResponsiveCarousel items={items} centerMode={false} payload={payload} />
    </div>
  );
}
