import Image from "next/image";
import {
  isImageSlide,
  useLightboxProps,
  useLightboxState,
} from "yet-another-react-lightbox";

function isNextJsImage(slide: any) {
  return (
    isImageSlide(slide) &&
    typeof slide.width === "number" &&
    typeof slide.height === "number"
  );
}

export default function NextJsImage({ slide, offset }: any) {
  const {
    on: { click },
  } = useLightboxProps();

  const { currentIndex } = useLightboxState();

  if (!isNextJsImage(slide))
    return (
      <div className="h-10 w-10 animate-spin rounded-full border-[5px] border-white border-t-primary-main" />
    );

  return (
    <Image
      alt="gallery-image"
      src={slide}
      loading="eager"
      draggable={false}
      placeholder={slide.blurDataURL ? "blur" : undefined}
      width={500}
      height={500}
      onClick={
        offset === 0 ? () => click?.({ index: currentIndex }) : undefined
      }
    />
  );
}
