"use client";
import Image from "next/image";
import type { BannerItem } from "@/interfaces/carousel.interface";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";

export default function DesktopCarousel({
  payload,
}: {
  payload: BannerItem | any;
}) {
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map((item: BannerItem, index: number) => (
      <div key={index} className="relative max-h-[400px] min-h-[400px] w-full">
        <Image
          src={item?.mediaItemUrl}
          alt={item?.altText ?? "image"}
          sizes="100%"
          fill
          className="object-cover"
        />
      </div>
    ));
  return (
    <div className="mx-3 hidden md:block xl:hidden">
      <ResponsiveCarousel
        renderThumbs={true}
        centerMode={false}
        items={items}
        payload={payload}
      />
    </div>
  );
}
