"use client";
import { ArrowDown, ArrowRight, ArrowUp } from "@/components/icons/arrows";
import { EngFlag, ThaiFlag } from "@/components/icons/other";
import type { CityItem, HotelItem } from "@/interfaces/global_interfaces";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
const SwapNavbar = ({
  item,
  hasHotels,
  noResultsFound,
  setMobileMenuOpen,
  handleMobileMenuToggle,
}: any) => {
  const activePath = usePathname();
  const [subChildMenu, setSubChildMenu] = useState<string | null>(null);
  const handleSubChildMenuClick = (id: string) => {
    setSubChildMenu((prevSubChildMenu) =>
      prevSubChildMenu === id ? null : id
    );
  };

  useEffect(() => {
    if (subChildMenu === activePath) {
      setMobileMenuOpen(false);
    }
  }, [activePath]);

  return (
    <div className="scrollbar h-[320px] max-h-[800px] overflow-y-auto overflow-x-hidden font-body text-t-subtitle-2 text-text-main">
      <div className="mt-2 flex h-auto w-full flex-col py-2">
        {hasHotels && item?.children?.length === 0 && (
          <div className="item-center flex justify-center px-6 py-10">
            <h4>{noResultsFound}</h4>
          </div>
        )}
        {item.children.map((child: any, index: number) =>
          child.cities && child.cities.length > 0 ? (
            <>
              {hasHotels ? (
                <div key={index} className="mt-4">
                  <section
                    className={`mx-4 mb-3 inline-block w-full font-black md:hover:text-primary-main ${activePath === child.uri ? "text-primary-main" : ""}`}
                    onClick={(e) => {
                      handleMobileMenuToggle(e);
                    }}
                  >
                    <Link role="menuitem" tabIndex={-1} href={child?.uri}>
                      {child?.label}
                    </Link>
                  </section>

                  {child?.cities?.length !== 0 &&
                    child?.cities.map((city: CityItem, i: number) => (
                      <div
                        onClick={() => handleSubChildMenuClick(city.uri)}
                        key={i}
                        className="relative"
                      >
                        <div
                          className={`flex w-full rounded-full bg-transparent py-2 pt-3 outline-none   ${subChildMenu === city.uri && "text-primary-main"}`}
                          id="user-menu-button"
                          aria-expanded="false"
                          aria-haspopup="true"
                        >
                          <div className="flex w-full items-center justify-between px-4">
                            <section
                              className="hover:text-primary-main"
                              onClick={(e) => {
                                handleMobileMenuToggle(e);
                              }}
                            >
                              <Link href={city?.uri} aria-current="page">
                                {city.label}
                              </Link>
                            </section>

                            {city.hotels?.length !== 0 &&
                              subChildMenu === city.uri && <ArrowDown />}
                            {city.hotels?.length !== 0 &&
                              subChildMenu !== city.uri && <ArrowRight />}
                          </div>
                        </div>
                        <div
                          className={`mt-2 h-auto w-full py-2 ${subChildMenu === city.uri && city.hotels?.length !== 0 ? "flex flex-col bg-secondary-400" : "hidden bg-white"}`}
                          role="menu"
                          aria-orientation="vertical"
                          aria-labelledby="user-menu-button"
                          tabIndex={-1}
                        >
                          {city.hotels?.length !== 0 &&
                            city.hotels?.map((hotel: HotelItem, i: number) => (
                              <div key={i} className="hover:text-primary-main">
                                <button
                                  type="button"
                                  className=" rounded-full bg-transparent px-3 py-3 text-left outline-none"
                                  id="user-menu-button"
                                  aria-expanded="false"
                                  aria-haspopup="true"
                                >
                                  <div className="flex items-start justify-start px-2">
                                    <section
                                      className="mr-2 inline-block w-full hover:text-primary-main"
                                      onClick={(e) => {
                                        handleMobileMenuToggle(e);
                                      }}
                                    >
                                      <Link
                                        href={hotel?.uri}
                                        aria-current="page"
                                      >
                                        {hotel.label}
                                      </Link>
                                    </section>
                                  </div>
                                </button>
                              </div>
                            ))}
                        </div>
                      </div>
                    ))}
                </div>
              ) : (
                <div
                  onClick={() => handleSubChildMenuClick(child.uri)}
                  key={index}
                  className="relative"
                >
                  <button
                    type="button"
                    className={`mx-2 flex w-full rounded-full bg-transparent py-3 outline-none   ${subChildMenu === child.uri && "text-primary-main"}`}
                    id="user-menu-button"
                    aria-expanded="false"
                    aria-haspopup="true"
                  >
                    <div className="flex items-center justify-center px-2">
                      <section
                        className="mr-6"
                        onClick={(e) => {
                          handleMobileMenuToggle(e);
                        }}
                      >
                        <Link href={child?.uri} aria-current="page">
                          {child.label}
                        </Link>
                      </section>

                      <ArrowUp />
                    </div>
                  </button>
                  <div
                    className={`mt-2 h-auto w-full py-2 ${subChildMenu === child.uri ? "flex flex-col bg-secondary-400" : "hidden bg-white"}`}
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="user-menu-button"
                    tabIndex={-1}
                  >
                    {child?.cities.map((subChild: CityItem, i: number) => (
                      <div key={i} className="hover:text-primary-main">
                        <button
                          type="button"
                          className=" flex rounded-full bg-transparent px-3 py-3 outline-none"
                          id="user-menu-button"
                          aria-expanded="false"
                          aria-haspopup="true"
                        >
                          <div className="flex items-start justify-center px-2">
                            <section
                              className={`mr-2 inline-block w-full md:hover:text-primary-main ${activePath === subChild?.uri ? "text-primary-mai" : ""}`}
                              onClick={(e) => {
                                handleMobileMenuToggle(e);
                              }}
                            >
                              <Link href={subChild?.uri} aria-current="page">
                                {subChild.label}
                              </Link>
                            </section>
                          </div>
                        </button>
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </>
          ) : (
            <section
              key={index}
              className={`w-full ${item.lang && "mx-4 flex items-center py-0"}  py-3`}
            >
              {item.lang && child.label === "English" && (
                <div className="mobile-locale-logo">
                  <EngFlag />
                </div>
              )}
              {item.lang && child.label === "Thai" && <ThaiFlag />}
              <section
                className={`mx-4 inline-block w-full cursor-pointer md:hover:text-primary-main ${activePath === child?.uri ? "text-primary-main" : ""}`}
                onClick={(e) => {
                  handleMobileMenuToggle(e);
                }}
              >
                <Link href={child?.uri} role="menuitem" tabIndex={-1}>
                  {child?.label}
                </Link>
              </section>
            </section>
          )
        )}
      </div>
    </div>
  );
};

export default SwapNavbar;
