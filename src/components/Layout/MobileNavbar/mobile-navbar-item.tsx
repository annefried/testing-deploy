"use client";
import { ArrowRight } from "@/components/icons/arrows";
import { EngFlag, ThaiFlag } from "@/components/icons/other";
import hotelCheck from "@/helpers/hotelCheck";
import Link from "next/link";
import { usePathname } from "next/navigation";
const NavbarItem = ({
  item,
  handleMouseEnter,
  filteredResultsWithHotels,
}: any) => {
  const activePath = usePathname();
  const hotels = hotelCheck(filteredResultsWithHotels, item);

  if (hotels || (item.children && item.children.length > 0)) {
    return (
      <div className="font-body text-t-subtitle-2 text-text-main">
        <div onClick={() => handleMouseEnter(item.id)} className="mx-3 px-2">
          <button
            type="button"
            className="relative w-full  rounded-full bg-transparent outline-none"
            id="user-menu-button"
            aria-expanded="false"
            aria-haspopup="true"
            onClick={() => handleMouseEnter(item.id)}
          >
            <span className="absolute -inset-1.5"></span>
            <div className="flex w-full items-center justify-between py-2">
              {item.lang && (
                <div className="mt-[0.3rem] flex w-full">
                  {item.label === "en" ? (
                    <div className="mobile-locale-logo">
                      <EngFlag />
                    </div>
                  ) : (
                    <div>
                      <ThaiFlag />
                    </div>
                  )}
                  <div>
                    <span className="ml-2 uppercase" aria-current="page">
                      {item.label}
                    </span>
                  </div>
                </div>
              )}
              {!item.lang && (
                <span className="mr-2" aria-current="page">
                  {item.label}
                </span>
              )}
              <ArrowRight />
            </div>
          </button>
        </div>
      </div>
    );
  } else {
    return (
      <section className="mx-3 w-full px-2 py-2">
        <Link href={item.uri} aria-current="page" legacyBehavior>
          <a
            className={`inline-block w-full text-start text-t-subtitle-2 ${activePath === item.uri && "text-primary-main"} md:hover:text-primary-main`}
          >
            {item.label}
          </a>
        </Link>
      </section>
    );
  }
};

export default NavbarItem;
