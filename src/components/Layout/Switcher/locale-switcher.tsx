import { ArrowUp } from "@/components/icons/arrows";
import type { FC } from "react";
import { useEffect, useRef } from "react";
import { useState, useContext } from "react";
import {
  useParams,
  useRouter,
  useSelectedLayoutSegment,
} from "next/navigation";
import dynamic from "next/dynamic";
import Button from "@/components/Button/button";
import { EngFlag, ThaiFlag } from "@/components/icons/other";
import AppContext from "@/lib/context";
import type { AppContextType } from "@/interfaces/global_interfaces";

const LinkComponent = dynamic(() => import("./link"), {
  ssr: false,
});
const LocaleSwitcher: FC<{
  bookNowBtn: string;
  closeBtn: string;
  handleMouseLeave: () => void;
}> = ({ bookNowBtn, closeBtn, handleMouseLeave }) => {
  const { dispatch, state }: AppContextType = useContext(AppContext);
  const locale = useParams()?.lang;
  const router = useRouter();
  const currentPath = useSelectedLayoutSegment();
  const localeMenuRef = useRef<HTMLDivElement>(null);
  const [isLocaleMenu, setIsLocaleMenu] = useState<boolean>(false);
  const handleMouseEnter = () => {
    setIsLocaleMenu((prev) => !prev);
    handleMouseLeave();
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (
      localeMenuRef.current &&
      !localeMenuRef.current.contains(event.target as Node)
    ) {
      setIsLocaleMenu(false);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <div
      ref={localeMenuRef}
      className="absolute inset-y-0 right-0 mr-2 flex hidden items-center pr-2 sm:static sm:inset-auto sm:flex sm:pr-0"
    >
      <div
        onClick={handleMouseEnter}
        className="relative mx-3 font-body text-t-subtitle-2 capitalize text-text-main xl:mx-8"
      >
        <button
          type="button"
          className="relative mt-2"
          id="user-menu-button"
          aria-expanded="false"
          aria-haspopup="true"
        >
          <div className="pointer-cursor flex items-center justify-center px-2">
            {locale === "en" ? <EngFlag /> : <ThaiFlag />}
            <span className="mx-2 uppercase">{locale}</span>
            <ArrowUp />
          </div>
        </button>
        <div
          className={`pointer-cursor absolute left-0 z-10 mt-2 flex w-48 max-w-24 origin-top-right flex-col rounded bg-white px-[4px] py-1 shadow-lg shadow-md ring-1 ring-black ring-opacity-5 focus:outline-none ${isLocaleMenu ? "block" : "hidden"}`}
          role="menu"
          aria-orientation="vertical"
          aria-labelledby="user-menu-button"
          tabIndex={-1}
        >
          <section className="my-2 flex items-center">
            <EngFlag />
            <LinkComponent value="en" />
          </section>
          <section className="locale-items my-2 flex">
            <ThaiFlag />
            <LinkComponent value="th" />
          </section>
        </div>
      </div>
      <div className="relative">
        <Button
          onClick={() => {
            if (
              currentPath === "hotels" &&
              state?.hotelPayload?.url &&
              !state?.hotelPayload?.url?.includes(
                "reservation.compasshospitality.com"
              )
            ) {
              router.push(state?.hotelPayload?.url);
            } else {
              dispatch.handleBookNow();
              handleMouseLeave();
            }
          }}
          className="hover-effect custom-background width-md focus-btn active-btn uppercase"
          shadow="shadow-md"
          style={{ fontSize: "16px" }}
        >
          {state?.showBookNow ? closeBtn : bookNowBtn}
        </Button>
      </div>
    </div>
  );
};

export default LocaleSwitcher;
