import Link from "next/link";
import { useParams } from "next/navigation";
const LinkComponent = (props: { value: string }) => {
  const locale = useParams()?.lang;
  const { value } = props;
  return (
    <Link
      className={` px-2 hover:text-primary-main ${locale === value && "text-primary-main"}`}
      href={`/${value}`}
      locale={value}
    >
      {value === "en" ? "English" : "Thai"}
    </Link>
  );
};

export default LinkComponent;
