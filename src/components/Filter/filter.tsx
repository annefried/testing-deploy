"use client";
import TransparentSelect from "@/components/Select/transparentSelect";
import type { FilterProps } from "@/interfaces/global_interfaces";
import { useParams } from "next/navigation";
import MobileFilter from "./Mobile/mobileFilter";
const Filter = (props: FilterProps) => {
  const lang = useParams()?.lang;
  const defaultOptions = [
    { value: "all", label: lang === "en" ? "All" : "ทั้งหมด" },
  ];
  return (
    <div className="w-full">
      <MobileFilter {...props} />
      <div className="border-filter-section hidden flex-wrap items-center justify-center font-body text-t-subtitle-2 sm:flex">
        {props?.showCountry && (
          <section className="mr-3 flex w-[300px] items-center justify-between border-r-secondary-900 px-4">
            <label>{props?.countryLabel}</label>
            <TransparentSelect
              onChange={props?.handleCountrySelect}
              options={props?.countryOptions}
              isLoading={props?.countriesIsLoading}
              onFocus={props?.loadCountriesOptions}
              defaultValue={defaultOptions}
            />
          </section>
        )}
        {props?.showDestination && (
          <section
            className={`flex w-[300px] items-center justify-between border-r-[1px] px-4 ${!props?.hideBorder ? "border-r-secondary-900" : ""}`}
          >
            <section>
              {" "}
              <label>{props?.destinationLabel}</label>{" "}
            </section>
            <TransparentSelect
              onChange={props?.handleDestinationSelect}
              options={props?.destinationOptions}
              isLoading={props?.destinationsIsLoading}
              onFocus={props?.loadDestinationsOptions}
              value={props?.destinationSelectedOption}
              borderLeft={props.borderLeft}
            />
          </section>
        )}
      </div>
    </div>
  );
};

export default Filter;
