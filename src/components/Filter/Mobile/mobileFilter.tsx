"use client";
import TransparentSelect from "@/components/Select/transparentSelect";
import type {
  AppContextType,
  FilterProps,
} from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import { useParams } from "next/navigation";
import { useContext, useState } from "react";
const MobileFilter = (props: FilterProps) => {
  const lang = useParams()?.lang;
  const defaultOptions = [
    { value: "all", label: lang === "en" ? "All" : "ทั้งหมด" },
  ];
  const { state }: AppContextType = useContext(AppContext);
  const [showModal, setShowModal] = useState(false);
  return (
    <div className="block text-center sm:hidden">
      {" "}
      <button
        className="w-full border border-secondary-300 py-4 font-body text-t-caption text-text-main"
        type="button"
        onClick={() => setShowModal(true)}
      >
        <span className="flex items-center justify-center">
          {" "}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            viewBox="0 0 16 16"
            fill="none"
          >
            <path
              d="M7.33333 14V10H8.66667V11.3333H14V12.6667H8.66667V14H7.33333ZM2 12.6667V11.3333H6V12.6667H2ZM4.66667 10V8.66667H2V7.33333H4.66667V6H6V10H4.66667ZM7.33333 8.66667V7.33333H14V8.66667H7.33333ZM10 6V2H11.3333V3.33333H14V4.66667H11.3333V6H10ZM2 4.66667V3.33333H8.66667V4.66667H2Z"
              fill="black"
            />
          </svg>{" "}
          <span className="ml-2">{state?.localeData?.filterHeading}</span>
        </span>
      </button>
      {showModal ? (
        <>
          {" "}
          <div
            className="fixed inset-0 z-10 bg-black opacity-30"
            onClick={() => setShowModal(false)}
          ></div>
          <div className="fixed inset-0 z-20 mx-6 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
            <div className="relative  my-6 w-full">
              <div className="relative flex w-full flex-col border-0 bg-white shadow-lg outline-none focus:outline-none">
                <div className="flex items-end justify-end rounded-t p-5 ">
                  <button
                    className="float-right border-0 bg-transparent text-black"
                    onClick={() => setShowModal(false)}
                    type="button"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                    >
                      <path
                        d="M6.40038 18.3084L5.69238 17.6004L11.2924 12.0004L5.69238 6.40038L6.40038 5.69238L12.0004 11.2924L17.6004 5.69238L18.3084 6.40038L12.7084 12.0004L18.3084 17.6004L17.6004 18.3084L12.0004 12.7084L6.40038 18.3084Z"
                        fill="black"
                      />
                    </svg>
                  </button>
                </div>
                <div className="relative px-6 text-left">
                  <h3 className="mb-4  font-body text-t-subtitle-3 text-text-main">
                    {state?.localeData?.filterHeading}
                  </h3>
                  {props?.showCountry && (
                    <section className="">
                      <label className="mb-2  text-t-subtitle-2 text-secondary-800">
                        {props?.countryLabel}
                      </label>{" "}
                      <TransparentSelect
                        onChange={props.handleCountrySelect}
                        options={props?.countryOptions}
                        isLoading={props?.countriesIsLoading}
                        onFocus={props?.loadCountriesOptions}
                        widthFull
                        borderBottom
                        defaultValue={defaultOptions}
                      />
                    </section>
                  )}
                  {props?.showDestination && (
                    <section className="mt-4">
                      {" "}
                      <label className="mb-2 text-t-subtitle-2 text-secondary-800">
                        {props?.destinationLabel}
                      </label>{" "}
                      <TransparentSelect
                        onChange={props?.handleDestinationSelect}
                        isLoading={props?.destinationsIsLoading}
                        onFocus={props?.loadDestinationsOptions}
                        options={props?.destinationOptions}
                        widthFull
                        borderBottom
                        value={props?.destinationSelectedOption}
                      />
                    </section>
                  )}
                </div>
                <div className="border-blueGray-200 flex items-center justify-end rounded-b border-t border-solid p-6">
                  <button
                    className="mb-1 mr-1 rounded bg-primary-main px-6 py-3 text-t-subtitle-2 text-white outline-none"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    {state?.localeData?.apply}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
};

export default MobileFilter;
