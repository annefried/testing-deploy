"use client";
import TransparentSelect from "@/components/Select/transparentSelect";
import type {
  AppContextType,
  DropdownApiResponseProps,
  FilterProps,
  Options,
} from "@/interfaces/global_interfaces";
import client from "@/lib/apollo/client";
import AppContext from "@/lib/context";
import { useParams } from "next/navigation";
import { useContext } from "react";
import MobileFilter from "./Mobile/mobileFilter_modified";
const Filter = (props: FilterProps) => {
  const lang = useParams()?.lang;
  const { state }: AppContextType = useContext(AppContext);
  const filterArray: any = [];
  if (props.showDestination) filterArray.push("Destination");
  if (props.showCountry) filterArray.push("Country");
  filterArray.concat(props.filterPacks);

  const defaultOptions = [
    {
      value: "",
      label: lang === "en" ? "All" : "ทั้งหมด",
    },
  ];

  const fetchOptions = async (
    query: any,
    setState: ((e: any) => void) | undefined,

    responseMapping:
      | ((e: any) => Array<{ label: string; value: string }>)
      | undefined,
    isLoading: boolean
  ) => {
    if (isLoading || !query) return;

    if (setState) setState({ isLoading: true });
    const response: { data: DropdownApiResponseProps } | any =
      await client.query({
        query,
      });
    let array: Options[] = [
      { value: "", label: lang === "en" ? "All" : "ทั้งหมด" },
    ];

    if (responseMapping && responseMapping(response))
      array = array.concat(responseMapping(response));
    if (setState) setState({ options: array, isLoading: false });
  };

  const getSelectionValue = (event: any) => {
    return event.options.filter(
      (setEvent: any) => setEvent.value == event.selectedOption
    )[0];
  };

  return (
    <div className="w-full">
      <MobileFilter {...props} />
      <div className="border-filter-section hidden flex-wrap items-center justify-center font-body text-t-subtitle-2 sm:flex">
        {props?.filterPacks?.map((item, index) => (
          <section
            key={index}
            className="flex w-[300px] items-center justify-between border-r-secondary-900 px-4"
          >
            <section>
              <label>
                {state?.localeData
                  ? state?.localeData[item?.label] ?? item?.label
                  : item?.label}
              </label>
            </section>
            <TransparentSelect
              id={index}
              onChange={(e: { value: string; label: string }) => {
                const state: any = { selectedOption: e.value };
                if (item.label == "filterByDestination") state.isChange = true;
                if (item.setState) item.setState(state);
              }}
              value={getSelectionValue(item)}
              options={item?.options}
              isLoading={item?.isLoading}
              defaultValue={defaultOptions}
              onFocus={() =>
                fetchOptions(
                  item.query,
                  item.setState,
                  item.responseMapping,
                  item.isLoading
                )
              }
            />
          </section>
        ))}
      </div>
    </div>
  );
};

export default Filter;
