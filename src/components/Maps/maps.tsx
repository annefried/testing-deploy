"use client";
import vector from "@/components/Images/marker.png";
import mapStyles from "@/lib/mapStyles/styles";
import { GoogleMap, Marker, useLoadScript } from "@react-google-maps/api";
import React, { useCallback, useState } from "react";

export default function Maps(props: {
  height?: string;
  coordinates: {
    latitude: number;
    longtitude: number;
    locationName?: string;
  }[];
  showCustomStyles?: boolean;
  zoom?: number;
}) {
  const [map, setMap] = useState<google.maps.Map | null>(null);
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_API_KEY ?? "",
  });

  const onLoad = useCallback((map: any) => setMap(map), []);

  const handleMarkerClick = (value: { lat: number; lng: number }) => {
    if (map) {
      map.panTo({
        lat: value.lat,
        lng: value.lng,
      });
      map.setZoom(18);
    }
  };

  if (!isLoaded) return null;

  return (
    <div>
      <GoogleMap
        zoom={props.zoom ?? 20}
        center={{
          lat: props?.coordinates[0]?.latitude,
          lng: props?.coordinates[0]?.longtitude,
        }}
        options={{
          fullscreenControl: true,
          streetViewControl: false,
          mapTypeControl: false,
          ...(props.showCustomStyles && {
            styles: mapStyles,
            fullscreenControl: false,
          }),
        }}
        mapContainerClassName="map"
        mapContainerStyle={{
          width: "100%",
          margin: "auto",
          height: props?.height ?? "500px",
        }}
        onLoad={onLoad}
      >
        {props.coordinates?.map((center, index) => (
          <Marker
            key={index}
            position={{
              lat: center?.latitude,
              lng: center?.longtitude,
            }}
            title={center?.locationName}
            clickable
            icon={props.showCustomStyles ? { url: vector.src } : undefined}
            onClick={() =>
              handleMarkerClick({
                lat: center.latitude,
                lng: center.longtitude,
              })
            }
          />
        ))}
      </GoogleMap>
    </div>
  );
}
