import type { DatePickerProps } from "@/interfaces/datepicker.interface";
import Datepicker from "react-tailwindcss-datepicker";
import type { DateValueType } from "react-tailwindcss-datepicker";
const Picker = ({ placeholder, lang, value, setValue }: DatePickerProps) => {
  const handleValueChange = (newValue: DateValueType) => {
    setValue(newValue);
  };

  return (
    <Datepicker
      primaryColor="green"
      inputClassName="placeholder:seconday-800 w-full relative rounded-md border border-secondary-300 bg-white font-body p-3 text-t-subtitle-2 text-gray-900  outline-none focus:border-primary-main"
      toggleClassName="absolute rounded-r-lg right-0 h-full px-3 text-secondary-500 focus:outline-none disabled:opacity-40 disabled:cursor-not-allowed"
      placeholder={placeholder}
      i18n={lang}
      displayFormat={"DD/MM/YYYY"}
      value={value}
      onChange={handleValueChange}
      separator="-"
      containerClassName="react-datepicker"
    />
  );
};
export default Picker;
