"use client";

import type { AppContextType } from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import Image from "next/image";
import Link from "next/link";
import { useContext } from "react";
import Button from "../Button/button";

const DetailItemsSecondFormat = ({
  item,
}: {
  item: {
    Img: string | any;
    Content: string;
    Title?: string;
    RoomSize?: {
      tableStyle: number | null;
      sizesquareMeter: number | null;
      numberOfPeople: number | null;
      capacity: number | null;
      pdfLink: { url: string } | null;
    };
    Url?: string;
    Alt?: string;
  }[];
}) => {
  const { state }: AppContextType = useContext(AppContext);
  return (
    <div className="px-i-sm pb-i-80 pt-i-sm md:px-[2rem] lg:px-[2rem] xl:px-[4.5rem]">
      {item?.length !== 0 &&
        item?.map((element, index) => (
          <div key={index} className={"flex flex-col  gap-i-sm xl:flex-row"}>
            <div className="h-full w-full py-i-sm md:h-[500px] lg:px-i-xxs xl:w-2/4">
              <Image
                alt={element.Alt ?? "Content Image"}
                src={element.Img}
                width={500}
                height={300}
                className="h-full w-full object-cover"
              />
            </div>
            <div className="break-word xl:w-2/4 xl:py-i-80">
              {element?.Title && (
                <h3 className="font-600 mb-3 font-display text-t-subtitle-2 uppercase lg:text-t-h6">
                  {element?.Title}
                </h3>
              )}
              <ul className="font-body text-t-subtitle-2">
                {element?.RoomSize?.tableStyle && (
                  <li className="my-4">{element?.RoomSize?.tableStyle}</li>
                )}
                {element?.RoomSize?.sizesquareMeter && (
                  <li className="my-4">{element?.RoomSize?.sizesquareMeter}</li>
                )}
                {element?.RoomSize?.numberOfPeople && (
                  <li className="my-4">{element?.RoomSize?.numberOfPeople}</li>
                )}
                {element?.RoomSize?.capacity && (
                  <li className="my-4">{element?.RoomSize?.capacity}</li>
                )}{" "}
              </ul>
              <div
                className="list-disc-detail font-body"
                dangerouslySetInnerHTML={{ __html: element.Content }}
              ></div>
              {element?.Url && (
                <Link
                  href={element?.Url}
                  target="blank"
                  className="font-600 lg:t-h6 mb-4 mt-2 inline-block font-body text-t-subtitle-2 text-text-main underline sm:hidden xl:inline-block"
                >
                  {state?.localeData?.moreInformationButton}
                </Link>
              )}{" "}
              <br />
              {element?.RoomSize?.pdfLink && (
                <Button className="hover-effect mt-3">
                  {state?.localeData?.dowloadButton}
                </Button>
              )}
            </div>
          </div>
        ))}
    </div>
  );
};

export default DetailItemsSecondFormat;
