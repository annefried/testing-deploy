"use client";
import "./styles.scss";
import "react-tabs/style/react-tabs.css";
import { Tabs, TabList, Tab, TabPanel } from "react-tabs";
import { useEffect, useState } from "react";
import Loader from "../Loader/loader";
import { useSearchParams } from "next/navigation";

export default function TabsSection(props: any) {
  const params = useSearchParams();
  const [tabIndex, setTabIndex] = useState(0);

  useEffect(() => {
    if (props.onTabChange) props.onTabChange(tabIndex);
  }, [tabIndex]);

  useEffect(() => {
    const tabsExist = props.tabList?.some(
      (item: { type: string }) =>
        item?.type?.toLowerCase() === params?.get("tab")
    );
    if (params?.get("tab") && tabsExist) {
      const tabIndex = props.tabList.findIndex(
        (item: { type: string }) =>
          item?.type?.toLowerCase() === params?.get("tab")
      );
      setTabIndex(tabIndex !== -1 ? tabIndex : 0);
    }
  }, []);

  return (
    <div className="bg-white pb-5 pt-8">
      {props?.title && (
        <h2 className="mb-3 text-center font-display text-t-subtitle-2 sm:text-t-subtitle-1 lg:text-t-h5">
          {props?.title}
        </h2>
      )}

      <Tabs selectedIndex={tabIndex} onSelect={(index) => setTabIndex(index)}>
        <TabList className="mb-6 flex flex-wrap justify-center font-body text-t-subtitle-2 text-text-main md:text-t-subtitle-3">
          {props?.tabList?.length !== 0 &&
            props?.tabList?.map((items: { label: string }, index: number) => {
              return (
                <Tab
                  className={`${tabIndex === index ? "tab-selected tab" : "tab"}`}
                  key={index}
                >
                  {items.label}
                </Tab>
              );
            })}
        </TabList>
        {props?.loading && (
          <div className="flex w-full justify-center py-12">
            <Loader />
          </div>
        )}

        {props?.tabPanelList?.length !== 0 &&
          !props?.loading &&
          props?.tabPanelList?.map((items: any, index: number) => {
            return <TabPanel key={index}>{items}</TabPanel>;
          })}
      </Tabs>
    </div>
  );
}
