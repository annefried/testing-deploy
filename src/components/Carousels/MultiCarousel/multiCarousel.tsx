import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import LightArrowRight from "@/components/icons/Home/Slider-Right.svg";
import LightArrowLeft from "@/components/icons/Home/Slider-Left.svg";
import Image from "next/image";

export default function MultiCarousel(props: {
  items: { title: string; src: string }[];
  centerMode?: boolean;
  numberOfItems?: number;
}) {
  const CustomRight = ({ onClick }: any) => (
    <button
      className="react-multiple-carousel__arrow absolute right-7 h-[38px] w-[38px]"
      onClick={onClick}
    >
      <Image
        src={LightArrowRight}
        width={38}
        height={38}
        className="h-auto w-full"
        alt="icon"
      />
    </button>
  );
  const CustomLeft = ({ onClick }: any) => (
    <button
      className="react-multiple-carousel__arrow absolute left-7 h-[38px] w-[38px]"
      onClick={onClick}
      type="button"
    >
      <Image
        src={LightArrowLeft}
        width={38}
        height={38}
        className="h-auto w-full"
        alt="icon"
      />
    </button>
  );
  return (
    <Carousel
      additionalTransfrom={0}
      draggable
      infinite
      keyBoardControl
      responsive={{
        desktop: {
          breakpoint: {
            max: 3000,
            min: 1024,
          },
          items: props.numberOfItems ?? 3,
          partialVisibilityGutter: 40,
        },
        mobile: {
          breakpoint: {
            max: 464,
            min: 0,
          },
          items: 1,
          partialVisibilityGutter: 10,
        },
        tablet: {
          breakpoint: {
            max: 1024,
            min: 464,
          },
          items: 3,
          partialVisibilityGutter: 30,
        },
      }}
      rewind={false}
      rewindWithAnimation={false}
      slidesToSlide={1}
      swipeable
      showDots={false}
      centerMode={props.centerMode ?? false}
      customRightArrow={<CustomRight onClick={undefined} />}
      customLeftArrow={<CustomLeft onClick={undefined} />}
    >
      {props?.items}
    </Carousel>
  );
}
