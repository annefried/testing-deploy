"use client";
import Button from "@/components/Button/button";
import ArrowLeft from "@/components/icons/Home/Carousel-Left.svg";
import ArrowRight from "@/components/icons/Home/Carousel-Right.svg";
import Pause from "@/components/icons/Home/Pause.svg";
import Play from "@/components/icons/Home/Play.svg";
import type { BannerItem } from "@/interfaces/carousel.interface";
import AppContext from "@/lib/context";
import Image from "next/image";
import { useParams } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import { Carousel } from "react-responsive-carousel";
import "./banner.scss";

const Banner = ({
  payload,
  eventPage,
}: {
  payload: BannerItem[];
  eventPage?: boolean;
}) => {
  const [index, setIndex] = useState({
    currentSlide: 0,
    autoPlay: true,
  });
  const { dispatch, state }: any = useContext(AppContext);
  const params = useParams()?.lang;
  const languageChanger = params == "th" ? "TH" : "";
  const [mounted, setMounted] = useState<boolean>(false);
  useEffect(() => {
    setMounted(true);
  }, []);

  const next = () => {
    setIndex((prevState) => ({
      ...prevState,
      currentSlide: prevState.currentSlide + 1,
    }));
  };

  const prev = () => {
    setIndex((prevState) => ({
      ...prevState,
      currentSlide: prevState.currentSlide - 1,
    }));
  };

  const changeAutoPlay = () => {
    setIndex((prevState) => ({
      ...prevState,
      autoPlay: !prevState.autoPlay,
    }));
  };

  const updateCurrentSlide = (newIndex: number) => {
    if (index.currentSlide !== newIndex) {
      setIndex({
        ...index,
        currentSlide: newIndex,
      });
    }
  };

  const items =
    payload && payload?.length !== 0
      ? payload?.map((item, index) => {
          return (
            <div className="banner-item-container relative w-full" key={index}>
              <Image
                alt={
                  item?.imageFieldForHomebanner?.node?.altText ??
                  item.bannerTitle
                }
                width={1000}
                priority
                height={600}
                src={
                  item?.imageFieldForHomebanner?.node?.mediaItemUrl ??
                  item?.mediaItemUrl
                }
                className="h-full max-h-[450px] min-h-[450px] w-full object-cover object-left-top lg:max-h-[600px] lg:min-h-[600px]"
              />
              {mounted && (
                <div
                  className={
                    eventPage
                      ? "break-word absolute left-10 top-[40%] z-50 mt-12 flex w-3/4 translate-y-0 flex-col flex-wrap items-start justify-start text-white sm:top-[28%] sm:w-2/3 md:translate-y-1/3 lg:top-1/3 xl:left-20 xl:w-[65%]"
                      : "absolute left-0 top-1/2 z-50 ml-4 flex -translate-y-1/2 translate-x-0 transform flex-col flex-wrap items-start justify-center md:ml-4 lg:left-1/2 lg:ml-0 lg:-translate-x-1/2 lg:items-center xl:left-1/2 xl:ml-0 xl:-translate-x-1/2 xl:items-center"
                  }
                >
                  {eventPage && (
                    <div className="sm:t-caption mb-3 font-body text-t-subtitle-2 uppercase text-white">
                      {item[`subText${languageChanger}`]}
                    </div>
                  )}
                  <h1 className="font-display text-t-subtitle-1 tracking-light text-white md:tracking-lighest lg:tracking-tight xl:text-t-h3 xl:tracking-tight">
                    {item[`bannerTitle${languageChanger}`]}
                  </h1>
                  {!eventPage && (
                    <section>
                      <Button
                        variant="outline"
                        className="outline-hover-effect mt-4"
                        onClick={() =>
                          dispatch &&
                          dispatch?.scrollToSection(state?.discoverRef)
                        }
                      >
                        {params === "en" ? "Discover" : "โรงแรมของเรา"}
                      </Button>
                    </section>
                  )}
                </div>
              )}{" "}
              <div
                className="absolute inset-0 z-10"
                style={{
                  background:
                    "linear-gradient(0deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.20) 100%)",
                }}
              ></div>
            </div>
          );
        })
      : [];
  return (
    <div className="main-banner-carousel banner relative">
      <Carousel
        showArrows={false}
        showIndicators={false}
        showThumbs={false}
        showStatus={false}
        animationHandler="fade"
        infiniteLoop
        transitionTime={1800}
        interval={2500}
        swipeable={false}
        autoPlay={index.autoPlay}
        selectedItem={index.currentSlide}
        onChange={updateCurrentSlide}
        stopOnHover={false}
      >
        {items}
      </Carousel>
      {payload?.length > 1 && (
        <div className="absolute bottom-0 z-20 mb-4 w-full text-white">
          <div className="ml-4 flex flex-wrap items-center justify-start gap-2 lg:ml-0 lg:justify-center">
            {!eventPage && (
              <button
                className="hidden lg:block"
                type="button"
                onClick={changeAutoPlay}
              >
                <Image
                  src={index.autoPlay ? Pause : Play}
                  width={28}
                  height={28}
                  alt="icon"
                />
              </button>
            )}

            {!eventPage && (
              <button
                onClick={prev}
                type="button"
                className="btn-prev rounded-[50%] border p-[5px] lg:border-none"
              >
                {" "}
                <Image src={ArrowLeft} width={28} height={28} alt="icon" />
              </button>
            )}
            {payload?.map((_, i) => {
              return (
                <button
                  key={i}
                  type="button"
                  className={`carousel__dots ${index.currentSlide === i ? "bg-white" : "bg-[#828282]"}`}
                />
              );
            })}
            {!eventPage && (
              <button
                onClick={next}
                type="button"
                className="btn-next rounded-[50%] border px-[5px] py-3 lg:border-none"
              >
                {" "}
                <Image
                  src={ArrowRight}
                  className="h-[15px] w-[27.61px]"
                  width={28}
                  height={28}
                  alt="icon"
                />
              </button>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default Banner;
