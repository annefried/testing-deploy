"use client";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import LightArrowRight from "@/components/icons/Home/Slider-Right.svg";
import LightArrowLeft from "@/components/icons/Home/Slider-Left.svg";
import Image from "next/image";
import "./styles.scss";
import { useContext, useEffect, useState } from "react";
import AppContext from "@/lib/context";
import type { AppContextType } from "@/interfaces/global_interfaces";

export default function InstagramPosts() {
  const { state }: AppContextType = useContext(AppContext);
  const [showMore, setShowMore] = useState(false);
  const [posts, setPosts] = useState({
    data: [],
    paging: {},
  });
  const data = posts?.data?.map((q: any) => ({
    media_url: q.media_url,
    media_type: q.media_type,
    id: q.id,
  }));

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1200 },
      items: 6,
    },
    tablet: {
      breakpoint: { max: 1200, min: 800 },
      items: 4,
    },
    mobile: {
      breakpoint: { max: 600, min: 0 },
      items: 2,
    },
  };

  const fetchInstaData = async () => {
    try {
      const longTermToken = `https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&&access_token=${process.env.NEXT_PUBLIC_INSTAGRAM_TOKEN}`;
      const longTermTokenPayload = await fetch(longTermToken);
      const longTermTokenResponse = await longTermTokenPayload?.json();
      const url = `https://graph.instagram.com/me/media?fields=id,media_url,media_type&access_token=${longTermTokenResponse?.access_token}`;
      const payload = await fetch(url);
      const response = await payload?.json();
      setPosts(response);
    } catch (err) {
      if (err) {
        return err;
      }
    }
  };

  useEffect(() => {
    fetchInstaData();
  }, []);

  const slicedItems = showMore ? data : data?.slice(0, 6);

  const CustomRight = ({ onClick }: any) => (
    <button
      className="react-multiple-carousel__arrow absolute right-5 h-[38px] w-[38px]"
      onClick={onClick}
      type="button"
    >
      <Image
        src={LightArrowRight}
        width={38}
        height={38}
        className="h-auto w-full"
        alt="icon"
      />
    </button>
  );
  const CustomLeft = ({ onClick }: any) => (
    <button
      className="react-multiple-carousel__arrow absolute left-5 h-[38px] w-[38px]"
      onClick={onClick}
      type="button"
    >
      <Image
        src={LightArrowLeft}
        width={38}
        height={38}
        className="h-auto w-full"
        alt="icon"
      />
    </button>
  );
  return (
    <>
      {posts?.data?.length !== 0 && posts && (
        <div className="instagram-slider hidden h-full w-full pb-5 lg:inline-block">
          <Carousel
            responsive={responsive}
            ssr
            showDots={false}
            customRightArrow={<CustomRight onClick={undefined} />}
            customLeftArrow={<CustomLeft onClick={undefined} />}
            slidesToSlide={1}
            transitionDuration={200}
            infinite
            rewind={false}
            rewindWithAnimation={false}
          >
            {posts?.data?.map((data: any, index: number) => {
              return data.media_type === "VIDEO" ? (
                <div
                  key={index}
                  className="overflow-hidden px-[5px] text-center"
                >
                  <video
                    className="h-[230px] w-[300px] object-cover"
                    loop
                    autoPlay
                    muted
                    playsInline
                  >
                    <source src={data.media_url} type="video/mp4" />
                  </video>
                </div>
              ) : (
                <div
                  key={index}
                  className="overflow-hidden px-[5px] text-center"
                >
                  <Image
                    key={data.media_url}
                    src={data.media_url}
                    alt="image"
                    width={300}
                    height={200}
                    quality={50}
                    className="h-[230px] w-[300px] object-cover"
                    loader={() => data.media_url}
                  />
                </div>
              );
            })}
          </Carousel>
        </div>
      )}
      {posts?.data?.length !== 0 && posts && (
        <div className="grid-wrapper block md:grid lg:hidden">
          {slicedItems?.map((data: any, index: number) => {
            return data.media_type === "VIDEO" ? (
              <div key={index}>
                <video
                  className="h-[200px] w-full object-cover"
                  loop
                  autoPlay
                  muted
                  playsInline
                  key={index}
                >
                  <source src={data.media_url} type="video/mp4" />
                </video>
              </div>
            ) : (
              <div key={data.media_url}>
                <Image
                  src={data.media_url}
                  alt="image"
                  width={200}
                  height={180}
                  className="my-4 h-[200px] w-full object-cover sm:my-0"
                  loader={() => data.media_url}
                />
              </div>
            );
          })}
          {posts?.data?.length > 5 && (
            <div className="text-center">
              <button
                className="font-600 lg:t-h6 mt-5 inline-block font-body text-t-subtitle-2 text-text-main sm:hidden xl:inline-block"
                onClick={() => {
                  setShowMore(!showMore);
                }}
                type="button"
              >
                {showMore
                  ? state?.localeData?.showLess
                  : state?.localeData?.showMore}{" "}
                {`(${posts?.data?.length})`}
              </button>
            </div>
          )}
        </div>
      )}
    </>
  );
}
