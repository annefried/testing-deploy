"use client";
import LightArrowLeft from "@/components/icons/Home/Slider-Left.svg";
import LightArrowRight from "@/components/icons/Home/Slider-Right.svg";
import type { BannerItem } from "@/interfaces/carousel.interface";
import Image from "next/image";
import { useState } from "react";
import { Carousel } from "react-responsive-carousel";
import "./carousel.scss";
import { useMediaQuery } from "react-responsive";

export default function ResponsiveCarousel({
  centerMode,
  payload,
  items,
  renderThumbs,
}: {
  payload: BannerItem | any;
  centerMode?: boolean;
  items: any;
  renderThumbs?: boolean;
}) {
  const mobileScreen = useMediaQuery({ maxWidth: 1000 });
  const [currentSlide, setCurrent] = useState(0);
  const handleNextSlide = () => {
    payload?.length > 1 && setCurrent((state) => state + 1);
  };

  const handlePrevSlide = () => {
    payload?.length > 1 && setCurrent((state) => state - 1);
  };

  const updateCurrentSlide = (index: number) => {
    if (currentSlide !== index) {
      setCurrent(index);
    }
  };

  const showArrows = payload?.length > 1;

  return (
    <div className={`relative ${centerMode ? "spaced-carousel" : ""}`}>
      {payload?.length > 1 ? (
        <Carousel
          showArrows={false}
          infiniteLoop
          centerSlidePercentage={mobileScreen ? 100 : 50}
          showIndicators={false}
          showStatus={false}
          showThumbs={renderThumbs === true}
          dynamicHeight={false}
          selectedItem={currentSlide}
          onChange={updateCurrentSlide}
          centerMode={centerMode}
          swipeable
          thumbWidth={150}
          renderThumbs={
            renderThumbs
              ? () =>
                  payload.map(
                    (
                      image: {
                        altText: string;
                        mediaItemUrl: string;
                        src: { src: string };
                      },
                      index: number
                    ) => (
                      <Image
                        key={index}
                        src={image?.src?.src ?? image?.mediaItemUrl}
                        alt={image?.altText ?? "image"}
                        width={100}
                        height={100}
                        className="object-cover"
                      />
                    )
                  )
              : () => null
          }
        >
          {items}
        </Carousel>
      ) : (
        items
      )}
      {showArrows && (
        <button
          className={`prev-btn absolute h-[38px] w-[38px] ${renderThumbs ? "top-[42%]" : "top-1/2"}`}
          type="button"
          onClick={handlePrevSlide}
        >
          {" "}
          <Image
            src={LightArrowLeft}
            width={38}
            height={38}
            className="h-auto w-full"
            alt="icon"
          />
        </button>
      )}
      {showArrows && (
        <button
          className={`next-btn absolute h-[38px] w-[38px] ${renderThumbs ? "top-[42%]" : "top-1/2"}`}
          type="button"
          onClick={handleNextSlide}
        >
          <Image
            src={LightArrowRight}
            width={38}
            height={38}
            className="h-auto w-full"
            alt="icon"
          />
        </button>
      )}
    </div>
  );
}
