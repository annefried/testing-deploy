"use client";
import Image from "next/image";

const TopBannerItems = ({
  bannerImg,
  title,
  description,
  opacity = false,
  altText,
}: {
  description: string;
  title: string;
  altText?: string;
  opacity?: boolean;
  bannerImg?:
    | {
        node: {
          mediaItemUrl: string;
        };
      }
    | any;
}) => {
  return (
    <div className="relative w-full">
      <Image
        src={bannerImg.src ?? bannerImg}
        width={550}
        height={800}
        alt={altText ?? "Desktop Banner"}
        className={`${opacity ? "opacity-10" : ""} h-full max-h-[450px] min-h-[450px] w-full object-cover lg:max-h-[600px] lg:min-h-[600px]`}
      />

      <div className="absolute top-[50%] z-10 mt-[-130px] w-full pb-i-20 pt-i-40 md:pt-i-20 lg:mt-[-190px] lg:pt-i-10">
        <h1 className="flex justify-center gap-2 font-display text-t-subtitle-1 md:text-t-h2">
          {title}
        </h1>
        <div
          className={`${description?.length > 220 ? "text-overflow-clamp" : ""} break-word px-i-90 pt-i-sm text-center font-body text-secondary-500 xl:px-[200px]`}
        >
          {description}
        </div>
      </div>
      <div
        className="z-5 text-red absolute top-0 h-full w-full"
        style={{
          background:
            "linear-gradient(180deg, rgba(187, 187, 187, 0.10) 10.68%, rgba(135, 135, 135, 0.10) 100%)",
        }}
      ></div>
    </div>
  );
};

export default TopBannerItems;
