export const ArrowUp = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="14"
      viewBox="0 0 14 14"
      fill="none"
    >
      <path
        d="M4.3225 4.66578L7 7.33745L9.6775 4.66578L10.5 5.48828L7 8.98828L3.5 5.48828L4.3225 4.66578Z"
        fill="#3D3712"
      />
    </svg>
  );
};

export const ArrowRight = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="8"
      height="14"
      viewBox="0 0 8 14"
      fill="none"
    >
      <path
        d="M5.17205 7.00002L0.222046 2.05002L1.63605 0.637024L8.00005 7.00002L1.63605 13.364L0.222046 11.949L5.17205 7.00002Z"
        fill="#3D3712"
      />
    </svg>
  );
};

export const ArrowLeft = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="8"
      height="14"
      viewBox="0 0 8 14"
      fill="none"
    >
      <path
        d="M2.82802 6.99998L7.77802 11.95L6.36402 13.363L1.52588e-05 6.99998L6.36402 0.635976L7.77802 2.05098L2.82802 6.99998Z"
        fill="#3D3712"
      />
    </svg>
  );
};

export const ArrowDown = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
    >
      <path
        d="M16.59 16.0018L12 11.4218L7.41 16.0018L6 14.5918L12 8.5918L18 14.5918L16.59 16.0018Z"
        fill="#3D3712"
      />
    </svg>
  );
};
