import "./button.scss";
import clsx from "clsx";
import { forwardRef } from "react";
import type { ButtonProps, ButtonVariant } from "@/interfaces/button.interface";
type Ref = HTMLButtonElement;
const getVariant = (variant: ButtonVariant) => {
  switch (variant) {
    case "outline":
      return "btn-outline rounded border border-solid border-white text-white bg-transparent relative overflow-hidden h-[52px] text-t-subtitle-2";
    case "outline-disable":
      return "text-t-caption sm:text-t-subtitle-1 lg:text-t-subtitle-3 bg-transparent text-secondary-800 border-secondary-800 text-t-h6";
    case "ghost":
      return "btn-ghost bg-secondary-800 text-secondary-800 bg-opacity-10 text-t-h6";
    case "white-background":
      return "btn-white-background bg-white text-text-main";
    default:
      return "btn bg-primary-main text-white text-t-h6 rounded border border-solid font-body border-transparent";
  }
};

const Button = forwardRef<Ref, ButtonProps>((props, ref) => {
  const {
    variant = "solid",
    type = "button",
    className,
    children,
    ...rest
  } = props;

  const merged = clsx("btn", getVariant(variant), className, {
    shadow: props?.shadow,
  });

  return (
    <button type={type} ref={ref} className={merged} {...rest}>
      {children}
    </button>
  );
});

Button.displayName = "Button";
export default Button;
