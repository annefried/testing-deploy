import type { InputProps } from "@/interfaces/input.interface";
import clsx from "clsx";
import Image from "next/image";
// const getVariant = (variant: ButtonVariant) => {
//   switch (variant) {
//     case "outline":
//       return "btn-outline";
//     case "outline-disable":
//       return "btn-outline-disable";
//     case "ghost":
//       return "btn-ghost";
//     default:
//       return undefined;
//   }
// };

const InputField = (props: InputProps) => {
  const {
    value,
    placeholder = "Placeholder",
    type,
    inputName,
    label,
    handleChange,
    className,
    showLeftIcon = false,
    showRightIcon = false,
    icon = "",
    required = false,
    error,
  } = props;

  const mergedClasses = clsx(
    `placeholder:seconday-800 block w-full rounded-md border ${required && error ? "border-error" : "border-secondary-300 focus:border-primary-main"}  bg-white font-body text-t-subtitle-2 text-gray-900  outline-none`,
    className
  );

  return (
    <>
      {label && (
        <label
          htmlFor={inputName}
          className="relative mb-3 block font-body text-t-subtitle-2 text-text-main"
        >
          {required && (
            <>
              <span className="absolute left-0 top-0 pr-1 text-red-100">*</span>
              &nbsp;&nbsp;
            </>
          )}
          {label}
        </label>
      )}
      <div className="relative flex items-center">
        {showLeftIcon && (
          <Image
            src={icon}
            alt="book-now-search-icon"
            width={24}
            height={24}
            className="pointer-events-none absolute left-2"
          />
        )}
        <input
          onChange={(e) => handleChange(e)}
          type={type}
          name={inputName}
          autoComplete="off"
          value={value}
          placeholder={placeholder}
          className={mergedClasses}
        />
        {showRightIcon && (
          <Image
            src={icon}
            alt="book-now-search-icon"
            width={24}
            height={24}
            className="pointer-events-none absolute right-0"
          />
        )}
      </div>
    </>
  );
};

export default InputField;
