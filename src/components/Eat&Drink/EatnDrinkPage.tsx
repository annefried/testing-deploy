"use client";

import type { FilterPropItem } from "@/interfaces/global_interfaces";
import Image from "next/image";
import { Suspense, createRef, useEffect, useState } from "react";

import Banner from "../Banner/banner";

import Filter from "@/components/Filter/filter_modified";
import QuincunxGrid from "@/components/Layout/Grid/QuincunxGrid";
import SubNavbar from "../SubNavbar/subNavbar";

import CarouselContainer from "@/components/Spa/CarouselContainer/container";
import { LanguageChanger } from "@/helpers/languageChanger";
import client from "@/lib/apollo/client";
import { GET_EAT_AND_DRINK_FILTER } from "@/lib/queries/eatNdrink";
import { useParams } from "next/navigation";
import Loader from "../Loader/loader";
import TabsSection from "../Tabs/tabs";

interface EatDrinkDataProps {
  EatDrinkData: any;
  destinationArray: any;
}

const EatnDrinkPage = ({
  EatDrinkData,
  destinationArray,
}: EatDrinkDataProps) => {
  const lang = useParams()?.lang;

  const [carouselCategory, setCarouselCategory] = useState<
    { label: string; value: string }[]
  >([]);

  const [category, setCategory] = useState<FilterPropItem>({
    options: [],
    isLoading: false,
    selectedOption: "",
    label: "filterByCategory",
    setState: (e: any) => {
      setCategory((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [destinations, setDestinations] = useState<FilterPropItem>({
    options: destinationArray,
    isLoading: false,
    selectedOption: "",
    label: "filterByDestination",
    isChange: true,
    setState: (e: any) => {
      setDestinations((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [resturants, setResturants] = useState<any>([]);
  const [resturantsLoading, setResturantsLoading] = useState(false);
  const [carousel, setCarousel] = useState<any>([]);
  const [carouselLoading, setCarouselLoading] = useState(false);

  const sectionKey = Object.keys(EatDrinkData);
  const objectRef: any = {};
  const subNavbarItems: any = [];

  sectionKey.forEach((key) => {
    if (!["__typename", "banner"].includes(key)) {
      objectRef[key] = createRef();
      subNavbarItems.push({
        title:
          key == "worldOfTastes"
            ? LanguageChanger(EatDrinkData[key][0], "mainHeading", lang)
            : LanguageChanger(EatDrinkData[key], "heading", lang),
        id: objectRef[key],
      });
    }
  });

  const GetEatDrinkFilter = async (
    categoryData: any,
    destinationsData: any,
    selectDestination: any
  ) => {
    setResturantsLoading(true);
    try {
      const data = await client.query({
        query: GET_EAT_AND_DRINK_FILTER({
          category: categoryData,
          destination: destinationsData.replace("hotels", "").trim(),
        }),
      });
      if (data) {
        const content = data?.data?.resturants?.edges.map((event: any) => {
          return {
            title: LanguageChanger(event.node, "title", lang),
            slug: "eat-and-drink/" + event.node?.slug,
            src: event.node?.featuredImage?.node?.mediaItemUrl || "",
            location: LanguageChanger(
              event.node?.resturantDetailsFields?.resturantDetails,
              "resturantLocation",
              lang
            ),
          };
        });

        if (selectDestination && destinations.isChange) {
          setDestinations((prev: any) => ({
            ...prev,
            isChange: false,
          }));
          let categorySet = new Set();
          data?.data?.resturants?.edges.forEach((event: any) => {
            const categoryData =
              event.node?.resturantDetailsFields?.resturantDetails
                ?.selectCusines?.nodes[0];
            if (categoryData) {
              categorySet.add(
                event.node?.resturantDetailsFields?.resturantDetails
                  ?.selectCusines?.nodes[0].name
              );
            }
          });
          let categoryArrayNew = [
            { label: "All", value: "" },
            ...Array.from(categorySet)
              .sort()
              .map((event: any) => ({
                label: event,
                value: event,
              })),
          ];
          setCategory((prev: any) => ({
            ...prev,
            options: categoryArrayNew,
          }));
          if (destinationsData == "") {
            setCarouselCategory(categoryArrayNew);
          }
        }

        setResturants(content);
      }
    } catch (error) {
      setResturants([]);
    }
    setResturantsLoading(false);
  };

  const GetEatDrinkFilterCarousel = async (index: number) => {
    setCarouselLoading(true);
    try {
      const data = await client.query({
        query: GET_EAT_AND_DRINK_FILTER({
          category: category.options[index]?.value,
        }),
      });
      if (data) {
        const content = data?.data?.resturants?.edges.map((event: any) => {
          return {
            title: LanguageChanger(event.node, "title", lang),
            slug: "eat-and-drink/" + event.node?.slug,
            src: event.node?.featuredImage?.node?.mediaItemUrl,
            // price:
            //   "Price " +
            //   event.node?.resturantDetailsFields?.resturantDetails
            //     ?.resturantPrice?.nodes[0].name,
          };
        });
        setCarousel(content);
      }
    } catch (error) {
      setCarousel([]);
    }
    setCarouselLoading(false);
  };

  const onTabChange = (e: any) => {
    GetEatDrinkFilterCarousel(e);
  };

  useEffect(() => {
    GetEatDrinkFilterCarousel(0);
  }, []);

  useEffect(() => {
    setCategory((prev: any) => ({
      ...prev,
      selectedOption: "",
    }));
    GetEatDrinkFilter("", destinations.selectedOption, true);
  }, [destinations.selectedOption]);

  useEffect(() => {
    if (!destinations.isChange)
      GetEatDrinkFilter(
        category.selectedOption,
        destinations.selectedOption,
        true
      );
  }, [category.selectedOption]);

  const filterProps = {
    filterPacks: [destinations, category],
  };

  return (
    <div>
      {sectionKey?.map((key, index: number) => {
        const subObjectPayload = EatDrinkData[key];
        switch (key) {
          case "banner":
            return (
              <>
                <div key={index}>
                  <Banner
                    bannerImage={{
                      src: subObjectPayload?.backgroundImage?.node
                        ?.mediaItemUrl,
                    }}
                    subTitle={LanguageChanger(
                      subObjectPayload,
                      "subHeading",
                      lang
                    )}
                    title={LanguageChanger(subObjectPayload, "heading", lang)}
                  />
                </div>
                <SubNavbar subNavbarItems={subNavbarItems} />
              </>
            );
          case "worldOfTastes":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 py-i-80 text-center font-display text-t-subtitle-1"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload[0], "mainHeading", lang)}
                  </div>
                </div>
                {subObjectPayload[0].content?.map(
                  (content: any, index: number) => {
                    return (
                      <div
                        key={index}
                        className="flex flex-col items-center gap-i-90 px-i-90 pb-i-80  md:px-i-40 lg:px-i-10 xl:px-[11.5rem]"
                      >
                        <div className="flex w-full flex-col gap-i-90 sm:flex-row">
                          {content.images?.nodes?.map(
                            (item: any, index: number) => (
                              <div key={index} className="h-[350px] w-full">
                                <Image
                                  key={index}
                                  width={300}
                                  height={450}
                                  src={item.mediaItemUrl}
                                  alt={
                                    item.mediaItemUrl ?? "Eat and Drink Images"
                                  }
                                  className="h-full w-full object-cover"
                                />
                              </div>
                            )
                          )}
                        </div>
                        <div className="font-body text-text-main">
                          <div className="pb-i-90 text-center text-t-subtitle-1">
                            {LanguageChanger(content, "heading", lang)}
                          </div>
                          <div>{LanguageChanger(content, "text", lang)}</div>
                        </div>
                      </div>
                    );
                  }
                )}
              </div>
            );
          case "sectionExploreExclusiveDining":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1 sm:pt-i-50"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload, "heading", lang)}
                  </div>
                </div>
                <div className="px-i-90 text-center font-body font-bold text-text-main">
                  {LanguageChanger(subObjectPayload, "subText", lang)}
                </div>

                <Suspense>
                  <TabsSection
                    loading={carouselLoading}
                    onTabChange={onTabChange}
                    tabList={carouselCategory}
                    tabPanelList={carouselCategory.map((item) => {
                      return (
                        <CarouselContainer
                          key={item.label.toLocaleLowerCase()}
                          payload={carousel}
                        />
                      );
                    })}
                  />
                </Suspense>
              </div>
            );
          case "sectionRestaurantsYouMayLike":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload, "heading", lang)}
                  </div>
                </div>
                <div className="px-i-90 text-center font-body font-bold text-text-main">
                  {LanguageChanger(subObjectPayload, "subText", lang)}
                </div>
                <div className="!px-i-90 !py-i-80 sm:p-0">
                  <Filter {...filterProps} />
                </div>
                {resturantsLoading && (
                  <div className="flex w-full justify-center pb-i-80">
                    <Loader />
                  </div>
                )}
                {!resturantsLoading && (
                  <div className="pb-i-80">
                    <QuincunxGrid payload={resturants} hideArrow={true} />
                  </div>
                )}
              </div>
            );
          default:
            return null;
        }
      })}
    </div>
  );
};

export default EatnDrinkPage;
