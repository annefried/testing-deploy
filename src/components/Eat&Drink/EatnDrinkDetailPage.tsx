"use client";
import type {
  AppContextType,
  FilterPropItem,
} from "@/interfaces/global_interfaces";
import client from "@/lib/apollo/client";
import AppContext from "@/lib/context";
import { GET_EAT_AND_DRINK_FILTER } from "@/lib/queries/eatNdrink";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useRef, useState } from "react";

import Banner from "@/components/Banner/banner";
import SubNavbar from "@/components/SubNavbar/subNavbar";
import Button from "../Button/button";
import Filter from "../Filter/filter_modified";
import QuincunxGrid from "../Layout/Grid/QuincunxGrid";
import Loader from "../Loader/loader";

import CarbonLocationVector from "@/components/icons/CarbonLocationVector.svg";
import ClockIcon from "@/components/icons/ClockIcon.svg";
import ShirtIcon from "@/components/icons/ShirtIcon.svg";
import { LanguageChanger } from "@/helpers/languageChanger";
import { useParams } from "next/navigation";
import ImageGalleryCarousel from "../ImageGalleryCarousel/ImageGalleryCarousel";

interface eatAndDrinkProps {
  getEatDrinkData: any;
  destinationArray: any;
  getHeaderData: any;
  slug: string | undefined;
}

const EatnDrinkDetailPage = ({
  getEatDrinkData,
  destinationArray,
  getHeaderData,
  slug,
}: eatAndDrinkProps) => {
  const lang = useParams()?.lang;

  const EatDrinkData = getEatDrinkData?.resturants?.edges[0].node;
  const RestaurantTimeRaw =
    EatDrinkData.resturantDetailsFields?.resturantDetails?.restaurantTime;
  const RestaurantTime = Array.isArray(RestaurantTimeRaw)
    ? RestaurantTimeRaw[0]?.timeDetails?.map((content: any) => ({
        title: LanguageChanger(content, "title", lang),
        time: LanguageChanger(content, "time", lang),
      }))
    : null;
  const moreInformationButton =
    EatDrinkData?.resturantDetailsFields?.resturantDetails?.contactWebsite?.url;

  const { state }: AppContextType = useContext(AppContext);
  const [resturants, setResturants] = useState<any>([]);
  const [resturantsLoading, setResturantsLoading] = useState(false);

  const [category, setCategory] = useState<FilterPropItem>({
    options: [],
    isLoading: false,
    selectedOption: "",
    label: "filterByCategory",
    setState: (e: any) => {
      setCategory((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [destinations, setDestinations] = useState<FilterPropItem>({
    options: destinationArray,
    isLoading: false,
    selectedOption: "",
    label: "filterByDestination",
    isChange: true,
    setState: (e: any) => {
      setDestinations((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const GetEatDrinkFilter = async (
    categoryData: any,
    destinationsData: any,
    selectDestination: any
  ) => {
    setResturantsLoading(true);

    try {
      const data = await client.query({
        query: GET_EAT_AND_DRINK_FILTER({
          category: categoryData,
          destination: destinationsData.replace("hotels", "").trim(),
        }),
      });

      if (data) {
        const content = data?.data?.resturants?.edges.map((event: any) => {
          return {
            title: LanguageChanger(event.node, "title", lang),
            slug: "eat-and-drink/" + event.node?.slug,
            src: event.node?.featuredImage?.node?.mediaItemUrl,
            location: LanguageChanger(
              event.node?.resturantDetailsFields?.resturantDetails,
              "resturantLocation",
              lang
            ),
          };
        });

        if (selectDestination && destinations.isChange) {
          setDestinations((prev: any) => ({
            ...prev,
            isChange: false,
          }));
          let categorySet = new Set();
          data?.data?.resturants?.edges.forEach((event: any) => {
            const categoryData =
              event.node?.resturantDetailsFields?.resturantDetails
                ?.selectCusines?.nodes[0];
            if (categoryData && event.node?.slug !== slug) {
              categorySet.add(
                event.node?.resturantDetailsFields?.resturantDetails
                  ?.selectCusines?.nodes[0]?.name
              );
            }
          });
          let categoryArrayNew = [
            { label: "All", value: "" },
            ...Array.from(categorySet)
              .sort()
              .map((event: any) => ({
                label: event,
                value: event,
              })),
          ];
          setCategory((prev: any) => ({
            ...prev,
            options: categoryArrayNew,
          }));
        }
        setResturants(
          content?.filter((event: any) => !event.slug.includes(slug))
        );
      }
    } catch (error) {
      setResturants([]);
    }
    setResturantsLoading(false);
  };

  const restaurantsRef = useRef(null);
  const restaurantsYouMayLikeRef = useRef(null);

  const subNavbarItems = [
    {
      title: state?.localeData?.eatDrinkPage?.header?.details,
      id: restaurantsRef,
    },
    {
      title: LanguageChanger(
        getHeaderData.page?.eatDrinksPage?.sectionRestaurantsYouMayLike,
        "heading",
        lang
      ),

      id: restaurantsYouMayLikeRef,
    },
  ];

  const FeaturedPayload =
    getEatDrinkData.resturants?.edges[0].node?.resturantDetailsFields?.resturantDetails?.resturantPhotos?.nodes.map(
      (gallery: any) => ({
        node: {
          title: "",
          src: gallery?.mediaItemUrl,
        },
      })
    );

  useEffect(() => {
    setCategory((prev: any) => ({
      ...prev,
      selectedOption: "",
    }));
    GetEatDrinkFilter("", destinations.selectedOption, true);
  }, [destinations.selectedOption]);

  useEffect(() => {
    if (!destinations.isChange)
      GetEatDrinkFilter(
        category.selectedOption,
        destinations.selectedOption,
        true
      );
  }, [category.selectedOption]);

  const filterProps = {
    filterPacks: [destinations, category],
  };

  return (
    <div>
      <Banner
        title={LanguageChanger(EatDrinkData, "title", lang)}
        bannerImage={{
          src: EatDrinkData?.featuredImage?.node?.mediaItemUrl,
        }}
      />
      <SubNavbar subNavbarItems={subNavbarItems} />
      <div
        className="scroll-margin px-i-xxs py-i-80 text-center font-display text-t-subtitle-1"
        ref={restaurantsRef}
      >
        <div>{state?.localeData?.eatDrinkPage?.header?.details}</div>
      </div>
      <div className="flex flex-col gap-i-90 px-i-sm font-body text-text-main md:px-i-80 xl:px-i-20">
        <span
          dangerouslySetInnerHTML={{
            __html: LanguageChanger(
              EatDrinkData?.resturantDetailsFields?.resturantDetails,
              "resturantDetails",
              lang
            ),
          }}
        ></span>

        <div className="flex flex-col gap-i-80 lg:flex-row lg:gap-0">
          <div className="lg:w-[50%]">
            <ImageGalleryCarousel payload={FeaturedPayload} />
          </div>
          <div className="flex flex-col justify-center gap-i-sm font-body text-text-main lg:w-[50%] lg:gap-i-90 lg:pl-[20px] xl:pl-[30px]">
            {EatDrinkData?.resturantDetailsFields?.resturantDetails
              ?.resturantLocation && (
              <div className="flex flex-col gap-i-xs">
                <span className="font-bold">
                  {state?.localeData?.eatDrinkPage?.header?.location}
                </span>
                <div className="flex gap-i-xs">
                  <Image src={CarbonLocationVector} alt="Carbon Vector" />
                  <span className="font-bold">
                    {
                      EatDrinkData?.resturantDetailsFields?.resturantDetails
                        ?.resturantLocation
                    }
                  </span>
                </div>
              </div>
            )}

            {RestaurantTime && (
              <div>
                <div className="flex flex-col gap-i-90">
                  <span className="font-bold">
                    {state?.localeData?.eatDrinkPage?.header?.info}
                  </span>

                  <div className="flex gap-i-xs">
                    <div>
                      <Image src={ClockIcon} alt="Clock" />
                    </div>
                    <div>
                      {RestaurantTime?.map((data: any, index: number) => (
                        <div key={index} className="flex">
                          <p className="w-[200px]">{data.title}</p>
                          <p>{data.time}</p>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            )}

            {EatDrinkData?.resturantDetailsFields?.resturantDetails
              ?.dressCode && (
              <div className="flex items-start gap-i-xs">
                <Image src={ShirtIcon} alt="Shirt" />
                <span
                  dangerouslySetInnerHTML={{
                    __html:
                      EatDrinkData?.resturantDetailsFields?.resturantDetails
                        ?.dressCode,
                  }}
                ></span>
              </div>
            )}

            {moreInformationButton && (
              <div className="py-i-sm">
                <Button className="hover-effect focus-btn active-btn !p-i-xxs !text-t-subtitle-2">
                  <Link
                    href={moreInformationButton}
                    target={moreInformationButton ? "_blank" : "_top"}
                  >
                    {state?.localeData?.moreInformationButton}
                  </Link>
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>

      <div
        className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1"
        ref={restaurantsYouMayLikeRef}
      >
        <div>
          {LanguageChanger(
            getHeaderData.page?.eatDrinksPage?.sectionRestaurantsYouMayLike,
            "heading",
            lang
          )}
        </div>
      </div>
      <div className="px-i-90 text-center font-body font-bold text-text-main">
        {LanguageChanger(
          getHeaderData.page?.eatDrinksPage?.sectionRestaurantsYouMayLike,
          "subText",
          lang
        )}
      </div>
      <div className="px-i-90 py-i-80">
        <Filter {...filterProps} />
      </div>
      {resturantsLoading && (
        <div className="flex w-full justify-center pb-i-80">
          <Loader />
        </div>
      )}
      {!resturantsLoading && (
        <div className="pb-i-80">
          <QuincunxGrid payload={resturants} hideArrow={true} />
        </div>
      )}
    </div>
  );
};

export default EatnDrinkDetailPage;
