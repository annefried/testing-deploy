"use client";

import type { FC } from "react";
import React, { useEffect, useState } from "react";
import type { GroupBase, OptionProps } from "react-select";
import Select, { components } from "react-select";
import makeAnimated from "react-select/animated";

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="8"
      viewBox="0 0 12 8"
      fill="none"
    >
      <path
        d="M10.59 0.09L6 4.67L1.41 0.0899992L5.24537e-07 1.5L6 7.5L12 1.5L10.59 0.09Z"
        fill="black"
      />
    </svg>
  );
};

interface DropdownIndicatorProps {
  selectProps: any;
}

const DropdownIndicator: FC<DropdownIndicatorProps> = ({ selectProps }) => {
  return (
    <components.DropdownIndicator {...selectProps}>
      {<Icon />}
    </components.DropdownIndicator>
  );
};

interface CustomAsyncSelectProps<
  OptionType,
  IsMulti extends boolean,
  GroupType extends GroupBase<OptionType>,
> {
  Option: React.ComponentType<OptionProps<OptionType, IsMulti, GroupType>>;
}

const TransparentSelect = <
  OptionType,
  IsMulti extends boolean = false,
  GroupType extends GroupBase<OptionType> = GroupBase<OptionType>,
>({
  ...props
}: CustomAsyncSelectProps<OptionType, IsMulti, GroupType> | any) => {
  const animatedComponents = makeAnimated();

  const [isMounted, setIsMounted] = useState(false);
  useEffect(() => setIsMounted(true), []);

  return (
    isMounted && (
      <Select
        id={props?.id}
        labelKey="label"
        valuekey="value"
        {...props}
        isSearchable
        autoload={false}
        components={{
          animatedComponents,
          IndicatorSeparator: () => null,
          DropdownIndicator: (k) => (
            <DropdownIndicator selectProps={{ ...k }} />
          ),
        }}
        isClearable={false}
        className="react-select-container"
        styles={{
          container: (baseStyles) => ({
            ...baseStyles,
            marginLeft: props.borderLeft ? "17px" : "0px",
          }),
          control: (baseStyles) => ({
            ...baseStyles,
            border: "none",
            outline: "none",
            boxShadow: 0,
            padding: "0px 5px",
            borderRadius: "none",
            width: props?.widthFull ? "100%" : "170px",
            borderBottom: props.borderBottom ? "1px solid #E1E1E1" : "none",
            background: "transparent",
            borderLeft: props.borderLeft ? "1px solid #e5e7eb" : "none",
          }),
          menu: (provided) => ({
            ...provided,
            zIndex: 20,
            textAlign: "left",
          }),
          placeholder: (provided) => ({
            ...provided,
            margin: "10px 16px",
          }),
          singleValue: (provided) => ({
            ...provided,
            textAlign: props.borderBottom ? "left" : "center",
          }),
          option: (provided) => ({
            ...provided,
            background: "transparent",
            color: "black",
            cursor: "pointer",
          }),
        }}
        theme={(theme) => ({
          ...theme,
          borderRadius: 6,
          colors: {
            ...theme.colors,
            primary25: "#009B2F",
            primary50: "#009B2F",
            primary: "#009B2F",
            neutral50: "#A8A8A8",
            color: "#3D3712",
          },
        })}
      />
    )
  );
};

export default TransparentSelect;
