"use client";

import type { FC } from "react";
import { useRef } from "react";
import { useState } from "react";
import React from "react";
import Select from "react-select";
import type { GroupBase, OptionProps } from "react-select";
import { components } from "react-select";
import makeAnimated from "react-select/animated";

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
    >
      <path
        d="M21.0002 21L16.6572 16.657M16.6572 16.657C17.4001 15.9141 17.9894 15.0321 18.3914 14.0615C18.7935 13.0909 19.0004 12.0506 19.0004 11C19.0004 9.94936 18.7935 8.90905 18.3914 7.93842C17.9894 6.96779 17.4001 6.08585 16.6572 5.34296C15.9143 4.60007 15.0324 4.01078 14.0618 3.60874C13.0911 3.20669 12.0508 2.99976 11.0002 2.99976C9.9496 2.99976 8.90929 3.20669 7.93866 3.60874C6.96803 4.01078 6.08609 4.60007 5.34321 5.34296C3.84288 6.84329 3 8.87818 3 11C3 13.1217 3.84288 15.1566 5.34321 16.657C6.84354 18.1573 8.87842 19.0002 11.0002 19.0002C13.122 19.0002 15.1569 18.1573 16.6572 16.657Z"
        stroke="#3D3712"
        stroke-width="1.5"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};

interface DropdownIndicatorProps {
  selectProps: any;
}

const DropdownIndicator: FC<DropdownIndicatorProps> = ({ selectProps }) => {
  return (
    <components.DropdownIndicator {...selectProps}>
      {<Icon />}
    </components.DropdownIndicator>
  );
};

interface CustomAsyncSelectProps<
  OptionType,
  IsMulti extends boolean,
  GroupType extends GroupBase<OptionType>,
> {
  Option: React.ComponentType<OptionProps<OptionType, IsMulti, GroupType>>;
}
const GroupHeading = ({ hasSelectedOption, ...props }: any) => {
  const style = { ...props.style, ...hasSelectedOption };
  return (
    <div className="cursor-default px-3 font-body">
      <components.GroupHeading {...props} style={style}>
        {props.children}
      </components.GroupHeading>
    </div>
  );
};

const CustomOption = ({
  selectProps,
  label,
  data,
  setMenuIsOpen,
  ...props
}: any) => {
  const handleClick = (item: any) => {
    selectProps.onChange({
      value: item.value,
      label: item?.label,
      type: item?.type,
      link: item?.link,
    });
    setMenuIsOpen(false);
    props.setValue({
      value: item.value,
      label: item?.label,
      type: item?.type,
      link: item?.link,
    });
  };
  return (
    <div {...props} className="scrollbar px-5 py-2 font-body">
      <span className="cursor-default px-2 text-t-subtitle-1 text-text-main hover:text-primary-main">
        {label}
      </span>

      <ul>
        {data?.options?.map(
          (
            item: { value: string; label: string; type: string; link: string },
            index: number
          ) => {
            return (
              <li
                onClick={() => handleClick(item)}
                key={index}
                className="cursor-pointer px-3 py-3 text-t-subtitle-2 hover:bg-primary-main hover:text-white"
              >
                {item.label}
              </li>
            );
          }
        )}
      </ul>
    </div>
  );
};
const CustomAsyncSelect = <
  OptionType,
  IsMulti extends boolean = false,
  GroupType extends GroupBase<OptionType> = GroupBase<OptionType>,
>({
  ...props
}: CustomAsyncSelectProps<OptionType, IsMulti, GroupType> | any) => {
  const animatedComponents = makeAnimated();
  const selectRef = useRef();
  const [menuIsOpen, setMenuIsOpen] = useState(false);

  return (
    <Select
      {...props}
      autoload={false}
      ref={selectRef}
      menuIsOpen={menuIsOpen}
      components={{
        GroupHeading: (k) => (
          <GroupHeading
            setDestination={props.setDestination}
            setMenuIsOpen={setMenuIsOpen}
            selectRef={selectRef}
            {...k}
          />
        ),
        Option: (k) => <CustomOption setMenuIsOpen={setMenuIsOpen} {...k} />,
        animatedComponents,
        IndicatorSeparator: () => null,
        DropdownIndicator: (k) => <DropdownIndicator selectProps={{ ...k }} />,
      }}
      isClearable
      onMenuOpen={() => setMenuIsOpen(true)}
      onMenuClose={() => setMenuIsOpen(false)}
      className="react-select-container"
      styles={{
        control: (baseStyles, state) => ({
          ...baseStyles,
          borderColor: state.isFocused ? "#009B2F" : "#E1E1E1",
          borderWidth: "1px",
          outline: "none",
          boxShadow: 0,
          padding: "0px 5px",
          flexDirection: "row-reverse",
        }),
        clearIndicator: (base) => ({
          ...base,
          position: "absolute",
          right: 0,
        }),
        placeholder: (provided) => ({
          ...provided,
          margin: "10px 16px",
        }),
        groupHeading: (provided) => ({
          ...provided,
          color: "#009B2F",
          fontSize: "24px",
        }),
        input: (provided) => ({
          ...provided,
          padding: "5px 0px",
        }),
      }}
      theme={(theme) => ({
        ...theme,
        borderRadius: 6,
        colors: {
          ...theme.colors,
          primary25: "#009B2F",
          primary50: "#009B2F",
          primary: "#009B2F",
          neutral50: "#A8A8A8",
          color: "#3D3712",
        },
      })}
    />
  );
};

export default CustomAsyncSelect;
