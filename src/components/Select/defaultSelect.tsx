"use client";

import type { FC } from "react";
import React from "react";
import Select from "react-select";
import type { GroupBase, OptionProps } from "react-select";
import { components } from "react-select";
import makeAnimated from "react-select/animated";

const Icon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="8"
      viewBox="0 0 12 8"
      fill="none"
    >
      <path
        d="M10.59 0.09L6 4.67L1.41 0.0899992L5.24537e-07 1.5L6 7.5L12 1.5L10.59 0.09Z"
        fill="black"
      />
    </svg>
  );
};

interface DropdownIndicatorProps {
  selectProps: any;
}

const DropdownIndicator: FC<DropdownIndicatorProps> = ({ selectProps }) => {
  return (
    <components.DropdownIndicator {...selectProps}>
      {<Icon />}
    </components.DropdownIndicator>
  );
};

interface CustomAsyncSelectProps<
  OptionType,
  IsMulti extends boolean,
  GroupType extends GroupBase<OptionType>,
> {
  Option: React.ComponentType<OptionProps<OptionType, IsMulti, GroupType>>;
}

const DefaultSelect = <
  OptionType,
  IsMulti extends boolean = false,
  GroupType extends GroupBase<OptionType> = GroupBase<OptionType>,
>({
  ...props
}: CustomAsyncSelectProps<OptionType, IsMulti, GroupType> | any) => {
  const animatedComponents = makeAnimated();

  return (
    <Select
      labelKey="label"
      valuekey="value"
      {...props}
      isSearchable
      autoload={false}
      components={{
        animatedComponents,
        IndicatorSeparator: () => null,
        DropdownIndicator: (k) => <DropdownIndicator selectProps={{ ...k }} />,
      }}
      isClearable
      className="react-select-container"
      styles={{
        control: (baseStyles, state) => ({
          ...baseStyles,
          borderColor: state.isFocused ? "#009B2F" : "#E1E1E1",
          borderWidth: "1px",
          outline: "none",
          boxShadow: 0,
          padding: "0px 5px",
        }),
        placeholder: (provided) => ({
          ...provided,
          margin: "6px 0px",
        }),
        option: (provided) => ({
          ...provided,
          "cursor": "pointer",
          ":hover": {
            color: "white",
          },
        }),
        input: (provided) => ({
          ...provided,
          padding: "5px 0px",
        }),
      }}
      theme={(theme) => ({
        ...theme,
        borderRadius: 6,
        colors: {
          ...theme.colors,
          primary25: "#009B2F",
          primary50: "#009B2F",
          primary: "#009B2F",
          neutral50: "#A8A8A8",
          color: "#3D3712",
        },
      })}
    />
  );
};

export default DefaultSelect;
