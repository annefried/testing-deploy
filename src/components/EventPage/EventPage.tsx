"use client";

import type { FilterPropItem } from "@/interfaces/global_interfaces";
import Image from "next/image";
import { createRef, useEffect, useState } from "react";

import Banner from "@/components/Carousels/Banner/banner";
import Filter from "@/components/Filter/filter_modified";
import QuincunxGrid from "@/components/Layout/Grid/QuincunxGrid";
import client from "@/lib/apollo/client";
import SubNavbar from "../SubNavbar/subNavbar";
import TopBannerItems from "../TopBannerBox/TopBannerItems";

import EventContainerBackground from "@/components/Images/compass-background.png";

import { LanguageChanger } from "@/helpers/languageChanger";
import { GET_EVENT_FILTER } from "@/lib/queries/event";
import { useParams } from "next/navigation";
import Loader from "../Loader/loader";

interface eventPageProps {
  getEventData: any;
  destinationArray: any;
}

const EventPage = ({ getEventData, destinationArray }: eventPageProps) => {
  const lang = useParams()?.lang;
  const objectRef: any = {};
  const subNavbarItems: any = [];
  const mainSectionKey = Object.keys(getEventData);

  getEventData.contentSection.map((keySection: any) => {
    const title = LanguageChanger(
      keySection.theDifferenceSection?.eventsDescription,
      "heading",
      lang
    );
    objectRef[title] = createRef();
    subNavbarItems.push({ title, id: objectRef[title] });
    return title;
  });

  const [events, setEvents] = useState<any>([]);
  const [eventsLoading, setEventsLoading] = useState(false);

  const [category, setCategory] = useState<FilterPropItem>({
    options: [],
    isLoading: false,
    selectedOption: "",
    label: "filterByCategory",
    setState: (e: any) => {
      setCategory((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [destinations, setDestinations] = useState<FilterPropItem>({
    options: destinationArray,
    isLoading: false,
    selectedOption: "",
    label: "filterByDestination",
    isChange: true,
    setState: (e: any) => {
      setDestinations((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const GetEventFilter = async (
    categoryData: any,
    destinationsData: any,
    selectDestination: any
  ) => {
    setEventsLoading(true);
    try {
      const data = await client.query({
        query: GET_EVENT_FILTER({
          category: categoryData,
          destination: destinationsData.replace("hotels", "").trim(),
        }),
      });
      if (data) {
        const content = data?.data?.allMeetingevents?.nodes?.map(
          (event: any) => {
            return {
              title: LanguageChanger(event, "title", lang),
              slug:
                "hotels/" +
                event.meetingAndEvents?.selectHotel?.nodes[0]?.slug +
                "?tab=meeting/event",
              src: event.featuredImage?.node?.mediaItemUrl || "",
            };
          }
        );

        if (selectDestination && destinations.isChange) {
          setDestinations((prev: any) => ({
            ...prev,
            isChange: false,
          }));
          let categorySet = new Set();
          data?.data?.allMeetingevents?.nodes.forEach((event: any) => {
            const categoryData = event.meetingAndEvents?.category?.nodes;
            if (categoryData && categoryData.length > 0) {
              categoryData.map((setCategory: any) =>
                categorySet.add(setCategory?.name)
              );
            }
          });

          let categoryArrayNew = [
            { label: "All", value: "" },
            ...Array.from(categorySet)
              .sort()
              .map((event: any) => ({
                label: event,
                value: event,
              })),
          ];
          setCategory((prev: any) => ({
            ...prev,
            options: categoryArrayNew,
          }));
        }
        setEvents(content);
      }
    } catch (error) {
      setEvents([]);
    }
    setEventsLoading(false);
  };

  useEffect(() => {
    setCategory((prev: any) => ({
      ...prev,
      selectedOption: "",
    }));
    GetEventFilter("", destinations.selectedOption, true);
  }, [destinations.selectedOption]);

  useEffect(() => {
    if (!destinations.isChange) {
      GetEventFilter(
        category.selectedOption,
        destinations.selectedOption,
        true
      );
    }
  }, [category.selectedOption]);

  const filterProps = {
    filterPacks: [destinations, category],
  };

  return (
    <div>
      {mainSectionKey?.map((key, index: number) => {
        const subObjectPayload = getEventData[key];
        switch (key) {
          case "eventBanner":
            return (
              <div key={index}>
                <Banner
                  payload={subObjectPayload?.bannerSection?.map(
                    (bannerImage: any) => ({
                      bannerTitle: LanguageChanger(bannerImage, "title", lang),
                      mediaItemUrl: bannerImage.image?.node?.mediaItemUrl,
                      subText: LanguageChanger(bannerImage, "eventName", lang),
                    })
                  )}
                  eventPage={true}
                />
                <SubNavbar subNavbarItems={subNavbarItems} />
              </div>
            );
          case "contentSection":
            return (
              <div key={index}>
                {subObjectPayload?.map((content: any, dataIndex: number) => {
                  const theDifference = content?.theDifferenceSection;
                  const events = theDifference?.eventsDescription;
                  const whatWeCanDo = theDifference?.whatWeCanDoSectionContent;

                  return (
                    <div key={dataIndex}>
                      <div
                        className="scroll-margin"
                        ref={
                          objectRef[LanguageChanger(events, "heading", lang)]
                        }
                      >
                        <TopBannerItems
                          opacity
                          title={LanguageChanger(events, "heading", lang)}
                          description={LanguageChanger(events, "subText", lang)}
                          bannerImg={
                            events?.backgroundImage?.node?.mediaItemUrl
                          }
                        />
                      </div>
                      <div
                        className="mt-i-90 w-full bg-cover "
                        style={{
                          backgroundImage: `url(${EventContainerBackground.src})`,
                        }}
                      >
                        <div className="pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1 text-text-main xs:text-t-h5">
                          {LanguageChanger(theDifference, "heading", lang)}
                        </div>
                        <div className="flex w-full flex-col gap-i-90 px-i-sm py-i-sm pb-i-80 font-body text-text-main sm:px-i-80 xl:flex  xl:flex-row xl:px-[4.5rem]">
                          <div className="h-[450px] w-full xl:w-[50%]">
                            <Image
                              src={theDifference?.image?.node?.mediaItemUrl}
                              alt={
                                theDifference?.image?.node?.altText ??
                                "The Difference"
                              }
                              width={300}
                              height={400}
                              className="h-full w-full object-cover"
                            />
                          </div>

                          <div className="w-full xl:w-[50%]">
                            <div>
                              {LanguageChanger(theDifference, "content", lang)}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div className="pb-i-90 pt-i-70 text-center font-display text-t-subtitle-1 text-text-main xs:text-t-h5">
                          {LanguageChanger(whatWeCanDo, "heading", lang)}
                        </div>
                        <div></div>
                      </div>
                    </div>
                  );
                })}
              </div>
            );
          case "perfectEventVenueSection":
            return (
              <div key={index} className="bg-secondary-400">
                <div className="p-i-90 text-center font-display text-t-subtitle-1 text-text-main xs:text-t-h5 ">
                  {LanguageChanger(subObjectPayload, "heading", lang)}
                </div>
                <div className="p-i-90">
                  <Filter {...filterProps} />
                </div>
                {eventsLoading && (
                  <div className="flex w-full justify-center pb-i-80">
                    <Loader />
                  </div>
                )}
                {!eventsLoading && (
                  <div className="pb-i-80">
                    <QuincunxGrid payload={events} hideArrow={true} />
                  </div>
                )}
              </div>
            );
          default:
            return null;
        }
      })}
    </div>
  );
};

export default EventPage;
