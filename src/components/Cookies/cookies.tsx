"use client";
import Link from "next/link";
import "./styles.scss";
import { useParams } from "next/navigation";
import CookieConsent from "react-cookie-consent";

const CookieConsentBanner = () => {
  const locale = useParams()?.lang;
  const eng = {
    cookie: {
      content:
        "By using our website you are consenting to our user of cookies in accordance with our ",
      policy: "Privacy Policy",
      accept: "Allow cookies",
      decline: "Close",
    },
  };
  const thai = {
    cookie: {
      content:
        "การใช้เว็บไซต์ของเราแสดงว่าคุณยินยอมต่อผู้ใช้คุกกี้ของเราตามที่เรากำหนด",
      policy: "นโยบายความเป็นส่วนตัว",
      accept: "อนุญาตคุกกี้",
      decline: "ปิด",
    },
  };

  const data = locale === "en" ? eng : thai;
  return (
    <CookieConsent
      location="bottom"
      buttonText={data?.cookie?.accept}
      declineButtonText={data?.cookie?.decline}
      declineButtonClasses="decline-cookie-btn font-body"
      buttonWrapperClasses="flex md:block justify-end w-full md:w-auto"
      buttonClasses="font-body"
      hideOnAccept
      hideOnDecline
      enableDeclineButton
      expires={7}
      cookieName="CompassCookieConsent"
      style={{ background: "white" }}
      containerClasses="p-5 md:p-0"
    >
      <div className="break-word flex flex-wrap font-body text-t-subtitle-2">
        {" "}
        <p className=" text-text-main">{data?.cookie?.content}</p>
        <Link
          href={`/${locale}/privacy-policy`}
          className="ml-0 text-primary-main md:ml-2"
        >
          {data?.cookie?.policy}
        </Link>
      </div>{" "}
    </CookieConsent>
  );
};

export default CookieConsentBanner;
