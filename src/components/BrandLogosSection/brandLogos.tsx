"use client";
import Image from "next/image";
import { GET_BRAND_LOGOS } from "@/lib/queries/sections";
import client from "@/lib/apollo/client";
import { useEffect, useState } from "react";

type BrandArray = {
  brandLogo: any[];
  brandText: string;
}[];

const BrandLogos = () => {
  const [payload, setPayload] = useState<null | BrandArray>([
    { brandLogo: [], brandText: "" },
  ]);
  const fetchData = async () => {
    try {
      const data = await client.query({
        query: GET_BRAND_LOGOS,
        variables: { title: "Without Background" },
      });
      setPayload(
        data?.data?.brandsComponent?.edges[0]?.node?.brandsComponentFields
          ?.brandLogo
      );
    } catch (err) {
      if (err) {
        return setPayload(null);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (!payload) {
    return null;
  }
  const logosData = payload && payload[0];
  const brandImages =
    (payload &&
      logosData?.brandLogo?.map((images: any) => {
        return images.image?.node?.mediaItemUrl;
      })) ||
    [];

  return (
    <div className="bg-secondary-400">
      <h1 className="flex justify-center gap-2 pt-6  font-display text-t-h5">
        <span className="text-center text-text-main">
          {payload && logosData?.brandText}
        </span>
      </h1>
      <div>
        <div className="flex flex-col items-center p-6 sm:grid sm:grid-cols-4 sm:place-items-center sm:gap-i-90">
          {brandImages.map((image: string, index: number) => (
            <Image
              width={150}
              height={130}
              key={index}
              alt={`Brand Image ${index + 1}`}
              src={image}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default BrandLogos;
