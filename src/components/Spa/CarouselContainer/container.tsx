"use client";
import MultiCarousel from "@/components/Carousels/MultiCarousel/multiCarousel";
import type { BannerItem } from "@/interfaces/carousel.interface";
import Image from "next/image";
import Link from "next/link";
export default function CarouselContainer({
  payload,
}: {
  payload: BannerItem | any;
}) {
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map(
      (
        item: { title: string; src: string; price?: string; slug?: string },
        index: number
      ) => (
        <div key={index} className="mx-2">
          <div className="group relative inline-block max-h-[250px] min-h-[250px] w-full overflow-hidden xl:max-h-[520px] xl:min-h-[520px]">
            <Link href={item.slug ? item.slug : "/"}>
              <Image
                height={400}
                width={400}
                className="h-[250px] w-full object-cover transition-transform duration-500 group-hover:scale-110 xl:h-[520px]"
                alt="special-offer-img"
                src={item.src}
              />

              <section className="absolute inset-0 z-20 flex transform flex-col justify-end p-4 px-5 font-display text-white transition-transform group-hover:translate-x-1">
                <span className="text-t-subtitle-2">{item.price}</span>
                <h2 className="break-word mb-4 text-start font-display text-t-subtitle-2 xl:text-t-subtitle-1">
                  {item?.title}
                </h2>
              </section>
              <div
                className="pointer-events-none absolute inset-0 z-10"
                style={{
                  background:
                    "linear-gradient(180deg, rgba(255, 255, 255, 0.00) 0.12%, rgba(0, 0, 0, 0.80) 93.02%)",
                }}
              ></div>
            </Link>
          </div>
        </div>
      )
    );
  return (
    <>
      <div className="hidden xl:block">
        <MultiCarousel centerMode items={items} />
      </div>
      <div className="block xl:hidden">
        <MultiCarousel items={items} />
      </div>
    </>
  );
}
