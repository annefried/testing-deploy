"use client";

import type { FilterPropItem } from "@/interfaces/global_interfaces";
import Image from "next/image";
import { Suspense, createRef, useEffect, useState } from "react";

import Filter from "@/components/Filter/filter_modified";
import Banner from "../Banner/banner";
import SubNavbar from "../SubNavbar/subNavbar";
import CarouselContainer from "./CarouselContainer/container";

import { LanguageChanger } from "@/helpers/languageChanger";
import client from "@/lib/apollo/client";
import { GET_SPA_DETAIL } from "@/lib/queries/spa";
import { useParams } from "next/navigation";
import Loader from "../Loader/loader";
import TabsSection from "../Tabs/tabs";

import QuincunxGrid from "../Layout/Grid/QuincunxGrid";

interface SpaDataProps {
  spaData: any;
  destinationArray: any;
}

const SpaPage = ({ spaData, destinationArray }: SpaDataProps) => {
  const lang = useParams()?.lang;
  const sectionKey = Object.keys(spaData);
  const objectRef: any = {};
  const subNavbarItems: any = [];

  const [destinations, setDestinations] = useState<FilterPropItem>({
    options: destinationArray,
    isLoading: false,
    selectedOption: "",
    label: "filterByDestination",
    setState: (e: any) => {
      setDestinations((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [spa, setSpa] = useState<any>([]);
  const [spaLoading, setSpaLoading] = useState(false);
  const [carousel, setCarousel] = useState<any>([]);
  const [carouselLoading, setCarouselLoading] = useState(false);

  sectionKey.forEach((key) => {
    if (!["__typename", "bannerSection"].includes(key)) {
      objectRef[key] = createRef();
      subNavbarItems.push({
        title:
          key == "wellnessSection"
            ? LanguageChanger(spaData[key][0], "mainHeading", lang)
            : LanguageChanger(spaData[key], "heading", lang),
        id: objectRef[key],
      });
    }
  });

  const GetSpaFilter = async () => {
    setSpaLoading(true);
    try {
      const data = await client.query({
        query: GET_SPA_DETAIL({
          destination: destinations.selectedOption.toLowerCase(),
        }),
      });
      if (data) {
        const content = data?.data?.spaServices?.edges.map((event: any) => {
          return {
            slug: "spa/" + event.node?.slug,
            title: LanguageChanger(event.node, "title", lang),
            src: event.node?.featuredImage?.node?.mediaItemUrl,
            location: LanguageChanger(
              event.node?.spaServiceDetailsFields?.serviceDetails,
              "location",
              lang
            ),
          };
        });
        setSpa(content);
      }
    } catch (error) {
      setSpa([]);
    }
    setSpaLoading(false);
  };

  const GetSpaCarousel = async () => {
    setCarouselLoading(true);
    try {
      const data = await client.query({
        query: GET_SPA_DETAIL({}),
      });
      if (data) {
        const content = data?.data?.spaServices?.edges?.map((event: any) => {
          return {
            slug: "spa/" + event.node?.slug,
            title: LanguageChanger(event.node, "title", lang),
            src: event.node?.featuredImage?.node?.mediaItemUrl,
            // price:
            //   "Price " +
            //   event.node?.spaServiceDetailsFields?.serviceDetails?.price
            //     ?.edges[0].node?.name,
          };
        });
        setCarousel(content);
      }
    } catch (error) {
      setCarousel([]);
    }
    setCarouselLoading(false);
  };

  const onTabChange = () => {
    GetSpaCarousel();
  };

  useEffect(() => {
    GetSpaFilter();
  }, [destinations.selectedOption]);

  const filterProps = {
    filterPacks: [destinations],
  };

  return (
    <div>
      {sectionKey?.map((key, index: number) => {
        const subObjectPayload = spaData[key];
        switch (key) {
          case "bannerSection":
            return (
              <>
                <div key={index}>
                  <Banner
                    bannerImage={{
                      src: subObjectPayload.backgroundImage?.node?.mediaItemUrl,
                    }}
                    title={LanguageChanger(subObjectPayload, "heading", lang)}
                    description={LanguageChanger(
                      subObjectPayload,
                      "subHeading",
                      lang
                    )}
                  />
                </div>
                <SubNavbar subNavbarItems={subNavbarItems} />
              </>
            );
          case "wellnessSection":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 py-i-80 text-center font-display text-t-subtitle-1"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload[0], "mainHeading", lang)}
                  </div>
                </div>
                {subObjectPayload[0].content?.map(
                  (content: any, index: number) => {
                    return (
                      <div
                        key={index}
                        className="flex flex-col items-center gap-i-90 px-i-90 md:px-i-40 lg:px-i-10 xl:px-[11.5rem]"
                      >
                        <div className="flex w-full flex-col gap-i-90 sm:flex-row">
                          {content.images?.nodes?.map(
                            (item: any, index: number) => (
                              <div key={index} className="h-[350px] w-full">
                                <Image
                                  width={300}
                                  height={450}
                                  src={item.mediaItemUrl}
                                  alt={item.mediaItemUrl ?? "Spa Images"}
                                  className="h-full w-full object-cover"
                                />
                              </div>
                            )
                          )}
                        </div>

                        <div className="font-body text-text-main">
                          <div className="pb-i-90 text-center text-t-h6">
                            {LanguageChanger(content, "heading", lang)}
                          </div>
                          <div className="pb-i-80">
                            {LanguageChanger(content, "text", lang)}
                          </div>
                        </div>
                      </div>
                    );
                  }
                )}
              </div>
            );
          case "sectionExploreTreatments":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1 sm:pt-i-50"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload, "heading", lang)}
                  </div>
                </div>
                <div className="px-i-90 text-center font-body font-bold text-text-main">
                  {LanguageChanger(subObjectPayload, "subText", lang)}
                </div>
                <Suspense>
                  <TabsSection
                    onTabChange={onTabChange}
                    loading={carouselLoading}
                    tabList={[{ label: "Spa" }]}
                    tabPanelList={[
                      <CarouselContainer key="spa" payload={carousel} />,
                    ]}
                  />
                </Suspense>
              </div>
            );
          case "sectionServicesYouMayLike":
            return (
              <div key={index}>
                <div
                  className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1"
                  ref={objectRef[key]}
                >
                  <div>
                    {LanguageChanger(subObjectPayload, "heading", lang)}
                  </div>
                </div>
                <div className="px-i-90 text-center font-body font-bold text-text-main">
                  {LanguageChanger(subObjectPayload, "subText", lang)}
                </div>
                <div className="px-i-90 py-i-80">
                  <Filter {...filterProps} />
                </div>
                {spaLoading && (
                  <div className="flex w-full justify-center pb-i-80">
                    <Loader />
                  </div>
                )}
                {!spaLoading && (
                  <div className="pb-i-80">
                    <QuincunxGrid payload={spa} hideArrow={true} />
                  </div>
                )}
              </div>
            );
          default:
            return null;
        }
      })}
    </div>
  );
};

export default SpaPage;
