"use client";
import type {
  AppContextType,
  FilterPropItem,
} from "@/interfaces/global_interfaces";
import client from "@/lib/apollo/client";
import AppContext from "@/lib/context";
import { GET_SPA_DETAIL } from "@/lib/queries/spa";
import Image from "next/image";
import Link from "next/link";
import { useContext, useEffect, useRef, useState } from "react";

import Banner from "@/components/Banner/banner";
import SubNavbar from "@/components/SubNavbar/subNavbar";
import Button from "../Button/button";
import Filter from "../Filter/filter_modified";
import QuincunxGrid from "../Layout/Grid/QuincunxGrid";
import Loader from "../Loader/loader";

import CarbonLocationVector from "@/components/icons/CarbonLocationVector.svg";
import { LanguageChanger } from "@/helpers/languageChanger";
import { useParams } from "next/navigation";
import ImageGalleryCarousel from "../ImageGalleryCarousel/ImageGalleryCarousel";

interface spaDataProps {
  getSpaData: any;
  getSpaContent: any;
  getSpaServiceHeader: any;
  moreInfomrationButton: string;
  slug: string | undefined;
  destinationArray: any;
}

const SpaDetailPage = ({
  getSpaData,
  getSpaContent,
  getSpaServiceHeader,
  moreInfomrationButton,
  slug,
  destinationArray,
}: spaDataProps) => {
  const lang = useParams()?.lang;
  const { state }: AppContextType = useContext(AppContext);

  const [destinations, setDestinations] = useState<FilterPropItem>({
    options: destinationArray,
    isLoading: false,
    selectedOption: "",
    label: "filterByDestination",
    setState: (e: any) => {
      setDestinations((prev: any) => ({
        ...prev,
        ...e,
      }));
    },
  });

  const [spa, setSpa] = useState<any>([]);
  const [spaLoading, setSpaLoading] = useState(false);

  const serviceDetailRef = useRef(null);
  const servicesRef = useRef(null);

  const subNavbarItems = [
    {
      title: state?.localeData?.spaPage?.header?.detail,
      id: serviceDetailRef,
    },
    {
      title: LanguageChanger(
        getSpaServiceHeader.sectionServicesYouMayLike,
        "heading",
        lang
      ),
      id: servicesRef,
    },
  ];

  const FeaturedPayload =
    getSpaData.spaServiceDetailsFields?.serviceDetails?.serviceImages?.edges.map(
      (gallery: any) => ({
        node: {
          title: "",
          src: gallery.node?.mediaItemUrl,
        },
      })
    );

  const GetSpaFilter = async () => {
    setSpaLoading(true);
    try {
      const data = await client.query({
        query: GET_SPA_DETAIL({ destination: destinations.selectedOption }),
      });
      if (data) {
        const content = data?.data?.spaServices?.edges.map((event: any) => {
          return {
            slug: "spa/" + event.node?.slug,

            title: LanguageChanger(event.node, "title", lang),
            src: event.node?.featuredImage?.node?.mediaItemUrl,
            location: LanguageChanger(
              event.node?.spaServiceDetailsFields?.serviceDetails,
              "location",
              lang
            ),
          };
        });
        setSpa(content?.filter((event: any) => !event.slug.includes(slug)));
      }
    } catch (error) {
      setSpa([]);
    }
    setSpaLoading(false);
  };

  useEffect(() => {
    GetSpaFilter();
  }, [destinations.selectedOption]);

  const filterProps = {
    filterPacks: [destinations],
  };

  return (
    <div>
      <Banner
        title={LanguageChanger(getSpaData, "title", lang)}
        bannerImage={{ src: getSpaData?.featuredImage?.node?.mediaItemUrl }}
      />
      <SubNavbar subNavbarItems={subNavbarItems} />
      <div
        className="scroll-margin px-i-xxs py-i-80 text-center font-display text-t-subtitle-1"
        ref={serviceDetailRef}
      >
        <div>{state?.localeData?.spaPage?.header?.detail}</div>
      </div>

      <div className="flex flex-col gap-i-80 px-i-sm font-body text-text-main md:px-i-80 xl:px-i-20">
        <span>
          {LanguageChanger(
            getSpaContent?.serviceDetails,
            "introductionText",
            lang
          )}
        </span>

        <div className="flex flex-col gap-i-80 lg:flex-row lg:gap-0">
          <div className="lg:w-[50%]">
            <ImageGalleryCarousel payload={FeaturedPayload} />
          </div>
          <div className="flex flex-col justify-center gap-i-sm font-body text-text-main lg:w-[50%] lg:gap-i-90 lg:pl-[20px] xl:pl-[30px]">
            <div className="list-disc-hight-light">
              <span className="font-bold">
                {state?.localeData?.spaPage?.header?.highlight}
              </span>
              <div
                dangerouslySetInnerHTML={{
                  __html: LanguageChanger(
                    getSpaContent.serviceDetails?.highlightSection,
                    "content",
                    lang
                  ),
                }}
              ></div>
            </div>
            {getSpaContent.serviceDetails.priceDetails && (
              <div className="flex flex-col">
                <span className="font-bold">
                  {state?.localeData?.spaPage?.header?.priceDetails}
                </span>
                <span
                  dangerouslySetInnerHTML={{
                    __html: LanguageChanger(
                      getSpaContent.serviceDetails,
                      "priceDetails",
                      lang
                    ),
                  }}
                ></span>
              </div>
            )}

            <div className="flex gap-i-xs">
              <Image src={CarbonLocationVector} alt="Carbon Vector" />
              <span className="font-bold">
                {LanguageChanger(
                  getSpaContent.serviceDetails,
                  "location",
                  lang
                )}
              </span>
            </div>

            {moreInfomrationButton && (
              <div className="py-i-sm">
                <Button className="hover-effect focus-btn active-btn !p-i-xxs !text-t-subtitle-2">
                  <Link href={moreInfomrationButton} target="_blank">
                    {state?.localeData?.moreInformationButton}
                  </Link>
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>

      <div
        className="scroll-margin px-i-90 pb-i-sm pt-i-80 text-center font-display text-t-subtitle-1"
        ref={servicesRef}
      >
        <div>
          {LanguageChanger(
            getSpaServiceHeader.sectionServicesYouMayLike,
            "heading",
            lang
          )}
        </div>
      </div>
      <div className="px-i-90 text-center font-body font-bold text-text-main">
        {LanguageChanger(
          getSpaServiceHeader.sectionServicesYouMayLike,
          "subText",
          lang
        )}
      </div>
      <div className="px-i-90 py-i-80">
        <Filter {...filterProps} />
      </div>
      {spaLoading && (
        <div className="flex w-full justify-center pb-i-80">
          <Loader />
        </div>
      )}
      {!spaLoading && (
        <div className="pb-i-80">
          <QuincunxGrid payload={spa} hideArrow={true} />
        </div>
      )}
    </div>
  );
};

export default SpaDetailPage;
