"use client";
import { forwardRef, useContext, useEffect, useState } from "react";
import SpecialOffers from "@/components/SpecialOffers/specialOffer";
import AppContext from "@/lib/context";
import type { AppContextType } from "@/interfaces/global_interfaces";
import Image from "next/image";
import Link from "next/link";
import client from "@/lib/apollo/client";
import Date from "@/components/icons/Home/Date.svg";
import Loader from "@/components/Loader/loader";
import { GET_FEATURED_OFFERS } from "@/lib/queries/offers";
import type { BannerItem } from "@/interfaces/banner.interface";
import { useMediaQuery } from "react-responsive";
const FeaturedOffers = forwardRef(
  (
    props: {
      title: string;
      description: string;
      lang: string | any;
    },
    ref
  ) => {
    const mobileScreen = useMediaQuery({ maxWidth: 767 });
    const { state }: AppContextType = useContext(AppContext);
    const { title = "", description, lang } = props;
    const [offers, setOffers] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchData = async () => {
      try {
        const data = await client.query({
          query: GET_FEATURED_OFFERS,
          variables: {
            first: 5,
          },
        });
        setOffers(data.data?.promotionsAndOffers?.edges);
        setLoading(false);
      } catch (err) {
        if (err) {
          return setOffers([]);
        }
        setLoading(false);
      }
    };

    useEffect(() => {
      fetchData();
    }, []);

    if (loading) {
      return (
        <div className="my-[200px] flex w-full justify-center">
          <Loader />
        </div>
      );
    }

    if (!loading && offers?.length === 0) {
      return null;
    }

    const items =
      offers && offers.length !== 0
        ? offers?.map(({ node }: BannerItem | any, index: number) => (
            <div
              key={index}
              className="group relative inline-block max-h-[300px]  min-h-[300px] w-full flex-shrink-0 overflow-hidden  lg:max-h-[500px] lg:min-h-[500px] xl:max-h-[700px] xl:min-h-[700px]"
            >
              <Link href={`/${lang}/offers/${node?.slug}`}>
                <Image
                  src={node?.featuredImage?.node?.mediaItemUrl}
                  alt={node?.featuredImage?.node?.altText ?? node?.title}
                  width={400}
                  height={400}
                  style={{ height: "auto", width: "100%" }}
                  className="object-cover transition-transform duration-500 group-hover:scale-110"
                />

                <section className="absolute inset-0 z-20 flex transform flex-col justify-end p-4 font-display text-white">
                  <div className="mb-4 flex transition-transform group-hover:translate-x-1">
                    <section>
                      {" "}
                      <Image
                        className="mt-[4.7px]"
                        src={Date}
                        alt="icon"
                        width={14}
                        height={15}
                      />
                    </section>
                    <span className="ml-2 mt-[1px] font-body text-t-subtitle-2">
                      {`${state?.localeData?.validityText} 20/12/2024`}
                    </span>
                  </div>
                  <h2 className="break-word lg-text-overflow-clamp text-start font-display text-t-subtitle-1 lg:text-t-h5">
                    {node?.title}
                  </h2>
                  <div
                    className="text-overflow-clamp break-word mt-3 flex text-start font-body text-t-caption lg:text-t-subtitle-2"
                    dangerouslySetInnerHTML={{
                      __html: node?.offerMain?.offerIntroduction,
                    }}
                  ></div>
                </section>

                <div
                  className="absolute inset-0 z-10"
                  style={{
                    background:
                      "linear-gradient(180deg, rgba(50, 49, 40, 0.00) 0%, rgba(50, 49, 40, 0.58) 78.55%, #000 100%)",
                  }}
                ></div>
                {node?.offerMain?.selectOfferTag?.nodes?.length !== 0 && (
                  <div className="absolute left-4 z-20 flex flex-wrap">
                    {node?.offerMain?.selectOfferTag?.nodes
                      ?.slice(0, mobileScreen ? 2 : undefined)
                      ?.map((item: { name: string }, index: number) => (
                        <section
                          key={index}
                          className="mx-3 bg-red-900 px-4 py-2"
                        >
                          <h4 className="my-4 font-body text-t-span font-bold uppercase text-white">
                            {item?.name}
                          </h4>
                        </section>
                      ))}
                  </div>
                )}
              </Link>
            </div>
          ))
        : [];

    return offers?.length !== 0 ? (
      <SpecialOffers
        title={title}
        description={description}
        offers={items}
        ref={ref}
        payload={offers}
        lang={lang}
      />
    ) : (
      <></>
    );
  }
);
FeaturedOffers.displayName = "FeaturedOffers";
export default FeaturedOffers;
