"use client";
import Date from "@/components/icons/Home/Date.svg";
import Image from "next/image";
import type { BannerItem } from "@/interfaces/carousel.interface";
import type { AppContextType } from "@/interfaces/global_interfaces";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";

export default function CarouselContainer({
  payload,
  state,
  lang,
}: {
  payload: BannerItem | any;
  state: AppContextType | any;
  offers?: any;
  lang?: string;
}) {
  const mobileScreen = useMediaQuery({ maxWidth: 767 });
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map(({ node }: any, index: number) => (
      <div
        key={index}
        className="group relative inline-block max-h-[300px]  min-h-[300px] w-full flex-shrink-0 overflow-hidden  lg:max-h-[500px] lg:min-h-[500px]"
      >
        <Link href={`/${lang}/offers/${node?.slug}`}>
          <Image
            src={node?.featuredImage?.node?.mediaItemUrl}
            alt={node?.featuredImage?.node?.altText ?? node?.title}
            sizes="100%"
            fill
            className="object-cover transition-transform duration-500 group-hover:scale-110"
          />

          <section className="absolute inset-0 z-20 flex transform flex-col justify-end p-4 font-display text-white">
            <div className="mb-4 flex transition-transform group-hover:translate-x-1">
              <section>
                {" "}
                <Image
                  className="mt-[4.7px]"
                  src={Date}
                  alt="icon"
                  width={14}
                  height={15}
                />
              </section>
              <span className="ml-2 mt-[1px] font-body text-t-subtitle-2">
                {`${state?.localeData?.validityText} 20/12/2024`}
              </span>
            </div>
            <h2 className="lg-text-overflow-clamp break-word text-start font-display text-t-subtitle-1 lg:text-t-h5">
              {node?.title}
            </h2>
            <div
              className="lg-text-overflow-clamp break-word mt-3 flex flex-col text-start font-body text-t-caption lg:text-t-subtitle-2"
              dangerouslySetInnerHTML={{
                __html: node?.offerMain?.offerIntroduction,
              }}
            ></div>
          </section>

          <div
            className="absolute inset-0 z-10"
            style={{
              background:
                "linear-gradient(180deg, rgba(50, 49, 40, 0.00) 0%, rgba(50, 49, 40, 0.58) 78.55%, #000 100%)",
            }}
          ></div>
          {node?.offerMain?.selectOfferTag?.nodes?.length !== 0 && (
            <div className="absolute left-4 z-20 flex flex-wrap">
              {node?.offerMain?.selectOfferTag?.nodes
                ?.slice(0, mobileScreen ? 2 : undefined)
                ?.map((item: { name: string }, index: number) => (
                  <section
                    key={index}
                    className="mx-3 mb-2 bg-red-900 px-4 py-2"
                  >
                    <h4 className="font-body text-t-span font-bold uppercase text-white">
                      {item?.name}
                    </h4>
                  </section>
                ))}
            </div>
          )}
        </Link>
      </div>
    ));
  return (
    <div>
      <ResponsiveCarousel items={items} centerMode payload={payload} />
    </div>
  );
}
