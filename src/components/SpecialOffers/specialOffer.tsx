"use client";
import { forwardRef, useContext } from "react";
import type { RefObject } from "react";
import type { OfferProps } from "@/interfaces/home.interface";
import AppContext from "@/lib/context";
import type { AppContextType } from "@/interfaces/global_interfaces";
import CarouselContainer from "./CarouselContainer/container";
const SpecialOffers = forwardRef((props: OfferProps | any, ref) => {
  const { state }: AppContextType = useContext(AppContext);
  const { title = "", description, payload, lang } = props;

  return (
    <div
      ref={ref as RefObject<HTMLInputElement>}
      className="scroll-margin bg-white py-8 text-center text-text-main"
    >
      <h3 className="mb-4 font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
        {title}
      </h3>
      {description && (
        <p className="mb-8 font-body text-t-subtitle-2">{description}</p>
      )}

      <CarouselContainer payload={payload} state={state} lang={lang} />
    </div>
  );
});
SpecialOffers.displayName = "SpecialOffers";
export default SpecialOffers;
