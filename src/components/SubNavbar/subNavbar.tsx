import type { RefObject } from "react";

interface PropTypes {
  subNavbarItems: {
    title: string;
    id: RefObject<HTMLDivElement>;
  }[];
}

const SubNavbar = (props: PropTypes) => {
  const { subNavbarItems } = props;
  const scrollToSection = (sectionRef: RefObject<HTMLDivElement>) => {
    if (sectionRef && sectionRef.current) {
      sectionRef.current.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };

  return (
    <div className="sticky left-0 top-[70px] z-20 mx-auto mb-5 hidden w-full bg-white px-2 py-5 font-body text-t-caption text-text-main shadow  lg:block xl:px-[3.8rem]">
      <ul className="flex flex-wrap">
        {subNavbarItems?.length !== 0 &&
          subNavbarItems?.map((item, index) => {
            return (
              <li className="mx-4" key={index}>
                <button
                  className="hover:text-primary-main"
                  onClick={() => scrollToSection(item.id)}
                  type="button"
                >
                  {item?.title}
                </button>
              </li>
            );
          })}
      </ul>
    </div>
  );
};

export default SubNavbar;
