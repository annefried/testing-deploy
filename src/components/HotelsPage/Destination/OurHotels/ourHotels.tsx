import { forwardRef, useContext, useEffect, useState } from "react";
import type { RefObject } from "react";
import QuincunxGrid from "@/components/Layout/Grid/QuincunxGrid";
import type {
  AppContextType,
  OptionsType,
  States,
} from "@/interfaces/global_interfaces";
import LoadOptions from "@/helpers/loadOptions";
import client from "@/lib/apollo/client";
import AppContext from "@/lib/context";
import Filter from "@/components/Filter/filter";
import { useParams } from "next/navigation";
import {
  GET_HOTELS_BY_CITIES,
  GET_HOTELS_BY_COUNTRY,
} from "@/lib/queries/hotels";
import Loader from "@/components/Loader/loader";

const OurHotels = forwardRef(
  (
    props: {
      title: string;
      description?: string;
      showFiltering?: boolean;
      city?: string;
      lang?: any;
    },
    ref
  ) => {
    const countrySlug = useParams()?.country;
    const citySlug = useParams()?.city;
    const { state }: AppContextType = useContext(AppContext);
    const staticOptions = [
      { value: "all", label: props.lang === "en" ? "All" : "ทั้งหมด" },
    ];
    const [destinations, setDestinations] = useState<States>({
      optionsLoaded: false,
      options: [],
      isLoading: false,
      selectedOption: staticOptions,
    });
    const [loading, setLoading] = useState<boolean>(false);

    const { title = "", description = "", showFiltering = false } = props;
    const [data, setData] = useState({
      edges: [],
      pageInfo: {},
    });
    const formattedPayload = data?.edges?.map(({ node }: any) => ({
      title: node?.title,
      src: {
        src: node?.hotelDetails?.hotelSection?.bannerImage?.node?.mediaItemUrl,
      },
      slug: `hotels/${node.slug}`,
      price:
        node?.hotelDetails?.hotelSection?.selectPriceRange?.edges[0]?.node
          ?.name,
    }));
    const handleFetchMore = async () => {
      setLoading(true);
      try {
        const response = await client.query({
          query: props.city
            ? GET_HOTELS_BY_CITIES(citySlug)
            : GET_HOTELS_BY_COUNTRY(
                countrySlug,
                1000000,
                destinations?.selectedOption !== "all" &&
                  destinations?.selectedOption?.length !== 0 &&
                  destinations?.selectedOption[0]?.value !== "all"
                  ? destinations?.selectedOption[0]?.value
                  : null
              ),
        });
        const data = response?.data?.hotels;
        if (data) {
          setData({
            edges: data?.edges,
            pageInfo: {},
          });
          setLoading(false);
        }
      } catch (err) {
        setData({
          edges: [],
          pageInfo: { endCursor: "", hasNextPage: false },
        });
        setLoading(false);
      }
    };

    useEffect(() => {
      handleFetchMore();
    }, []);

    const fetchOptions = async (
      setData: React.Dispatch<React.SetStateAction<OptionsType>>
    ) => {
      const seenSet = new Set<string>();
      data?.edges?.map((k: any) => {
        const destination =
          k?.node?.hotelDetails?.hotelSection?.selectDestination?.nodes?.filter(
            (type: { description: string }) => type?.description !== "COUNTRY"
          );
        if (destination?.length !== 0) {
          destination.forEach((destination: any) => {
            const { name } = destination;
            if (name) {
              seenSet.add(name);
            }
          });
        }
      });

      const convertToOptionArray = Array.from(seenSet)?.map(
        (option: string) => ({
          value: option,
          label: option,
        })
      );

      const allOptions = [...staticOptions, ...(convertToOptionArray || [])];
      setData((prevDestinations) => ({
        ...prevDestinations,
        optionsLoaded: true,
        options: allOptions,
        isLoading: false,
      }));
    };

    useEffect(() => {
      if (destinations?.selectedOption?.length !== 0) {
        handleFetchMore();
      }
    }, [destinations?.selectedOption]);

    const handleDestinationSelect = (e: { value: string; label: string }) => {
      setDestinations((prev: any) => ({
        ...prev,
        selectedOption: [{ value: e.value, label: e.label }],
      }));
    };

    const filterProps = {
      hideBorder: true,
      destinationOptions: destinations.options,
      destinationSelectedOption: destinations.selectedOption,
      destinationLabel: state?.localeData?.filterByDestination,
      handleDestinationSelect: handleDestinationSelect,
      showDestination: true,
      destinationsIsLoading: destinations.isLoading,
      borderLeft: true,
      loadDestinationsOptions: () =>
        LoadOptions(destinations, setDestinations, fetchOptions, "city"),
    };

    return (
      <div
        ref={ref as RefObject<HTMLInputElement>}
        className="scroll-margin breaks-word mx-0 bg-white py-8 text-text-main xl:mx-[80px]"
      >
        <h3 className="mb-2 text-center font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
          {title}
        </h3>
        {description && (
          <p className="mb-6 text-center font-body text-t-subtitle-2 lg:text-t-subtitle-3">
            {description}
          </p>
        )}

        {showFiltering && (
          <div className="mb-8 w-full">
            <Filter {...filterProps} />
          </div>
        )}
        {loading && (
          <div className="my-[200px] flex w-full justify-center">
            <Loader />
          </div>
        )}
        {formattedPayload?.length === 0 && !loading && (
          <div className="my-[200px]">
            <h3 className="text-center font-body text-t-subtitle-2 tracking-normal">
              {state?.localeData?.noResultsFound}
            </h3>
          </div>
        )}

        {formattedPayload?.length !== 0 && !loading && (
          <QuincunxGrid
            payload={formattedPayload}
            urlText={state?.localeData?.hotels?.viewInfo}
          />
        )}
      </div>
    );
  }
);
OurHotels.displayName = "OurHotels";
export default OurHotels;
