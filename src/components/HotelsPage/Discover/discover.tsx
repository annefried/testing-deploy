"use client";
import { useRef } from "react";
import OurHotels from "../Destination/OurHotels/ourHotels";
import SubNavbar from "@/components/SubNavbar/subNavbar";
import Banner from "@/components/Banner/banner";
import type { HotelProps } from "@/interfaces/hotels.interface";
import RelatedOffers from "./RelatedOffers/relatedOffers";
const Discover = (props: HotelProps) => {
  const ourHotelsRef = useRef(null);
  const relatedOffersRef = useRef(null);
  const {
    sectionDestinationBanner,
    taglineUnderHotelHeading,
    taglineForOffers,
    lang,
    relatedOffers,
  } = props;
  const translatedTile = `${lang === "en" ? "Our Hotels in" : "โรงแรมใน"} ${sectionDestinationBanner?.bannerTitle}`;

  const subNavbarItems = [
    {
      title: translatedTile,
      id: ourHotelsRef,
    },
    {
      title: taglineForOffers?.heading,
      id: relatedOffersRef,
    },
  ];

  return (
    <div>
      <Banner
        title={sectionDestinationBanner?.bannerTitle}
        description={sectionDestinationBanner?.subText}
        bannerImage={{
          src: sectionDestinationBanner?.backgroundImage?.node?.mediaItemUrl,
        }}
        alt={sectionDestinationBanner?.backgroundImage?.node?.altText}
      />
      <SubNavbar subNavbarItems={subNavbarItems} />
      <OurHotels
        title={translatedTile}
        ref={ourHotelsRef}
        description={taglineUnderHotelHeading?.subText}
        showFiltering={true}
        lang={lang}
      />
      <RelatedOffers
        ref={relatedOffersRef}
        lang={lang}
        title={taglineForOffers?.heading}
        description={taglineForOffers?.subText}
        relatedOffers={relatedOffers}
      />
    </div>
  );
};

export default Discover;
