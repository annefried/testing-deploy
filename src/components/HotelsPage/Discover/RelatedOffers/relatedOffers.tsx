"use client";
import SpecialOffers from "@/components/SpecialOffers/specialOffer";
import Date from "@/components/icons/Home/Date.svg";
import type { BannerItem } from "@/interfaces/banner.interface";
import type { AppContextType } from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import Image from "next/image";
import Link from "next/link";
import { forwardRef, useContext } from "react";

const RelatedOffers = forwardRef(
  (
    props: {
      title: string;
      description: string;
      lang: string | any;
      relatedOffers?: any;
    },
    ref
  ) => {
    const { state }: AppContextType = useContext(AppContext);
    const { title = "", description, lang } = props;

    if (props.relatedOffers?.length === 0) {
      return null;
    }

    const items =
      props.relatedOffers && props.relatedOffers.length !== 0
        ? props.relatedOffers?.map(
            ({ node }: BannerItem | any, index: number) => (
              <div
                key={index}
                className="relative max-h-[300px] min-h-[300px]  w-full flex-shrink-0 lg:max-h-[500px] lg:min-h-[500px]  xl:max-h-[700px] xl:min-h-[700px]"
              >
                <Link href={`/${lang}/offers/${node?.slug}`}>
                  <Image
                    src={node?.featuredImage?.node?.mediaItemUrl}
                    alt={node?.featuredImage?.node?.altText ?? node?.title}
                    width={400}
                    height={400}
                    style={{ height: "auto", width: "100%" }}
                  />

                  <section className=" absolute inset-0 z-20 flex transform flex-col justify-end p-4 font-display text-white">
                    <div className="mb-4 flex transition-transform hover:translate-x-1">
                      <section>
                        {" "}
                        <Image
                          className="mt-[4.7px]"
                          src={Date}
                          alt="icon"
                          width={14}
                          height={15}
                        />
                      </section>
                      <span className="ml-2 mt-[1px] font-body text-t-subtitle-2">
                        {`${state?.localeData?.validityText} 20/12/2024`}
                      </span>
                    </div>
                    <h2 className="break-word text-start font-display text-t-subtitle-1 lg:text-t-h5">
                      {node?.title}
                    </h2>
                    <div
                      className="text-overflow-clamp break-word mt-3 flex text-start font-body text-t-caption lg:text-t-subtitle-2"
                      dangerouslySetInnerHTML={{
                        __html: node?.offerMain?.offerIntroduction,
                      }}
                    ></div>
                  </section>

                  <div
                    className="absolute inset-0 z-10"
                    style={{
                      background:
                        "linear-gradient(180deg, rgba(50, 49, 40, 0.00) 0%, rgba(50, 49, 40, 0.58) 78.55%, #000 100%)",
                    }}
                  ></div>
                  {node?.offerMain?.selectOfferTag?.nodes[0]?.name && (
                    <div className="absolute left-4 z-20 bg-red-900 px-4 py-2">
                      <h4 className="font-body text-t-span font-bold uppercase text-white">
                        {node?.offerMain?.selectOfferTag?.nodes[0]?.name}
                      </h4>
                    </div>
                  )}
                </Link>
              </div>
            )
          )
        : [];

    return props.relatedOffers?.length !== 0 ? (
      <SpecialOffers
        title={title}
        description={description}
        offers={items}
        ref={ref}
        payload={props.relatedOffers}
        lang={lang}
      />
    ) : (
      <></>
    );
  }
);
RelatedOffers.displayName = "RelatedOffers";
export default RelatedOffers;
