"use client";
import BedIcon from "@/components/icons/Hotels/Bed.svg";
import PersonIcon from "@/components/icons/Hotels/Person.svg";
import Image from "next/image";
import type { AppContextType } from "@/interfaces/global_interfaces";
import { useContext } from "react";
import AppContext from "@/lib/context";
import Link from "next/link";
import Button from "@/components/Button/button";
export default function RoomDetails({
  title,
  description,
  occupany,
  bedroomType,
  price,
  link,
}: {
  title: string;
  description?: string;
  occupany?: string;
  bedroomType?: string;
  price?: string;
  link: string;
}) {
  const { state }: AppContextType = useContext(AppContext);
  return (
    <div className="break-word bg-white px-4 py-4 text-text-main lg:px-[80px] lg:py-12">
      <h2 className="font-600 lg:text-t-t-h4 mb-4 text-left font-display text-t-subtitle-1 ">
        {title}
      </h2>
      {description && (
        <p className="text-left font-display text-t-caption lg:text-t-subtitle-2">
          {description}
        </p>
      )}
      {occupany && (
        <div className="my-3 hidden items-center font-body sm:flex">
          <section>
            {" "}
            <Image
              src={PersonIcon}
              alt="icon"
              className="object-scale-down"
              width={24}
              height={17}
            />
          </section>
          <span className="ml-2 mt-[2px] text-t-subtitle-2">{occupany}</span>
        </div>
      )}
      {bedroomType && (
        <div className="hidden items-center font-body sm:flex">
          <section>
            {" "}
            <Image src={BedIcon} alt="icon" width={24} height={25} />
          </section>
          <span className="ml-2 text-t-subtitle-2">{bedroomType}</span>
        </div>
      )}
      {price && (
        <section className="my-4">
          <h5 className="mb-2 font-body text-t-subtitle-2">
            {state?.localeData?.hotels?.rate}
          </h5>
          <h4 className="font-600 font-display text-t-subtitle-1 lg:text-t-h5">
            {`${price} / ${state?.localeData?.hotels?.perNight}`}
          </h4>
        </section>
      )}
      <Link
        className="text-t-subtitle-2 md:text-t-h6"
        target="blank"
        href={link}
      >
        <Button className="hover-effect focus-btn mt-2">
          {state?.localeData?.navbar?.bookNowText}
        </Button>
      </Link>
    </div>
  );
}
