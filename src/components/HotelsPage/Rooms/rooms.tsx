"use client";
import Banner from "@/components/Banner/banner";
import Gallery from "@/components/Layout/Grid/Gallery/galleryGrid";
import Facilities from "../Facilities/facilities";
import AppContext from "@/lib/context";
import { useContext, useEffect } from "react";
import type { AppContextType } from "@/interfaces/global_interfaces";
import RoomDetails from "./details";
import type { HotelRoomsProps } from "@/interfaces/hotels.interface";
import Rooms from "../Hotel/Rooms/rooms";
import { useParams, usePathname } from "next/navigation";
export default function HotelRooms(props: HotelRoomsProps) {
  const pathName = usePathname();
  const params = useParams()?.hotel;
  const { state, dispatch }: AppContextType = useContext(AppContext);
  const { node, hotelDetails, hotelSlug, lang, roomFeatures, roomsData } =
    props;
  const hotelInfo = hotelDetails?.hotelDetails?.hotelSection;

  const details = node?.roomsFields?.roomsDetails;

  useEffect(() => {
    if (hotelDetails?.slug === params) {
      dispatch?.setHotelPayload({
        title: hotelDetails?.title,
        url: hotelInfo?.bookUrl?.url,
      });
    }
    return () => {
      dispatch?.setHotelPayload(null);
    };
  }, [pathName]);
  return (
    <div>
      <Banner
        title={node?.title}
        description={details?.subText}
        bannerImage={details?.bannerImage?.node?.mediaItemUrl ?? ""}
        alt={details?.bannerImage?.node?.altText}
      />
      <RoomDetails
        title={node?.title}
        description={details?.subText}
        occupany={details?.occupancy}
        bedroomType={details?.bedding?.join(", ")}
        price={hotelInfo?.selectPriceRange?.edges[0]?.node?.name}
        link={hotelInfo?.bookUrl?.url ?? "/"}
      />
      <Facilities facilities={roomFeatures?.nodes} />

      <Gallery
        title={state?.localeData?.hotels?.roomGallery}
        payload={details?.roomGallery?.nodes}
      />

      {roomsData?.length !== 0 && (
        <div className="my-8">
          <h3 className="mb-5 text-center font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
            {state?.localeData?.hotels?.roomType}
          </h3>
          <Rooms
            loading={false}
            payload={roomsData}
            hotelUrl={`/${lang}/hotels/${hotelSlug}`}
          />
        </div>
      )}
    </div>
  );
}
