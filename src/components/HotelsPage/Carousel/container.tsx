"use client";
import Image from "next/image";
import type { AppContextType } from "@/interfaces/global_interfaces";
import ResponsiveCarousel from "@/components/Carousels/ResponsiveCarousel/carousel";
import PersonIcon from "@/components/icons/Person.svg";
import BedIcon from "@/components/icons/Bed.svg";
import Button from "@/components/Button/button";
import Link from "next/link";
import { useContext } from "react";
import AppContext from "@/lib/context";
import { useRouter } from "next/navigation";
export default function Carousel({
  payload,
  hideBookNow,
  hotelUrl,
  hotelReservationUrl,
}: {
  payload: any;
  hideBookNow?: boolean;
  hotelUrl?: string;
  hotelReservationUrl?: string;
}) {
  const { state }: AppContextType = useContext(AppContext);
  const router = useRouter();
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map(({ node }: any, index: number) => (
      <div
        key={index}
        className="relative max-h-[300px] min-h-[300px]  w-full flex-shrink-0 md:max-h-[500px] md:min-h-[500px]  xl:max-h-[550px] xl:min-h-[550px]"
      >
        <Link
          href={`${hotelUrl}/rooms/${node?.title?.toLowerCase()?.replace(/\s+/g, "-")}`}
        >
          {" "}
          <Image
            src={
              node?.roomsFields?.roomsDetails?.bannerImage?.node?.mediaItemUrl
            }
            alt={
              node?.roomsFields?.roomsDetails?.bannerImage?.node?.altText ??
              node?.title
            }
            sizes="100%"
            fill
            className="object-cover"
          />
        </Link>
        <section className="break-word absolute inset-0 z-20 flex flex-col justify-end px-4 py-4 text-start font-display text-white lg:px-12">
          <Link
            href={`${hotelUrl}/rooms/${node?.title?.toLowerCase()?.replace(/\s+/g, "-")}`}
          >
            {" "}
            <h2 className="lg-text-overflow-clamp font-display text-t-subtitle-2 md:text-t-h5">
              {node?.title}
            </h2>
            {node?.roomsFields?.roomsDetails?.subText && (
              <div className="hidden sm:block">
                <p className="lg-text-overflow-clamp my-3  font-body text-t-subtitle-2 ">
                  {node?.roomsFields?.roomsDetails?.subText}
                </p>
              </div>
            )}
            {node?.roomsFields?.roomsDetails?.occupancy && (
              <div className="my-3 flex items-center font-body">
                <section>
                  {" "}
                  <Image src={PersonIcon} alt="icon" width={24} height={25} />
                </section>
                <span className="ml-2 mt-[2px] text-t-subtitle-2">
                  {node?.roomsFields?.roomsDetails?.occupancy}
                </span>
              </div>
            )}
            {node?.roomsFields?.roomsDetails?.bedding &&
              node?.roomsFields?.roomsDetails?.bedding?.length !== 0 && (
                <div className="flex items-center font-body">
                  <section>
                    {" "}
                    <Image src={BedIcon} alt="icon" width={24} height={25} />
                  </section>
                  <span className="ml-2 text-t-subtitle-2">
                    {node?.roomsFields?.roomsDetails?.bedding?.join(", ")}
                  </span>
                </div>
              )}{" "}
          </Link>
          {!hideBookNow && (
            <section className="my-4 text-left">
              <Button
                className="hover-effect focus-btn active-btn"
                onClick={() => router.push(hotelReservationUrl ?? "/")}
              >
                {state?.localeData?.navbar?.bookNowText}
              </Button>
            </section>
          )}
        </section>
        <Link
          href={`${hotelUrl}/rooms/${node?.title?.toLowerCase()?.replace(/\s+/g, "-")}`}
        >
          {" "}
          <div
            className="absolute inset-0 z-10"
            style={{
              background:
                "linear-gradient(0deg, rgba(0, 0, 0, 0.40) 0%, rgba(0, 0, 0, 0.40) 100%)",
            }}
          ></div>
        </Link>
      </div>
    ));
  return (
    <>
      <ResponsiveCarousel items={items} centerMode payload={payload} />
    </>
  );
}
