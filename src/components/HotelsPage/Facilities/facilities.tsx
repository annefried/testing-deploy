"use client";

import type { AppContextType } from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import Image from "next/image";
import { useContext, useEffect, useRef, useState } from "react";
import BackgroundImage from "@/components/Images/Hotels/facilitiesBg.png";
import Button from "@/components/Button/button";
import { useMediaQuery } from "react-responsive";
export default function Facilities(props: any) {
  const { state }: AppContextType = useContext(AppContext);
  const { facilities } = props;
  const mobileScreen = useMediaQuery({ maxWidth: 767 });
  const [showMore, setShowMore] = useState(false);

  const itemsToShow = showMore ? facilities : facilities?.slice(0, 6);
  const containerRef = useRef<HTMLDivElement>(null);
  const itemsPerRow = 3;
  const heightMultiplier = showMore
    ? 2
    : facilities?.length <= 4 && !mobileScreen
      ? 3
      : 2.7;
  const defaultItemHeight = 55;
  useEffect(() => {
    const resizeContainer = () => {
      if (containerRef.current) {
        const items = containerRef.current.querySelectorAll(".facility-item");
        const rows = Math.ceil(items.length / itemsPerRow);
        let itemHeight = defaultItemHeight;
        if (items.length > 0 && items?.length <= 3) {
          itemHeight = 120;
        }
        containerRef.current.style.height = `${rows * itemHeight * heightMultiplier}px`;
      }
    };

    resizeContainer();

    window.addEventListener("resize", resizeContainer);
    return () => {
      window.removeEventListener("resize", resizeContainer);
    };
  }, [showMore]);
  return (
    <div className="relative mb-5 object-cover" ref={containerRef}>
      <Image
        src={BackgroundImage?.src}
        alt="background-image"
        width={0}
        height={0}
        sizes="100vw"
        style={{ height: "100%", width: "100%" }}
      />
      <section className="break-word absolute bottom-0 top-0 z-10 mx-0 my-auto flex w-full flex-col justify-center px-0 py-7 text-white lg:px-[120px]">
        <div className="mt-6">
          <h2 className="mt-3 text-center font-display text-t-subtitle-2 uppercase sm:text-t-subtitle-1 lg:text-t-h5">
            {state?.localeData?.hotels?.facilities}
          </h2>
        </div>

        <section
          className={`mb-6 mt-7 grid w-full grid-cols-3 gap-y-5 px-3 ${itemsToShow?.length <= 4 ? "sm:grid-cols-4" : "sm:grid-cols-6"} lg:px-8 xl:mt-12`}
        >
          {itemsToShow?.length !== 0 &&
            itemsToShow?.map((items: any, index: number) => {
              return (
                <div
                  key={index}
                  className="facility-item flex flex-col items-center text-center"
                >
                  <section>
                    <Image
                      src={items?.icon?.icon?.node?.mediaItemUrl}
                      alt={items?.icon?.icon?.node?.altText ?? "icon"}
                      width={30}
                      height={30}
                      className="h-[20px] w-[20px] sm:h-[30px] sm:w-[30px]"
                    />
                  </section>
                  <span className="sm:t-subtitle-2 mt-2 font-body text-t-span">
                    {items?.name}
                  </span>
                </div>
              );
            })}
        </section>

        {facilities?.length > 6 && (
          <div className="mb-5 text-center">
            <Button
              onClick={() => setShowMore(!showMore)}
              className="hover-effect"
              variant="white-background"
              style={{ fontSize: "14px" }}
            >
              {" "}
              {showMore
                ? state?.localeData?.showLess
                : state?.localeData?.showMore}
            </Button>
          </div>
        )}
      </section>
      <div
        style={{
          background:
            "linear-gradient(0deg, rgba(0, 0, 0, 0.50) 0%, rgba(0, 0, 0, 0.50) 100%)",
        }}
        className="z-5 absolute inset-0"
      ></div>
    </div>
  );
}
