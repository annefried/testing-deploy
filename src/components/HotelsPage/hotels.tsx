"use client";
import type { HotelProps } from "@/interfaces/hotels.interface";
import { useRef } from "react";
import Banner from "../Banner/banner";
import SubNavbar from "../SubNavbar/subNavbar";
import OurHotels from "./Destination/OurHotels/ourHotels";
import RelatedOffers from "./Discover/RelatedOffers/relatedOffers";

const Hotels = (props: HotelProps) => {
  const {
    sectionDestinationBanner,
    taglineUnderHotelHeading,
    taglineForOffers,
    lang,
    relatedOffers,
  } = props;
  const ourHotelsRef = useRef(null);
  const relatedOffersRef = useRef(null);
  const translatedTile = `${lang === "en" ? "Our Hotels in" : "โรงแรมใน"} ${sectionDestinationBanner?.bannerTitle}`;
  const subNavbarItems = [
    {
      title: translatedTile,
      id: ourHotelsRef,
    },
    {
      title: taglineForOffers?.heading,
      id: relatedOffersRef,
    },
  ];
  return (
    <div>
      <Banner
        title={sectionDestinationBanner?.bannerTitle}
        description={sectionDestinationBanner?.subText}
        bannerImage={{
          src: sectionDestinationBanner?.backgroundImage?.node?.mediaItemUrl,
        }}
        alt={sectionDestinationBanner?.backgroundImage?.node?.altText}
      />
      <SubNavbar
        subNavbarItems={
          relatedOffers?.length !== 0 ? subNavbarItems : [subNavbarItems[0]]
        }
      />
      <OurHotels
        title={translatedTile}
        ref={ourHotelsRef}
        description={taglineUnderHotelHeading?.subText}
        city={lang?.toLowerCase()?.replace(/-/g, " ")}
      />
      <RelatedOffers
        lang={lang}
        ref={relatedOffersRef}
        title={taglineForOffers?.heading}
        description={taglineForOffers?.subText}
        relatedOffers={relatedOffers}
      />
    </div>
  );
};

export default Hotels;
