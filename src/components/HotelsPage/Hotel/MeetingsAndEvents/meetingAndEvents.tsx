"use client";
import Loader from "@/components/Loader/loader";
import DetailItems from "@/components/DetailItems/DetailItems";

import dynamic from "next/dynamic";
import Link from "next/link";
import Button from "@/components/Button/button";

const NoresultFound = dynamic(() => import("../../noResultsFound"), {
  ssr: false,
});

export default function MeetingEvents(props: {
  contactUs: string;
  lang: string;
  loading: boolean;
  payload: any;
}) {
  const { payload, loading } = props;

  const modifiedKeysArray = payload?.map(
    ({ meetingAndEvents }: { meetingAndEvents: any }) => ({
      Title: meetingAndEvents?.meetingevents[0]?.venue[0]?.heading,
      Img: meetingAndEvents?.meetingevents[0]?.venue[0]?.image?.node
        ?.mediaItemUrl,
      Alt: meetingAndEvents?.meetingevents[0]?.venue[0]?.image?.node?.altText,
      Content: meetingAndEvents?.meetingevents[0]?.venue[0]?.description,
      RoomSize: meetingAndEvents?.meetingevents[0]?.venue[0]?.roomSize,
      Url: meetingAndEvents?.moreInformation?.url,
    })
  );

  if (loading) {
    return (
      <div className="my-[200px] flex w-full justify-center">
        <Loader />
      </div>
    );
  }
  if (!loading && payload?.length === 0) {
    return <NoresultFound />;
  }
  return (
    payload?.length !== 0 && (
      <>
        <DetailItems item={modifiedKeysArray} />
        <section className="text-center">
          <Link href={`/${props.lang}/contact-us`}>
            <Button style={{ fontSize: "16px" }} className="hover-effect">
              {props.contactUs}
            </Button>
          </Link>
        </section>
      </>
    )
  );
}
