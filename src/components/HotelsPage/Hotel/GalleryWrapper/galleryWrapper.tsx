"use client";
import Gallery from "@/components/Layout/Grid/Gallery/galleryGrid";
import client from "@/lib/apollo/client";
import { GET_HOTEL_GALLERY } from "@/lib/queries/hotels";
import React, { useEffect, useState } from "react";

export default function GalleryWrapper(props: { slug: string; title: string }) {
  const [payload, setPayload] = useState([]);
  const [loading, setLoading] = useState<boolean>(true);
  const fetchData = async () => {
    try {
      const data = await client.query({
        query: GET_HOTEL_GALLERY,
        variables: { slug: props?.slug },
      });
      setPayload(
        data?.data?.hotelBy?.hotelDetails?.hotelSection?.hotelGallery?.nodes
      );
      setLoading(false);
    } catch (err) {
      setLoading(false);
      if (err) {
        return setPayload([]);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (loading) {
    return null;
  }
  if (!loading && payload?.length === 0) {
    return null;
  }
  return (
    payload?.length !== 0 && <Gallery title={props?.title} payload={payload} />
  );
}
