"use client";
import dynamic from "next/dynamic";
import Banner from "@/components/Banner/banner";
const Location = dynamic(() => import("./Location/location"), {
  ssr: false,
});

const GalleryWrapper = dynamic(
  () => import("./GalleryWrapper/galleryWrapper"),
  {
    ssr: false,
  }
);
const NoresultFound = dynamic(() => import("../noResultsFound"), {
  ssr: false,
});
import Facilities from "../Facilities/facilities";
import { useContext, useEffect, useState } from "react";
import AppContext from "@/lib/context";
import type {
  AppContextType,
  imageProps,
} from "@/interfaces/global_interfaces";
import TabsSection from "@/components/Tabs/tabs";
import type { HotelDetailProps } from "@/interfaces/hotels.interface";
import Rooms from "./Rooms/rooms";
import MeetingEvents from "./MeetingsAndEvents/meetingAndEvents";
import { useParams, usePathname, useSearchParams } from "next/navigation";
import OfferDetail from "./Offers/detail";
import OfferContainer from "./Offers/container";
import DetailItems from "@/components/DetailItems/DetailItems";
import client from "@/lib/apollo/client";
import {
  GET_EVENTS_MEETINGS_BY_HOTEL,
  GET_ROOMS_BY_HOTEL,
} from "@/lib/queries/hotels";

export default function Hotel(props: HotelDetailProps) {
  const params = useParams()?.hotel;
  const searchParam = useSearchParams();
  const pathName = usePathname();
  const {
    lang,
    hotelDetails,
    title,
    slug,
    hotelFacilities,
    tabsData,
    offers,
    dining,
  } = props;
  const { hotelSection, discoverHotelSection } = hotelDetails;
  const { dispatch, state }: AppContextType = useContext(AppContext);
  const [hotels, setHotels] = useState([]);
  const [hotelLoading, setHotelLoading] = useState<boolean>(true);
  const [meetingEvents, setMeetingEvents] = useState([]);
  const [meetingEventsLoading, setMeetingEventsLoading] =
    useState<boolean>(true);
  const [showDetails, setShowDetails] = useState<string | null>(
    searchParam?.get("offerSlug") ?? null
  );
  const filteredOffers =
    offers?.length !== 0
      ? offers
          ?.filter((item: { offersType: { nodes: any[] } }) => {
            return item.offersType.nodes.some(
              (node) => node.name?.toLowerCase() === "featured"
            );
          })
          ?.slice(0, 3)
      : [];

  const fetchHotels = async () => {
    try {
      const data = await client.query({
        query: GET_ROOMS_BY_HOTEL,
        variables: { slug },
      });
      setHotels(data?.data?.rooms?.edges);
      setHotelLoading(false);
    } catch (err) {
      setHotelLoading(false);
      if (err) {
        return setHotels([]);
      }
    }
  };

  const fetchMeetingEvents = async () => {
    try {
      const data = await client.query({
        query: GET_EVENTS_MEETINGS_BY_HOTEL,
        variables: {
          hotelSlug: slug,
        },
      });
      const items = data?.data?.allMeetingevents?.nodes;
      setMeetingEvents(items);
      setMeetingEventsLoading(false);
    } catch (err) {
      setMeetingEventsLoading(false);
      if (err) {
        return setMeetingEvents([]);
      }
    }
  };

  useEffect(() => {
    fetchHotels();
  }, []);

  useEffect(() => {
    fetchMeetingEvents();
  }, []);
  useEffect(() => {
    if (slug === params) {
      dispatch?.setHotelPayload({
        title,
        url: hotelSection?.bookUrl?.url ?? "",
      });
    }
    return () => {
      dispatch?.setHotelPayload(null);
    };
  }, [pathName]);

  const diningData =
    dining?.length !== 0
      ? dining?.map(
          ({
            node,
          }: {
            node: {
              title: string;
              featuredImage: imageProps;
              resturantDetailsFields: {
                resturantDetails: { resturantDetails: string };
              };
            };
          }) => ({
            Title: node?.title,
            Img: node?.featuredImage?.node?.mediaItemUrl,
            Alt: node?.featuredImage?.node?.altText,
            Content:
              node?.resturantDetailsFields?.resturantDetails?.resturantDetails,
          })
        )
      : [];

  type Tab = {
    type: string;
  };
  type TabsData = Tab[];
  let filteredTabList: TabsData = tabsData;

  if (offers?.length === 0) {
    filteredTabList = filteredTabList.filter(
      (tab) => tab.type?.toLowerCase() !== "offers"
    );
  }
  if (dining?.length === 0) {
    filteredTabList = filteredTabList.filter(
      (tab) => tab.type?.toLowerCase() !== "dining"
    );
  }
  if (discoverHotelSection[0]?.locationcontact === null) {
    filteredTabList = filteredTabList.filter(
      (tab) => tab.type?.toLowerCase() !== "location"
    );
  }
  if (hotels?.length === 0) {
    filteredTabList = filteredTabList.filter(
      (tab) => tab.type?.toLowerCase() !== "room"
    );
  }
  if (meetingEvents?.length === 0) {
    filteredTabList = filteredTabList.filter(
      (tab) => tab.type?.toLowerCase() !== "meeting/event"
    );
  }

  return (
    <div>
      <Banner
        title={title}
        subTitle={hotelSection?.selectBrand?.edges[0]?.name}
        description={hotelSection?.hotelShortDescription}
        bannerImage={hotelSection?.bannerImage?.node?.mediaItemUrl}
        alt={hotelSection?.bannerImage?.node?.altText}
      />
      <div className="break-word bg-white px-4 py-4 text-text-main lg:px-[80px] lg:py-12">
        <h2 className="font-600 lg:text-t-t-h4 mb-4 text-center font-display text-t-subtitle-1 ">
          {title}
        </h2>
        <div
          className="text-left font-display text-t-caption lg:text-t-subtitle-2"
          dangerouslySetInnerHTML={{ __html: hotelSection?.hotelDescription }}
        ></div>
      </div>
      <Facilities facilities={hotelFacilities?.nodes} />
      <h2 className="my-3 text-center font-display text-t-subtitle-2 sm:text-t-subtitle-1 lg:text-t-h5">
        {state?.localeData?.hotels?.discover}
      </h2>
      {!hotelLoading && (
        <TabsSection
          tabList={filteredTabList}
          tabPanelList={filteredTabList?.map((item: { type: string }) => {
            switch (item.type?.toLowerCase()) {
              case "room":
                return (
                  <Rooms
                    key="rooms"
                    hotelReservationUrl={hotelSection?.bookUrl?.url ?? "/"}
                    hotelUrl={`/${lang}/hotels/${slug}`}
                    payload={hotels}
                    loading={hotelLoading}
                  />
                );
              case "dining":
                return <DetailItems key="dining" item={diningData} />;
              case "meeting/event":
                return (
                  <MeetingEvents
                    key="meeting/event"
                    contactUs={state?.localeData?.contactUs}
                    lang={lang}
                    payload={meetingEvents}
                    loading={meetingEventsLoading}
                  />
                );
              case "location":
                return (
                  <Location
                    key="location"
                    title={state?.localeData?.hotels?.address}
                    payload={discoverHotelSection[0]?.locationcontact}
                  />
                );
              case "offers":
                return showDetails ? (
                  <OfferDetail
                    key="offers"
                    {...state?.localeData?.offers}
                    hotelUrl={hotelSection?.bookUrl?.url ?? "/"}
                    bookNowText={state?.localeData?.navbar?.bookNowText}
                    setShowDetails={setShowDetails}
                    slug={showDetails}
                    hotelSlug={slug}
                  />
                ) : (
                  <OfferContainer
                    payload={offers}
                    viewDetail={state?.localeData?.hotels?.viewDetail}
                    setShowDetails={setShowDetails}
                  />
                );
              default:
                return <NoresultFound />;
            }
          })}
        />
      )}

      <GalleryWrapper
        title={state?.localeData?.hotels?.hotelGallery}
        slug={slug}
      />
      {filteredOffers?.length !== 0 && (
        <div className="bg-white py-8 pb-0 text-text-main xl:pb-[70px]">
          <h3 className="my-5 text-center font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
            {state?.localeData?.offers?.specialOffers}
          </h3>
          <OfferContainer payload={filteredOffers} lang={lang} />
        </div>
      )}
    </div>
  );
}
