"use client";
import Maps from "@/components/Maps/maps";
import AddressIcon from "@/components/icons/Hotels/Address.svg";
import EmailIcon from "@/components/icons/Hotels/Email.svg";
import PhoneIcon from "@/components/icons/Hotels/Phone.svg";
import type { LocationContact } from "@/interfaces/hotels.interface";
import Image from "next/image";
import { useMediaQuery } from "react-responsive";

export default function Location(props: {
  title: string;
  payload: LocationContact;
}) {
  const { title, payload } = props;
  const { address, phone, email, longitude, latitude } = payload;
  const mobileScreen = useMediaQuery({ maxWidth: 767 });
  const array = [
    {
      type: "address",
      content: address,
      icon: AddressIcon,
    },
    {
      type: "phone",
      content: phone,
      icon: PhoneIcon,
    },
    {
      type: "email",
      content: email,
      icon: EmailIcon,
    },
  ];

  return (
    <div className="flex w-full flex-col-reverse flex-wrap items-center justify-center px-4 py-4 font-body text-text-main lg:flex-row lg:items-start  lg:px-12">
      <section className="w-full lg:w-1/2">
        <h4 className="font-600 mb-4 text-t-subtitle-1 lg:text-t-h6">
          {title}
        </h4>
        <ul>
          {array.map((item: { type: string; content: string }, i: number) => (
            <li
              key={i}
              className="-caption flex items-center pb-4 md:text-t-subtitle-2"
            >
              {item?.content && (
                <>
                  <Image
                    src={
                      item.type === "address"
                        ? AddressIcon
                        : item.type === "phone"
                          ? PhoneIcon
                          : EmailIcon
                    }
                    alt="icon"
                    width={24}
                    height={24}
                  />
                  &nbsp;
                  {item.type !== "address" ? (
                    <a
                      href={
                        item.type === "email"
                          ? `mailto:${item.content}`
                          : `tel:${item.content}`
                      }
                    >
                      {item.content}
                    </a>
                  ) : (
                    <span>{item.content}</span>
                  )}
                </>
              )}
            </li>
          ))}
        </ul>
      </section>
      <section className="google-map my-4 w-full lg:my-0 lg:w-1/2">
        <Maps
          height={mobileScreen ? "300px" : "400px"}
          coordinates={[
            {
              locationName: "Thailand",
              latitude: latitude,
              longtitude: longitude,
            },
          ]}
        />
      </section>
    </div>
  );
}
