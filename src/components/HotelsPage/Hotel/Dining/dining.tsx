"use client";
import Loader from "@/components/Loader/loader";
import client from "@/lib/apollo/client";
import { GET_DINING_BY_SLUG } from "@/lib/queries/hotels";
import React, { useEffect, useState } from "react";
import DetailItems from "@/components/DetailItems/DetailItems";
import type { imageProps } from "@/interfaces/global_interfaces";
import dynamic from "next/dynamic";

const NoresultFound = dynamic(() => import("../../noResultsFound"), {
  ssr: false,
});

export default function Dining(props: { slug: string }) {
  const [payload, setPayload] = useState([]);
  const [loading, setLoading] = useState<boolean>(true);
  const fetchData = async () => {
    try {
      const data = await client.query({
        query: GET_DINING_BY_SLUG,
        variables: { hotelSlug: props?.slug },
      });
      const modifiedKeysArray = data?.data?.resturants?.edges?.map(
        ({
          node,
        }: {
          node: {
            title: string;
            featuredImage: imageProps;
            resturantDetailsFields: {
              resturantDetails: { resturantDetails: string };
            };
          };
        }) => ({
          Title: node?.title,
          Img: node?.featuredImage?.node?.mediaItemUrl,
          Alt: node?.featuredImage?.node?.altText,
          Content:
            node?.resturantDetailsFields?.resturantDetails?.resturantDetails,
        })
      );
      setPayload(modifiedKeysArray);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      if (err) {
        return setPayload([]);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (loading) {
    return (
      <div className="my-[200px] flex w-full justify-center">
        <Loader />
      </div>
    );
  }
  if (!loading && payload?.length === 0) {
    return <NoresultFound />;
  }
  return payload?.length !== 0 && <DetailItems item={payload} />;
}
