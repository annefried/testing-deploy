"use client";
import Loader from "@/components/Loader/loader";
import Carousel from "../../Carousel/container";
import NoresultFound from "../../noResultsFound";

export default function Rooms(props: {
  hotelUrl: string;
  hotelReservationUrl?: string;
  loading: boolean;
  payload: any;
}) {
  const { payload, loading } = props;

  if (loading) {
    return (
      <div className="my-[200px] flex w-full justify-center">
        <Loader />
      </div>
    );
  }
  if (!loading && payload?.length === 0) {
    return <NoresultFound />;
  }
  return (
    payload?.length !== 0 && (
      <Carousel
        payload={payload}
        hotelReservationUrl={props.hotelReservationUrl}
        hotelUrl={props.hotelUrl}
      />
    )
  );
}
