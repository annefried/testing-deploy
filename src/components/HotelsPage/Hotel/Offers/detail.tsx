"use client";
import React, { useEffect, useState } from "react";
import { ArrowLeft } from "@/components/icons/arrows";
import Image from "next/image";
import LimitedOffer from "@/components/icons/Offers/LimitedOffer.svg";
import StayPeriod from "@/components/icons/Offers/StayPeriod.svg";
import Button from "@/components/Button/button";
import Link from "next/link";
import client from "@/lib/apollo/client";
import Loader from "@/components/Loader/loader";
import { GET_OFFER_DETAILS_BY_HOTEL } from "@/lib/queries/offers";
import type { ChildOfferProps } from "@/interfaces/offers.interface";
import moment from "moment";
import NoresultFound from "../../noResultsFound";

export default function OfferDetail(props: {
  stayPeriod: string;
  limitedOffer: string;
  termsAndConditions: string;
  specialOffers: string;
  hotelUrl: string;
  bookNowText: string;
  setShowDetails: any;
  slug: string;
  hotelSlug: string;
}) {
  const {
    stayPeriod,
    limitedOffer,
    termsAndConditions,
    specialOffers,
    bookNowText,
    setShowDetails,
    slug,
    hotelSlug,
  } = props;
  const [offerDetail, setOfferDetail] = useState<ChildOfferProps | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const fetchData = async () => {
    try {
      const data = await client.query({
        query: GET_OFFER_DETAILS_BY_HOTEL,
        variables: {
          mainOfferSlug: slug,
          hotelSlug,
        },
      });
      const items = data?.data?.childOffers?.edges[0];
      setOfferDetail(items);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      if (err) {
        return setOfferDetail(null);
      }
    }
  };

  useEffect(() => {
    if (slug && hotelSlug) {
      fetchData();
    }
  }, [slug]);

  if (loading) {
    return (
      <div className="my-[200px] flex w-full justify-center">
        <Loader />
      </div>
    );
  }

  const details = offerDetail?.node?.offersChild?.offersDetail;
  const parentOfferDetail =
    offerDetail?.node?.offersChild?.parentOffer?.nodes[0] ?? "";

  return (
    <div className="px-4 py-12 text-text-main lg:px-[50px] xl:px-[100px]">
      <section className="text-left font-body">
        <button
          onClick={() => setShowDetails(null)}
          className="flex items-center text-t-caption lg:text-t-subtitle-2"
          type="button"
        >
          <ArrowLeft />
          <span className="ml-3">{specialOffers}</span>
        </button>
      </section>
      {!loading && !offerDetail ? (
        <NoresultFound />
      ) : (
        <div className="my-6 flex flex-col lg:flex-row">
          <section className="mb-6 w-full lg:mb-0 lg:w-[40%] xl:w-[50%]">
            {parentOfferDetail && (
              <Image
                src={parentOfferDetail?.featuredImage?.node?.mediaItemUrl}
                width={400}
                height={500}
                className="h-[300px] w-full object-cover lg:h-[400px] lg:w-auto xl:h-[500px]"
                alt={parentOfferDetail?.featuredImage?.node?.altText}
              />
            )}
          </section>
          <section className="lg:[40%] ml-0 w-full lg:ml-8">
            <h3 className="mb-4 font-display text-t-subtitle-1 tracking-normal lg:text-t-h5">
              {parentOfferDetail?.title}
            </h3>
            {parentOfferDetail.offerMain?.offerIntroduction && (
              <div
                className="break-word text-start font-body text-t-caption lg:text-t-subtitle-2"
                dangerouslySetInnerHTML={{
                  __html: parentOfferDetail?.offerMain?.offerIntroduction,
                }}
              ></div>
            )}
            <section className="my-5 font-body  text-t-caption lg:text-t-subtitle-2">
              {details?.inclusions && (
                <section className="offer-inclusions flex">
                  <div
                    dangerouslySetInnerHTML={{ __html: details?.inclusions }}
                  ></div>
                </section>
              )}

              {details?.periodEndDate && (
                <section className="mt-4 flex items-center font-body text-t-caption font-bold lg:text-t-subtitle-2">
                  <Image
                    src={LimitedOffer}
                    alt="icon"
                    className="mr-2"
                    width={24}
                    height={25}
                  />
                  <span>
                    {limitedOffer}{" "}
                    {moment(details?.periodEndDate).format("DD MMMM YYYY")}
                  </span>
                </section>
              )}

              {details?.stayPeriodStart && (
                <section className="mt-2 flex items-center font-body text-t-caption font-bold lg:text-t-subtitle-2">
                  <Image
                    src={StayPeriod}
                    className="mr-2"
                    alt="icon"
                    width={24}
                    height={25}
                  />
                  <span>
                    {stayPeriod}{" "}
                    {moment(details?.stayPeriodStart).format("DD MMMM YYYY")} -{" "}
                    {moment(details?.periodEndDate).format("DD MMMM YYYY")}
                  </span>
                </section>
              )}
              {details?.termsConditions && (
                <section className="my-4 font-body  text-t-caption lg:text-t-subtitle-2">
                  <h5 className="font-bold">{termsAndConditions}</h5>
                  <div
                    className="mt-2 pl-3"
                    dangerouslySetInnerHTML={{
                      __html: details?.termsConditions,
                    }}
                  ></div>
                </section>
              )}
              {parentOfferDetail && details?.reservationUrl?.url && (
                <Link href={details?.reservationUrl?.url ?? "/"} target="blank">
                  <Button>{bookNowText}</Button>
                </Link>
              )}
            </section>
          </section>
        </div>
      )}
    </div>
  );
}
