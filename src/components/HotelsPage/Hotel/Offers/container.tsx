"use client";
import type { BannerItem } from "@/interfaces/carousel.interface";
import ArrowRight from "@/components/icons/Home/RightArrow.svg";
import Image from "next/image";
import type { imageProps } from "@/interfaces/global_interfaces";
import { useRouter } from "next/navigation";
import NoresultFound from "../../noResultsFound";
export default function OfferContainer({
  payload,
  viewDetail,
  setShowDetails,
  lang,
}: {
  payload: BannerItem | any;
  viewDetail?: string;
  setShowDetails?: any;
  lang?: string;
}) {
  const router = useRouter();
  if (payload?.length === 0) {
    return <NoresultFound />;
  }
  const items =
    payload &&
    payload.length !== 0 &&
    payload?.map(
      (
        item: {
          title: string;
          src: string;
          slug?: string;
          featuredImage: imageProps;
        },
        index: number
      ) => (
        <div key={index} className="mx-2 my-3 sm:my-0 lg:mx-5">
          <div
            key={index}
            className="relative max-h-[250px] min-h-[250px] w-full cursor-pointer md:max-h-[350px] md:min-h-[350px]"
            onClick={() => {
              if (setShowDetails) {
                setShowDetails(item?.slug);
              } else {
                router.push(`/${lang}/offers/${item?.slug}`);
              }
            }}
          >
            <Image
              height={400}
              width={400}
              className="h-[250px] w-full object-cover md:h-[350px]"
              alt={item.featuredImage?.node?.altText ?? ""}
              src={item.featuredImage?.node?.mediaItemUrl}
            />

            <section className="absolute inset-0 z-20 flex transform flex-col justify-end p-4 px-8 font-display text-white transition-transform hover:translate-x-1">
              <h2 className="break-word mb-2 text-start font-display text-t-subtitle-2 lg:text-t-h6">
                {item?.title}
              </h2>
              {viewDetail && (
                <div className="mb-2 mt-3 flex cursor-pointer items-center transition-transform hover:translate-x-1">
                  <h5 className="mr-3 font-body text-t-caption text-t-span">
                    {viewDetail}
                  </h5>
                  <Image src={ArrowRight} alt="icon" width={12} height={10} />
                </div>
              )}
            </section>
            <div
              className="absolute inset-0 z-10"
              style={{
                background:
                  "linear-gradient(180deg, rgba(255, 255, 255, 0.00) 0.12%, rgba(0, 0, 0, 0.80) 93.02%)",
              }}
            ></div>
          </div>
        </div>
      )
    );
  return (
    <div className="grid grid-cols-1 gap-y-0 px-4 py-4 sm:grid-cols-2 sm:gap-y-4 lg:grid-cols-3 lg:gap-y-8 lg:px-12">
      {items}
    </div>
  );
}
