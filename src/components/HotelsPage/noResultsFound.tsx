import type { AppContextType } from "@/interfaces/global_interfaces";
import AppContext from "@/lib/context";
import { useContext } from "react";

const NoresultFound = () => {
  const { state }: AppContextType = useContext(AppContext);

  return (
    <div className="my-[200px]">
      <h3 className="text-center font-body text-t-subtitle-2 tracking-normal">
        {state?.localeData?.noResultsFound}
      </h3>
    </div>
  );
};

export default NoresultFound;
