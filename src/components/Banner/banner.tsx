import ArrowRight from "@/components/icons/Home/RightArrow.svg";
import type { BannerItem } from "@/interfaces/banner.interface";
import Image from "next/image";
import Link from "next/link";
import Button from "../Button/button";

const Banner: React.FC<BannerItem> = ({
  bannerImage,
  title,
  subTitle,
  description,
  link,
  buttonText,
  showMainButton,
  alt,
}) => {
  return (
    <div className="relative inline-block w-full overflow-hidden">
      <Image
        alt={alt ?? title}
        width={1000}
        height={800}
        src={bannerImage?.src ?? bannerImage}
        className="h-full max-h-[450px] min-h-[450px] w-full object-cover object-left-top transition-transform duration-500 hover:scale-110 lg:max-h-[600px] lg:min-h-[600px]"
      />
      <div
        style={{ top: "50%", transform: "translateY(-50%)" }}
        className={`break-word absolute left-10 z-20 flex w-3/4 flex-col flex-wrap items-start  justify-start text-white sm:w-2/3 xl:left-20 xl:w-[65%]`}
      >
        {subTitle && (
          <h4 className="sm:t-caption mb-3 font-body text-t-subtitle-2 uppercase">
            {subTitle}
          </h4>
        )}
        <h1 className="text-sub font-display text-t-h6 tracking-light sm:text-t-h4 md:tracking-lighest lg:text-t-h2 xl:text-t-h2 xl:tracking-tight">
          {title}
        </h1>
        {description && (
          <div
            className="text-overflow-clamp mt-3 font-body text-t-span text-t-subtitle-2"
            dangerouslySetInnerHTML={{ __html: description }}
          ></div>
        )}
        {!showMainButton && buttonText && link && (
          <Link href={link} passHref legacyBehavior>
            <a className="mt-3 font-body text-t-caption transition-transform hover:translate-x-1">
              <span className="inline-block">{buttonText}</span>
              <span className="ml-3 inline-block">
                <Image src={ArrowRight} alt="icon" width={12} height={10} />
              </span>
            </a>
          </Link>
        )}
        {showMainButton && buttonText && link && (
          <Link href={link}>
            <Button className="hover-effect mt-2 text-t-caption" type="button">
              {buttonText}
            </Button>
          </Link>
        )}
      </div>
      <div
        className="pointer-events-none absolute inset-0 z-10"
        style={{
          background:
            "linear-gradient(0deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.20) 100%)",
        }}
      ></div>
    </div>
  );
};

export default Banner;
