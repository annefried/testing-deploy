import fetch from "isomorphic-unfetch";
import _replace from "lodash/replace";
const WORDPRESS_URL = process.env.NEXT_PUBLIC_WORDPRESS_DOMAIN_URL ?? "";
const FRONTEND_URL = process.env.NEXT_PUBLIC_DOMAIN_URL;

const HOSTNAME_REGEX = new RegExp(WORDPRESS_URL, "g");

export default async function proxy(
  req: { url: string | string[] },
  res: {
    redirect: (arg0: string) => any;
    setHeader: (arg0: string, arg1: string | null) => void;
    send: (arg0: any) => void;
  }
) {
  let content;
  let contentType;
  const upstreamRes = await fetch(`${WORDPRESS_URL}${req.url}`, {
    redirect: "manual",
  });

  if (upstreamRes.status > 300 && upstreamRes.status < 310) {
    const location = upstreamRes.headers.get("location") ?? "";
    const locationURL = new URL(location, upstreamRes.url);
    if (locationURL.href.includes(WORDPRESS_URL)) {
      const locationURL = new URL(location, upstreamRes.url);
      const response2 = await fetch(locationURL, {
        redirect: "manual",
      });
      content = await response2.text();
      contentType = response2.headers.get("content-type");
    } else {
      throw new Error(
        `abort proxy to non wordpress target ${locationURL.href} to avoid redirect loops`
      );
    }
  } else if (upstreamRes.status === 404) {
    return res.redirect(`${FRONTEND_URL}/404`);
  } else {
    content = await upstreamRes.text();
    content = _replace(content, HOSTNAME_REGEX, FRONTEND_URL);
    if (req.url === "/post-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/travel/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/destination-pages-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/destination/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/destination-pages/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/room_type-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/hotels/bangkok/rooms/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/room_type/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/hotel-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/hotels/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/hotel/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/spa-service-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/spa/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/spa-service/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/resturant-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/eat-and-drink/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/resturant/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/promotion_and_offers-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/offers/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/promotion_and_offers/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/hotel_offers-sitemap.xml") {
      const localeSuffixReplacePosts = `${FRONTEND_URL}/offers/`;
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/hotel_offers/`, "g"),
        localeSuffixReplacePosts
      );
    }
    if (req.url === "/page-sitemap.xml") {
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/home/`, "g"),
        `${FRONTEND_URL}/`
      );
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/spa-treatment/`, "g"),
        `${FRONTEND_URL}/spa`
      );
      content = _replace(
        content,
        new RegExp(`${FRONTEND_URL}/travel-inspiration/`, "g"),
        `${FRONTEND_URL}/travel`
      );
    }
    contentType = upstreamRes.headers.get("content-type");
  }
  if (req.url.includes("sitemap")) {
    content = _replace(content, HOSTNAME_REGEX, FRONTEND_URL);
    const sitemapFind = "//(.*)main-sitemap.xsl";
    const sitemapReplace = "/main-sitemap.xsl";
    const localeSuffixFind = "/index.php";
    const localeSuffixReplace = "";
    const SITEMAP_XSL_REGEX = new RegExp(sitemapFind, "g");
    content = _replace(content, SITEMAP_XSL_REGEX, sitemapReplace);
    content = _replace(
      content,
      new RegExp(localeSuffixFind, "g"),
      localeSuffixReplace
    );
  }

  res.setHeader("Content-Type", contentType);
  res.setHeader("Cache-Control", "max-age=60");
  res.send(content);
}
