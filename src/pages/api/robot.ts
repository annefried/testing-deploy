export default function handler(
  _: any,
  res: { write: (arg0: string) => void; send: () => void }
) {
  let sitemap =
    process.env.NEXT_PUBLIC_WORDPRESS_SITE_URL + "/sitemap_index.xml";

  res.write(`Sitemap: ${sitemap}`);
  res.write("\n");
  res.write("User-agent: *");
  res.write("\n");
  res.write("Allow: /");
  res.write("\n");
  res.write("Disallow: /404");
  res.send();
}
