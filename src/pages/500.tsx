"use client";
import Button from "@/components/Button/button";
import "../app/globals.scss";
import Link from "next/link";
import type { AppContextType } from "@/interfaces/global_interfaces";
import { useContext } from "react";
import AppContext from "@/lib/context";

export default function Page500() {
  const { state }: AppContextType = useContext(AppContext);
  const text = state?.localeData?.errorPages?.client;

  return (
    <div className="break-word flex h-[70vh] flex-col items-center justify-center bg-secondary-400 bg-white text-center text-text-main">
      {state?.localeData && (
        <div className="px-5 text-left xl:px-0">
          <h1 className="font-display text-t-h5 lg:text-t-h1">500</h1> <br />
          <h2 className="font-display text-t-h6 lg:text-t-h2">
            {text?.notFound}
          </h2>{" "}
          <br />
          <p className="font-display text-t-subtitle-2">
            {text?.description}
          </p>{" "}
          <br />
          <Link href="/">
            <Button className="hover-effect custom-background">
              {" "}
              {text?.button}
            </Button>
          </Link>
        </div>
      )}
    </div>
  );
}
