import million from "million/compiler";

/** @type {import('next').NextConfig} */
const nextConfig = {
  // logging: {
  //   fetches: {
  //     fullUrl: true,
  //   },
  // },
  experimental: {
    scrollRestoration: true,
  },
  async rewrites() {
    return [
      {
        source: "/(.*)sitemap.xml",
        destination: "/api/upstream-proxy",
      },
      {
        source: "/sitemap(.*).xml",
        destination: "/api/upstream-proxy",
      },
      {
        source: "/robots.txt",
        destination: "/api/robots",
      },
    ];
  },
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "wordpress-455821-4310122.cloudwaysapps.com",
        port: "",
      },
    ],
  },
};

export default million.next(nextConfig);
