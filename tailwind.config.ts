import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/react-tailwindcss-datepicker/dist/index.esm.js",
  ],
  corePlugins: {
    textOpacity: false,
  },
  darkMode: ["class"],
  theme: {
    corePlugins: {
      textOpacity: false,
    },
    extend: {
      screens: {
        xs: "340px",
      },
      fontFamily: {
        display: "var(--libre_baskerville-font)",
        body: "var(--karla-font)",
      },
      fontSize: {
        "t-h1": "4em", // 64px
        "t-h2": "3em", // 48px
        "t-h3": "2.5em", // 40px
        "t-h4": "2.25em", // 36px
        "t-h5": "2em", // 32px
        "t-h6": "1.25em", // 24px
        "t-subtitle-1": "1.5em", // 20px
        "t-subtitle-2": "1em", // 16px,
        "t-subtitle-3": "1.5em", // 24px
        "t-caption": "0.875em", // 14px
        "t-span": "0.75em", // 12px
      },
      letterSpacing: {
        tightest: "0.125em", // 2px
        tight: "0.08em", // 1.28px
        light: "0.05em", // 0.8px
        lighest: "0.025em", // 0.4px
        normal: "0.04em", // 0.64px
      },
      spacing: {
        "i-10": "7.5em", // 120px
        "i-20": "5em", // 80px
        "i-30": "4em", // 64px
        "i-40": "3.5em", // 56px
        "i-50": "3em", // 48px
        "i-70": "2.5em", // 40px
        "i-80": "2em", // 32px
        "i-90": "1.5em", // 24px
        "i-sm": "1em", // 16px
        "i-xs": "0.5em", // 8px
        "i-xxs": "0.25em", // 4px
      },
      colors: {
        "primary-main": "#009B2F",
        "primary-dark": "#106B2C",
        "primary-light": "#86BC96",
        "text-main": "#3D3712",
        "secondary-900": "#4E4C43",
        "secondary-800": "#A8A8A8",
        "secondary-700": "#CACACA",
        "secondary-600": "#EEEEEE",
        "secondary-500": "#3D3712",
        "secondary-400": "#F6F6F6",
        "secondary-300": "#E1E1E1",
        "success": "#039855",
        "error": "#FF0000",
        "red-100": "#E21836",
        "red-900": "#B5121B",
      },
    },
  },
  plugins: [],
};
export default config;
