const siteUrl = process.env.NEXT_PUBLIC_WORDPRESS_DOMAIN_URL;
module.exports = {
  siteUrl,
  exclude: ["/404", "/subscribed-to-newsletter-success"],
  generateRobotsTxt: true,
  robotsTxtOptions: {
    policies: [
      {
        userAgent: "*",
        disallow: ["/404"],
      },
      { userAgent: "*", allow: "/" },
    ],
    additionalSitemaps: [`${siteUrl}sitemap.xml`],
  },
};
